package com.mercer.ngpd.bpm.service.gbmclientonboard;

import org.springframework.stereotype.Service;

@Service
public class GbmClientOnboardingService {

    public GbmClientOnboardingService() {
    }

    public void fetchFromSalesForce() {
        System.out.println("Fetching from sales force!");
    }
}
