package com.mercer.ngpd.bpm.service.gbmclientonboard;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(GbmClientOnboardingProperties.class)
public class GbmClientOnboardingConfiguration {

    @Bean
    public GbmClientOnboardingService gbmClientOnboardingService() {
        return new GbmClientOnboardingService();
    }
}
