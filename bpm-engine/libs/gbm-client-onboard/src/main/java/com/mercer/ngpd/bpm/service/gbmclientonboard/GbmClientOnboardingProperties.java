package com.mercer.ngpd.bpm.service.gbmclientonboard;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("gbmClientOnboarding")
public class GbmClientOnboardingProperties {

    private String mode;

    public String getMode() {
        return this.mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }
}
