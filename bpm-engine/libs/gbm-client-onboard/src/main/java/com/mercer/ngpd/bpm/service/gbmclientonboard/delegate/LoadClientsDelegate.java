package com.mercer.ngpd.bpm.service.gbmclientonboard.delegate;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

public class LoadClientsDelegate implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        execution.setVariable("firstName","Leo");
        execution.setVariable("lastName","Jin");
    }
}
