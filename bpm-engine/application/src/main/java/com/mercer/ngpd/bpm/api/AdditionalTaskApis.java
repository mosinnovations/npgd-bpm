package com.mercer.ngpd.bpm.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;


@RestController
public class AdditionalTaskApis {

    @GetMapping("/hello")
    public String hello() {
        return "World!!";
    }
}
