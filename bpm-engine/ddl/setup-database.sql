CREATE ROLE ngpdbpm WITH LOGIN PASSWORD 'ngpdbpm';

ALTER ROLE ngpdbpm CREATEDB;

CREATE DATABASE ngpdbpm;

GRANT ALL PRIVILEGES ON DATABASE ngpdbpm to ngpdbpm;