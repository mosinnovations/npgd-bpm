import * as Dotenv from 'dotenv';
import * as path from 'path';

before(function () {
    Dotenv.config({path: path.join(__dirname, '.env')});
});
