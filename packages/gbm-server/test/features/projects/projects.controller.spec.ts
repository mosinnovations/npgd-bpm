import * as sinon from 'sinon';
import * as request from 'supertest';
import * as app from '../../../src/server';
import { Project } from "../../../src/features/projects/project.schema";
import { testUtils } from '../../test.utils';
import { expect } from 'chai';
import { projects as mockProjects } from "./projects.mock";
import { CastError } from "mongoose";


describe(`Test Project Controller`, function () {
    let sandbox: sinon.SinonSandbox;

    before(function () {
        sandbox = sinon.sandbox.create();
    });

    after(function () {
        sandbox = undefined;
    });

    describe(`Test delete project`, function () {
       let projectDeleteSub: sinon.SinonStub;

       beforeEach(function () {
          projectDeleteSub = sandbox.stub(Project, 'findByIdAndRemove');
       });

       afterEach(function () {
          sandbox.restore();
       });

       it(`Should delete a project without errors given a valid id`, function (done) {
          projectDeleteSub.resolves(true);
          request(app).delete(testUtils.enhanceUrlPath('/projects/notrealid'))
              .expect(200, done);
       });

       it(`Should throw 'not found' if no projects is associated with given id`, function (done) {
           projectDeleteSub.rejects(new CastError('ObjectId', 'notrealid', '_id'));
           request(app).delete(testUtils.enhanceUrlPath('/projects/notrealid'))
               .expect(404, done);
       });

       it(`Should throw 'delete error' if db errors occur during delete`, function (done) {
           projectDeleteSub.rejects({
               message: 'Fail to delete from db',
               status: 500
           });
           request(app).delete(testUtils.enhanceUrlPath('/projects/notrealid'))
               .expect(500, done);
       });
    });

    describe(`Test get projects`, function () {
        let projectFindStub: sinon.SinonStub;
        let projectPaginateStub: sinon.SinonStub;
        let projectFindByIdStub: sinon.SinonStub;

        beforeEach(function () {
            projectFindStub = sandbox.stub(Project, 'find');
            projectPaginateStub = sandbox.stub(Project, 'paginate');
            projectFindByIdStub = sandbox.stub(Project, 'findById');
        });

        afterEach(function () {
            sandbox.restore();
        });

        it(`Should return a list of projects when no query is specified`, function (done) {
            projectFindStub.resolves(
                testUtils.enhanceMockedDocs(mockProjects)
            );
            request(app).get(testUtils.enhanceUrlPath(`/projects`))
                .end((err, res) => {
                    expect(res.ok).to.be.true;
                    expect(res.body).to.have.length.greaterThan(0);
                    done();
                });
        });

        it(`Should call paginate if 'pageIndex' and 'pageSize' properties exist`, function (done) {
            projectPaginateStub.resolves([]);
            request(app).get(testUtils.enhanceUrlPath('/projects'))
                .query({pageIndex: 0, pageSize: 10})
                .end((err, res) => {
                    expect(projectPaginateStub.calledOnce).to.be.true;
                    done();
                });
        });

        it(`Should throw 'bad request' when 'pageIndex' and 'pageSize' is not a number`, function (done) {
            projectPaginateStub.resolves(testUtils.enhanceMockedDocs(mockProjects));
            request(app).get(testUtils.enhanceUrlPath('/projects'))
                .query({
                    pageIndex: 'pageIndex',
                    pageSize: 'pageSize'
                }).expect(400, done);
        });

        it(`Should throw 'not found' if there are no projects found in db`, function (done) {
            projectFindStub.resolves([]);
            request(app).get(testUtils.enhanceUrlPath(`/projects`))
                .expect(404, done);
        });

        it(`Should throw 'not found' if no project with id exists`, function (done) {
            projectFindByIdStub.resolves({});
            request(app).get(testUtils.enhanceUrlPath('/projects/notrealid'))
                .expect(404, done);
        });
    });

    describe(`Test post project`, function () {

        let projectCreateSub: sinon.SinonStub;

        beforeEach(function () {
           projectCreateSub = sandbox.stub(Project, 'create');
        });

        afterEach(function () {
            sandbox.restore();
        });

        it(`Should call 'createProject' if request is valid`, function (done) {
            projectCreateSub.resolves(mockProjects[0]);
            request(app).post(testUtils.enhanceUrlPath('/projects'))
                .send(mockProjects[0])
                .expect(200, done);
        });

        it(`Should throw 'bad request' if request payload is invalid`, function(done) {
            projectCreateSub.resolves(mockProjects[0]);
            request(app).post(testUtils.enhanceUrlPath('/projects'))
                .send({
                    url: 'www.badpayload.com',
                    status: ''
                })
                .expect(400, done);

        });

        it(`Should throw 'create failed' if db error occurs`, function(done) {
            projectCreateSub.rejects({
                message: 'Fail to save to db',
                status: 500
            });
            request(app).post(testUtils.enhanceUrlPath('/projects'))
                .send(mockProjects[0])
                .expect(500, done);
        });
    });

    describe(`Test put project`, function () {

        let projectUpdateStub: sinon.SinonStub;

        beforeEach(function () {
            projectUpdateStub = sandbox.stub(Project, 'findByIdAndUpdate');
        });

        afterEach(function () {
            sandbox.restore();
        });

        it(`Should call 'updateProject' if request is valid`, function (done) {
            projectUpdateStub.resolves(testUtils.enhanceMockedDocs(mockProjects[0]));
            request(app).put(testUtils.enhanceUrlPath('/projects/notrealid'))
                .send(mockProjects[0])
                .set('Accept', 'application/json')
                .expect(200, done);
        });

        it(`Should throw 'bad request' if request is invalid`, function (done) {
            request(app).put(testUtils.enhanceUrlPath('/projects/notrealid'))
                .send({
                    status: 'not valid'
                }).expect(400, done);
        });

        it(`Should throw 'update failed' if save to db failed`, function (done) {
            projectUpdateStub.rejects({
                message: 'Fail to save to db',
                status: 500
            });
            request(app).put(testUtils.enhanceUrlPath('/projects/notrealid'))
                .send(mockProjects[0])
                .expect(500, done);
        });
    });
});