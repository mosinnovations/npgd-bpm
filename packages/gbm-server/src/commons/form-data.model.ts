export interface FormData {
  formId: string;
  userId: string;
  collectedData: any;
}