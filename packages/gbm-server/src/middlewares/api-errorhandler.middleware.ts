import * as express from 'express';
import { appUtils } from "../utils/AppUtils";
import * as commons from 'ngpd-bpmn-commons';

export const errorHandler = (err: commons.IHttpError, req: express.Request, res: express.Response, next: express.NextFunction) => {
    const env = appUtils.getExecutionEnv();
    if (env !== 'production') {
        res.send({
            message: err.message,
            error: err
        }).status(err.status || 500);
    } else {
        res.send({
            message: err.message
        }).status(err.status || 500);
    }
};
