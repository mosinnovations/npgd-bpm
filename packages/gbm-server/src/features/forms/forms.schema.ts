import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export interface IFormData {
  formName: string;
  fields: {
    id: string;
    label: string;
    type: string;
    controlConfig: {
      width: number;
      type: string;
    }
  }[];
  componentConfig: {
    componentKey: string;
    configData: {
      layoutConfig: {
        colNumber: number;
        hAlignment: string;
        vAlignment: string;
      }
    }
  }
}

export interface FormModel extends IFormData, mongoose.Document {
}

const FormSchema = new Schema({
  formName: String,
  fields: Schema.Types.Mixed,
  componentConfig: Schema.Types.Mixed
}, {
    timestamps: true
  });

export const FormData = mongoose.model<FormModel>('Form', FormSchema);