import * as express from 'express';
import * as controller from './forms.controller';
import { requestValidate } from '../../middlewares/request-validator.middleware';
import { validator } from './forms.validator';

const router = express.Router();

router.post('/', requestValidate(validator['post']['/']), controller.createForm);
router.get('/', controller.getForms);
router.get('/:id', requestValidate(validator['get']['/:id']), controller.getForm);
router.put('/:id', controller.updateForm);

export default router;