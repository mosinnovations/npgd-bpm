import * as Joi from 'joi';
import { IValidator } from '../../middlewares/request-validator.middleware';

export const validator: IValidator = {
  get: {
    '/:id': {
      params: {
        id: Joi.string().required()
      }
    }
  },
  post: {
    '/': {
      body: {
        formName: Joi.string().required(),
        fields: Joi.any().optional(),
        componentConfig: Joi.any().optional(),
      }
    },
  }
};