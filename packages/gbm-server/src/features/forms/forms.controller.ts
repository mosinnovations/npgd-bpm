import * as express from 'express';
import * as commons from 'ngpd-bpmn-commons';
import { CastError } from 'mongoose';

import { IFormData, FormData } from './forms.schema';

export const createForm = (req: express.Request, res: express.Response) => {
  const newForm: IFormData = { ...req.body };

  FormData.create(newForm)
    .then((form: IFormData) => {
      res.status(200).send(form);
    }).catch((error: commons.IHttpError) => {
      console.error(error);
      res.status(500).send(error);
    });
};

export const getForm = (req: express.Request, res: express.Response) => {
  const { id } = req.params;

  FormData.findById(id)
    .then((form: IFormData) => {
      res.status(200).send(form);
    }).catch((error: commons.IHttpError) => {
      console.error(error);
      res.status(500).send(error);
    });
};

export const getForms = (req: express.Request, res: express.Response) => {
  FormData.find({})
    .then((forms: IFormData[]) => {
      res.status(200).send(forms);
    }).catch((error: commons.IHttpError) => {
      console.error(error);
      res.status(500).send(error);
    });
};

export const updateForm = (req: express.Request, res: express.Response) => {
  const formResults: IFormData = { ...req.body };

  FormData.findOneAndUpdate({ _id: req.params.id }, formResults)
    .then((form: IFormData) => {
      res.status(200).send(form);
    }).catch((error: commons.IHttpError) => {
      console.error(error);
      res.status(500).send(error);
    });
};

