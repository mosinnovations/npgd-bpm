import * as express from 'express';
import * as commons from 'ngpd-bpmn-commons';
import { CastError } from 'mongoose';

import { FormDataModel, FormData } from './form-data.schema';

export const createFormResults = (req: express.Request, res: express.Response) => {
  const formResults: FormDataModel = { ...req.body, userId: req.user ? req.user.id : 'userid' };

  FormData.create(formResults)
    .then((form: FormDataModel) => {
      // validate form completion & trigger next step?

      res.status(200).send(form);
    }).catch((error: commons.IHttpError) => {
      console.error(error);
      res.status(500).send(error);
    });
};

export const updateForm = (req: express.Request, res: express.Response) => {
  const formResults: FormDataModel = { ...req.body, userId: req.user ? req.user.id : 'userid' };

  FormData.findOneAndUpdate({ _id: req.params.id }, formResults)
    // validate form completion & trigger next step?

    .then((form: FormDataModel) => {
      res.status(200).send(form);
    }).catch((error: commons.IHttpError) => {
      console.error(error);
      res.status(500).send(error);
    });
};

export const getFormById = (req: express.Request, res: express.Response) => {
  const { id } = req.params;

  FormData.findById(id)
    .then((form: FormDataModel) => {
      res.status(200).send(form);
    }).catch((error: commons.IHttpError) => {
      console.error(error);
      res.status(500).send(error);
    });
};

export const getFormsByUserId = (req: express.Request, res: express.Response) => {
  const { id } = req.params;

  FormData.find({ userId: id })
    .then((forms: FormDataModel[]) => {
      res.status(200).send(forms);
    }).catch((error: commons.IHttpError) => {
      console.error(error);
      res.status(500).send(error);
    });
};

export const getFormsByFormId = (req: express.Request, res: express.Response) => {
  const { id } = req.params;

  FormData.find({ formId: id })
    .then((forms: FormDataModel[]) => {
      res.status(200).send(forms);
    }).catch((error: commons.IHttpError) => {
      console.error(error);
      res.status(500).send(error);
    });
};

export const getFormsByFormIds = (req: express.Request, res: express.Response) => {
  const ids = req.query.id;

  FormData.find({ formId: { $in: ids } })
    .then((forms: FormDataModel[]) => {
      res.status(200).send(forms);
    }).catch((error: commons.IHttpError) => {
      console.error(error);
      res.status(500).send(error);
    });
};

