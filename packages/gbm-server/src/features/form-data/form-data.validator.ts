import * as Joi from 'joi';
import { IValidator } from '../../middlewares/request-validator.middleware';

export const validator: IValidator = {
  get: {
    '/:id': {
      params: {
        id: Joi.string().required()
      }
    },
    '/user/:id': {
      params: {
        id: Joi.string().required()
      }
    },
    '/form/:id': {
      params: {
        id: Joi.string().required()
      }
    }
  },
  post: {
    '/': {
      body: {
        formId: Joi.string().required(),
        collectedData: Joi.any().optional()
      }
    },
  },
  put: {
    '/:id': {
      params: {
        id: Joi.string().required()
      },
      body: {
        formId: Joi.string().required(),
        collectedData: Joi.any().optional()
      }
    }
  }
};