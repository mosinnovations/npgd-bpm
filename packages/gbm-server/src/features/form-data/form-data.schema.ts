import { FormData as _FormData } from '../../commons/form-data.model';
import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export interface FormDataModel extends _FormData, mongoose.Document {
}

const FormSchema = new Schema({
  formId: String,
  userId: String,
  collectedData: Schema.Types.Mixed,
  }, {
    timestamps: true
  });

export const FormData = mongoose.model<FormDataModel>('FormData', FormSchema);