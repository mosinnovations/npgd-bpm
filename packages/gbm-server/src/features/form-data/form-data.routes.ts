import * as express from 'express';
import * as controller from './form-data.controller';
import { requestValidate } from '../../middlewares/request-validator.middleware';
import { validator } from './form-data.validator';

const router = express.Router();

router.post('/', requestValidate(validator['post']['/']), controller.createFormResults);
router.get('/', controller.getFormsByFormIds);
router.put('/:id', requestValidate(validator['put']['/:id']), controller.updateForm);
router.get('/:id', requestValidate(validator['get']['/:id']), controller.getFormById);
router.get('/form/:id', requestValidate(validator['get']['/form/:id']), controller.getFormsByFormId);
router.get('/user/:id', requestValidate(validator['get']['/user/:id']), controller.getFormsByUserId);

export default router;