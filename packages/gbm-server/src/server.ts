import 'rxjs/add/operator/map';
import 'rxjs/add/operator/concat';
import 'rxjs/add/operator/expand';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/combineLatest';
import 'rxjs/add/operator/combineAll';
import 'rxjs/add/operator/concatAll';
import 'rxjs/add/operator/last';
import 'rxjs/add/operator/zip';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/concatMap';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/defer';
import 'rxjs/add/observable/from';
import 'rxjs/add/observable/empty';
import 'rxjs/add/observable/concat';
import 'rxjs/add/observable/zip';
import 'rxjs/add/observable/of';

import * as bodyParser from 'body-parser';
import * as compression from 'compression'; // compresses requests
/**
 * Module dependencies.
 */
import * as express from 'express';
import * as flash from 'express-flash';
import * as lusca from 'lusca';
import * as mongoose from 'mongoose';
import * as cors from 'cors';
import { errorHandler } from './middlewares/api-errorhandler.middleware';
/**
 * API keys and Passport configuration.
 */
import * as path from 'path';

import { appUtils } from './utils/AppUtils';
/**
 * Features
 */
import { routes as formDataRoutes } from './features/form-data';
import { routes as formRoutes } from './features/forms';

appUtils.bootstrap();

const logger = appUtils.getLogger('server');


/**
 * Create Express server.
 */
const app = express();

/**
 * Get execution env
 */
const execution_env = process.env.NODE_ENV || 'development';

/**
 * Connect to MongoDB.
 */
(<any>mongoose).Promise = global.Promise;

if (execution_env !== 'test') {
    mongoose.connect(process.env.MONGODB_URI || process.env.MONGOLAB_URI, {
        useMongoClient: true
    });

    mongoose.connection.on('error', () => {
        logger.error('MongoDB connection error. Please make sure MongoDB is running.');
        process.exit();
    });
}


/**
 * Express configuration.
 */
const serverPort = process.env.PORT || 3300;
app.set('port', serverPort);
app.use(compression());
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(flash());
app.use(lusca.xframe('SAMEORIGIN'));
app.use(lusca.xssProtection(true));
app.use(express.static(path.join(__dirname, 'public'), {maxAge: 31557600000}));


/**
 * Feature routes
 */
const apiVersion = appUtils.getApiVersion();
app.use(`/${apiVersion}/forms`, formDataRoutes);
app.use(`/${apiVersion}/form-ref`, formRoutes);

/**
 * API examples routes.
 */
// app.get('/api', apiController.getApi);
// app.get('/api/facebook', passportConfig.isAuthenticated, passportConfig.isAuthorized, apiController.getFacebook);

/**
 * OAuth authentication routes. (Sign in)
 */
// app.get('/auth/facebook', passport.authenticate('facebook', {scope: ['email', 'public_profile']}));
// app.get('/auth/facebook/callback', passport.authenticate('facebook', {failureRedirect: '/login'}), (req, res) => {
//     res.redirect(req.session.returnTo || '/');
// });

/**
 * Error Handler. Provides full stack - remove for production
 */
if (execution_env !== 'production') {
    app.use(errorHandler);
}

/**
 * Start Express server.
 */
app.listen(app.get('port'), () => {
    logger.info(`App is running at http://localhost:${app.get('port')} in '${execution_env}' mode.`);
    logger.info('  Press CTRL-C to stop\n');
});

module.exports = app;
