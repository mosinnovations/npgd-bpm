import * as Rx from 'rxjs';

const mockRepositoryCall = 
Rx.Observable.of([1, 2, 3, 4, 5, 6, 7, 8])
.mergeMap((val) => {
    const obs$ = val.map(v => Rx.Observable.of(v));
    return Rx.Observable.concat(obs$);
}).distinctUntilChanged().combineAll();

mockRepositoryCall.subscribe((val) => console.log('value = ', val));