const gulp = require('gulp');
const mocha = require('gulp-mocha');
const path = require('path');
const nodemon = require('gulp-nodemon');
const ts = require('gulp-typescript');
const shell = require('shelljs');
const tsProject = ts.createProject('tsconfig.json');
const sourcemaps = require('gulp-sourcemaps');
const rename = require('gulp-rename');
const distDir = 'dist'; // prod distribution directory
const workspace = 'build'; // workspace for running tests and other dev activities

// top level composing tasks
gulp.task('dev', ['watch-ts', 'watch-server'], () => {
});

gulp.task('build', ['compile-src', 'copy-static-assets', 'copy-config-files'], () => {
});

gulp.task('clean', () => {
  shell.rm('-rf', distDir);
  shell.rm('-rf', workspace);
});

gulp.task('test', ['compile-tests'], () => {
  gulp.src(workspace + '/test/**/*.js', {read: false})
    .pipe(mocha({reporter: 'spec'}));
});

gulp.task('test-watch', ['test'], () => {
  return gulp.watch([
    'src/**/*.ts', 'test/**/*.ts'
  ], ['test']);
});

// ** sub tasks
gulp.task('compile-src', () => {
  return gulp.src('src/**/*.ts', {base: 'src'})
    .pipe(sourcemaps.init())
    .pipe(tsProject())
    .pipe(sourcemaps.write({
      // Return relative source map root directories per file.
      sourceRoot: function (file) {
        const sourceFile = path.join(file.cwd, file.sourceMap.file);
        return path.relative(path.dirname(sourceFile), file.cwd);
      }
    }))
    .pipe(gulp.dest(distDir));
});

gulp.task('compile-tests', ['copy-test-config-files'], () => {
  return tsProject.src()
    .pipe(sourcemaps.init())
    .pipe(tsProject())
    .pipe(sourcemaps.write({
      // Return relative source map root directories per file.
      sourceRoot: function (file) {
        const sourceFile = path.join(file.cwd, file.sourceMap.file);
        return path.relative(path.dirname(sourceFile), file.cwd);
      }
    })).pipe(gulp.dest(workspace));
});

gulp.task('copy-static-assets', () => {
  return gulp.src('src/public/**/*')
    .pipe(gulp.dest(distDir + '/public'));
});

gulp.task('copy-config-files', () => {
  return gulp.src('.env')
    .pipe(gulp.dest(distDir));
});

gulp.task('copy-test-config-files', () => {
  return gulp.src('.env.test')
    .pipe(rename('.env'))
    .pipe(gulp.dest(workspace + '/test'))
    .pipe(gulp.dest(workspace + '/src'));
});

gulp.task('watch-ts', () => {
  return gulp.watch('src/**/*.ts', ['compile-src']);
});

gulp.task('watch-server', ['compile-src', 'copy-static-assets', 'copy-config-files'], () => {
  return nodemon({
    script: distDir + '/server.js',
    ext: 'js html',
    env: {'NODE_ENV': 'development'}
  })
});
