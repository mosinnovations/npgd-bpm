/* SystemJS module definition */
declare var module: NodeModule;

interface NodeModule {
  id: string;
}

/**
 * bpmnjs typings
 */
declare module 'bpmn-js' {
  class Viewer {
    constructor(config?: any);

    public importXML(xml: any, cb: (err: any) => any);

    public attachTo(id: string);

    public get(key: string): any;

    public detach();
  }

  export = Viewer;
}

declare module 'bpmn-js/lib/Modeler' {
  class Modeler {

    public injector: any;

    protected _modules: any[];

    constructor(config?: any);

    public get(key: string): any;

    public addCustomElements(customElements: any): any;

    public importXML(xml: string, callback?: (err: any) => any);

    public on(eventName: string, callback?: (err?: any) => any);

    public saveSVG(callback: () => any);

    public saveXML(options: any, callback?: (err: any, xml: any) => any);

    public getCustomElements(): any;
  }

  export = Modeler;
}

declare module 'bpmn-js/lib/features/modeling' {

  export class ElementFactory {
    protected $inject: string[];
    protected constructor();
    protected baseCreate(elementType: string, data: {
      [key: string]: any;
    });
    protected create(elementType: string, attrs: any): any;
    protected createBpmnElement(elementType: string, attrs: any); any;
    protected _getCustomElementSize(type: string): any;
  }
}

declare module 'diagram-js/lib/draw/BaseRenderer' {

  class BaseRenderer {
    protected styles: any;
    constructor(eventBus: any, timer: number);
  }

  export default BaseRenderer;
}

declare module 'bpmn-js/lib/util/LabelUtil' {
  export const DEFAULT_LABEL_SIZE: string;
}

declare module 'bpmn-js/lib/util/ModelUtil' {
  export function getBusinessObject(element: any): any;
}

/***
 * Diagram js typings
 */
declare module 'diagram-js/lib/util/RenderUtil' {
  export function componentsToPath(path: any): any;
  export function createLine(waypoints: any, attrs: any): any;
}
declare module 'diagram-js/lib/navigation/zoomscroll';
declare module 'diagram-js/lib/navigation/movecanvas';

/**
 * tiny-svg typings
 */
declare module 'tiny-svg/lib/append';
declare module 'tiny-svg/lib/attr';
declare module 'tiny-svg/lib/create';

/**
 * out of the box properties panel typings
 */
declare module 'bpmn-js-properties-panel';
declare module 'bpmn-js-properties-panel/lib/provider/bpmn';
declare module 'bpmn-js-properties-panel/lib/provider/camunda';

declare module 'bpmn-moddle' {

  class BpmnModdle {
    constructor();

    public fromXML(xmlStr: string, callback?: (err: Error, definitions: any) => void): any;
    public toXML(definitions: any, callback?: (err: Error, xmlStrUpdated: string) => void): any;
  }

  export = BpmnModdle;
}

declare module '*.json' {
  const value: any;
  export default value;
}
