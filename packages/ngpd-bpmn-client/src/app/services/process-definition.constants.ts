import * as commons from 'ngpd-bpmn-commons';

export const fieldTypeToDefaultControlType = {
  string: 'input',
  long: 'input',
  boolean: 'toggle',
  date: 'datePicker'
};

export const uiControls: {
  [key:string]: commons.ILookupItem[];
} = {
  string: [
    {
      label: 'Text Input',
      value: 'input'
    },
    {
      label: 'Select List',
      value: 'select'
    }
  ],
  long: [
    {
      label: 'Text Input',
      value: 'input'
    },
    {
      label: 'Select List',
      value: 'select'
    }
  ],
  date: [
    {
      label: 'Text Input',
      value: 'input'
    },
    {
      label: 'Date Picker',
      value: 'datePicker'
    }
  ],
  boolean: [
    {
      label: 'Toggle',
      value: 'toggle'
    }
  ],
  list: [
    {
      label: 'Multiselect',
      value: 'multiselect'
    }
  ],
};
