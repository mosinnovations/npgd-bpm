import * as commons from 'ngpd-bpmn-commons';
import * as _ from 'lodash';

const mockBpmnXMLWithUserTasks = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:camunda="http://camunda.org/schema/1.0/bpmn" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" id="Definitions_1" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="1.10.0">
  <bpmn:process id="collectUserInfo" name="Collect User Information" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>SequenceFlow_1xeceoa</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="SequenceFlow_1xeceoa" sourceRef="StartEvent_1" targetRef="Task_129s0lv" />
    <bpmn:userTask id="Task_129s0lv" name="Collect Personal Info" camunda:formKey="personalInfo">
      <bpmn:extensionElements>
        <camunda:formData>
          <camunda:formField id="firstName" label="First Name" type="string" />
          <camunda:formField id="lastName" label="Last Name" type="string" />
          <camunda:formField id="dob" label="Birthdate" type="date" />
        </camunda:formData>
      </bpmn:extensionElements>
      <bpmn:incoming>SequenceFlow_1xeceoa</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1h9nsts</bpmn:outgoing>
    </bpmn:userTask>
    <bpmn:sequenceFlow id="SequenceFlow_1h9nsts" sourceRef="Task_129s0lv" targetRef="Task_1n67vlw" />
    <bpmn:userTask id="Task_1n67vlw" name="Collect Employment Info" camunda:formKey="employmentInfo">
      <bpmn:extensionElements>
        <camunda:formData>
          <camunda:formField id="hiredDate" label="Hired Date" type="date" />
          <camunda:formField id="employmentTitle" label="Title" type="string" />
          <camunda:formField id="isRemote" label="Work Remote" type="boolean" />
        </camunda:formData>
      </bpmn:extensionElements>
      <bpmn:incoming>SequenceFlow_1h9nsts</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1fncde1</bpmn:outgoing>
    </bpmn:userTask>
    <bpmn:endEvent id="EndEvent_1glmypo">
      <bpmn:incoming>SequenceFlow_1fncde1</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="SequenceFlow_1fncde1" sourceRef="Task_1n67vlw" targetRef="EndEvent_1glmypo" />
  </bpmn:process>
  <bpmndi:BPMNDiagram id="BPMNDiagram_1">
    <bpmndi:BPMNPlane id="BPMNPlane_1" bpmnElement="collectUserInfo">
      <bpmndi:BPMNShape id="_BPMNShape_StartEvent_2" bpmnElement="StartEvent_1">
        <dc:Bounds x="173" y="102" width="36" height="36" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="SequenceFlow_1xeceoa_di" bpmnElement="SequenceFlow_1xeceoa">
        <di:waypoint xsi:type="dc:Point" x="209" y="120" />
        <di:waypoint xsi:type="dc:Point" x="253" y="120" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="231" y="98.5" width="0" height="13" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="UserTask_1rkw5wq_di" bpmnElement="Task_129s0lv">
        <dc:Bounds x="253" y="80" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="SequenceFlow_1h9nsts_di" bpmnElement="SequenceFlow_1h9nsts">
        <di:waypoint xsi:type="dc:Point" x="353" y="120" />
        <di:waypoint xsi:type="dc:Point" x="396" y="120" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="374.5" y="98.5" width="0" height="13" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="UserTask_1tp5kf4_di" bpmnElement="Task_1n67vlw">
        <dc:Bounds x="396" y="80" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="EndEvent_1glmypo_di" bpmnElement="EndEvent_1glmypo">
        <dc:Bounds x="547" y="102" width="36" height="36" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="565" y="141" width="0" height="13" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="SequenceFlow_1fncde1_di" bpmnElement="SequenceFlow_1fncde1">
        <di:waypoint xsi:type="dc:Point" x="496" y="120" />
        <di:waypoint xsi:type="dc:Point" x="547" y="120" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="521.5" y="98" width="0" height="13" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
    </bpmndi:BPMNPlane>
  </bpmndi:BPMNDiagram>
</bpmn:definitions>
`;

const mockFormInfosWithUserTasks: commons.IFormInfo[] = [
  {
    formId: 'Task_129s0lv',
    formName: 'Collect Personal Info',
    formKey: 'personalInfo',
    componentConfig: {
      componentKey: 'dynoForm',
      configData: {
        layoutConfig: {
          colNumber: 1,
          hAlignment: 'left',
          vAlignment: 'center'
        }
      }
    },
    fields: [
      {
        id: 'firstName',
        label: 'First Name',
        type: 'string',
        controlConfig: {
          type: 'input',
          width: 100
        }
      },
      {
        id: 'lastName',
        label: 'Last Name',
        type: 'string',
        controlConfig: {
          type: 'input',
          width: 100
        }
      },
      {
        id: 'dob',
        label: 'Birthdate',
        type: 'date',
        controlConfig: {
          type: 'datePicker',
          width: 100
        }
      }
    ]
  },
  {
    formId: 'Task_1n67vlw',
    formName: 'Collect Employment Info',
    formKey: 'employmentInfo',
    componentConfig: {
      componentKey: 'dynoForm',
      configData: {
        layoutConfig: {
          colNumber: 1,
          hAlignment: 'left',
          vAlignment: 'center'
        }
      }
    },
    fields: [
      {
        id: 'hiredDate',
        label: 'Hired Date',
        type: 'date',
        controlConfig: {
          type: 'datePicker',
          width: 100
        }
      },
      {
        id: 'employmentTitle',
        label: 'Title',
        type: 'string',
        controlConfig: {
          type: 'input',
          width: 100
        }
      },
      {
        id: 'isRemote',
        label: 'Work Remote',
        type: 'boolean',
        controlConfig: {
          type: 'toggle',
          width: 100
        }
      }
    ]
  }
];

export const processDefinitionMocks = {
  testSyncBPMNToFormInfo: {
    bpmnXML: () => _.cloneDeep(mockBpmnXMLWithUserTasks),
    formInfo: () => _.cloneDeep(mockFormInfosWithUserTasks)
  }
};
