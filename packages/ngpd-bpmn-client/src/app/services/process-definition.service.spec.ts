import {IProcessDefinitionSyncPayload, ProcessDefinitionService} from './process-definition.service';
import {processDefinitionMocks} from './process-definition.mock';
import * as commons from 'ngpd-bpmn-commons';
import * as _ from 'lodash';

fdescribe('ProcessDefinitionService', () => {
  let service: ProcessDefinitionService;

  beforeEach(() => {
    service = new ProcessDefinitionService();
  });

  describe('Sync UserTasks', () => {

    it('should sync form info changes to bpmn 2.0 xml', (done) => {
      const mockBpmnXML = processDefinitionMocks.testSyncBPMNToFormInfo.bpmnXML();
      const expected = processDefinitionMocks.testSyncBPMNToFormInfo.formInfo();
      const processDefinition: commons.IProcessDefinition = {
        _id: 'testProcDef1',
        name: 'collect user info',
        configXml: mockBpmnXML
      };
      service.syncFormMetaDataWithBpmnXML(processDefinition).subscribe((payload: IProcessDefinitionSyncPayload) => {
        expect(_.isEqual(payload.formData, expected)).toBe(true);
        done();
      });
    });

    it('should sync bpmn 2.0 xml changes to form info', (done) => {
      const mockFormInfo = processDefinitionMocks.testSyncBPMNToFormInfo.formInfo();
      const expected = processDefinitionMocks.testSyncBPMNToFormInfo.bpmnXML();
      const field = mockFormInfo[0].fields[0];
      field.label = field.label + ' (updated)';
      const processDefinition: commons.IProcessDefinition = {
        _id: 'testProcDef1',
        name: 'collect user info',
        configXml: expected,
        formInfoList: mockFormInfo
      };
      service.syncBpmnXMLWithFormMetaData(processDefinition).subscribe((payload: IProcessDefinitionSyncPayload) => {
        const regex = /First Name \(updated\)/i;
        expect(regex.test(payload.processDefinition.configXml)).toBe(true);
        done();
      });
    });
  });

});
