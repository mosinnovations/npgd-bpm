import {Injectable} from '@angular/core';
import * as commons from 'ngpd-bpmn-commons';
import BpmnModdle = require('bpmn-moddle');
import {AsyncSubject} from 'rxjs/AsyncSubject';
import {camundaUtils} from '../commons/camunda.utils';
import {fieldTypeToDefaultControlType} from './process-definition.constants';
import {Observable} from 'rxjs/Observable';
import * as _ from 'lodash';
import {Subject} from 'rxjs/Subject';

export interface IProcessDefinitionSyncPayload {
  processDefinition: commons.IProcessDefinition;
  formData: commons.IFormInfo[];
}

@Injectable()
export class ProcessDefinitionService {

  constructor() {
  }

  /**
   * Sync up form info meta data with BPMN XML
   *
   * Direction: BPMN XML -> Form Info
   *
   * @param {IProcessDefinition} processDefinition
   * @returns {Observable<any>}
   */

  public syncFormMetaDataWithBpmnXML(processDefinition: commons.IProcessDefinition): Observable<IProcessDefinitionSyncPayload> {
    const bpmnModdle = new BpmnModdle();
    const subject: Subject<IProcessDefinitionSyncPayload> = new AsyncSubject();
    if (!processDefinition.configXml) {
      subject.next({
        processDefinition,
        formData: []
      });
      subject.complete();
    } else {
      bpmnModdle.fromXML(processDefinition.configXml, (err, definitions) => {
        let finalFormData: commons.IFormInfo[] = this.xmlToFormInfo(processDefinition, definitions);
        subject.next({
          processDefinition,
          formData: finalFormData
        });
        subject.complete();
      });
    }
    return subject.asObservable();
  }

  /**
   * Sync up BPMN XML with Form Information
   *
   * Direction: Form Info -> BPMN XML
   *
   * @param {IProcessDefinition} processDefinition
   * @returns {Observable<any>}
   */
  public syncBpmnXMLWithFormMetaData(processDefinition: commons.IProcessDefinition): Observable<IProcessDefinitionSyncPayload> {
    const bpmnModdle = new BpmnModdle();
    const subject: Subject<IProcessDefinitionSyncPayload> = new AsyncSubject();
    if (!processDefinition.configXml) {
      subject.next({
        processDefinition,
        formData: []
      });
      subject.complete();
    } else {
      bpmnModdle.fromXML(processDefinition.configXml, (err, definitions) => {
        this.formInfoToXml(bpmnModdle, processDefinition, definitions).subscribe((syncedXml: string) => {
          subject.next({
            processDefinition: Object.assign({}, processDefinition, {
              configXml: syncedXml
            }),
            formData: processDefinition.formInfoList
          });
          subject.complete();
        });
      });
    }
    return subject.asObservable();
  }

  private xmlToFormInfo(processDefinition: commons.IProcessDefinition, definitions): commons.IFormInfo[] {
    const extractedFormData: commons.IFormInfo[] = camundaUtils.extractFormFields(definitions);
    let finalFormData: commons.IFormInfo[] = [];
    if (!processDefinition.formInfoList || processDefinition.formInfoList.length === 0) {
      // no form info present in process definition, so we assign defaults.
      finalFormData = this.setupFormInfoForNewFlow(extractedFormData);
    } else {
      // if has form Infos, we check each formInfo to see if there's an existing configuration
      finalFormData = extractedFormData.map((f) => {
        // look for the formInfo element in process def
        const fIndexInProcessDef = processDefinition.formInfoList.findIndex(pf => pf.formId === f.formId);
        if (fIndexInProcessDef === -1) {
          // not found, setup using defaults
          return this.setupNewFormInfoForExistingFlow(f);
        } else {
          // found, we make sure there are no new fields
          const existingFields = processDefinition.formInfoList[fIndexInProcessDef].fields;
          const extractedFields = f.fields;
          const newFieldIds = _.difference(extractedFields.map(ex => ex.id), existingFields.map(es => es.id));
          if (newFieldIds && newFieldIds.length > 0) {
            // we have new fields, handle accordingly.
            f.fields = this.setupNewAndExistingFieldsForExistingFlow(extractedFields, existingFields);
            // return f;
          } else {
            // no new fields, we should merge any info user changed in bpmn modeler and
            // our own form config and return to user.
            f.fields = this.setupExistingFieldsForExistingFlow(extractedFields, existingFields);
            // return f;
          }
          if (!f.componentConfig) {
            // assign default
            f = this.assignDefaultFormConfig(f);
            return f;
          } else {
            // TODO: Edit formConfig goes here
          }

          return f;

        }
      });
    }
    return finalFormData;
  }

  private formInfoToXml(bpmnModdle: any, processDefinition: commons.IProcessDefinition, definitions) {
    const subject = new AsyncSubject();
    if (!definitions) {
      return;
    }
    // walk the tree
    const results = [];
    this.populateDefinitionByType(definitions, 'bpmn:UserTask', results);
    processDefinition.formInfoList.forEach((f: commons.IFormInfo) => {
      const formDefinition = results.find(rts => rts.id === f.formId);
      this.syncFormInfoWithBpmnFormDefinition(f, formDefinition);
    });
    bpmnModdle.toXML(definitions, (err, xmlUpdates: string) => {
      if (err) {
        subject.error(err);
      } else {
        subject.next(xmlUpdates);
      }
      subject.complete();
    });
    return subject.asObservable();
  }

  private populateDefinitionByType(definition: any, type: string, results: any[]) {
    if (_.isArray(definition)) {
      definition.forEach(e => {
        if (e.get('flowElements')) {
          this.populateDefinitionByType(e.get('flowElements'), type, results);
        } else {
          this.populateDefinitionByType(e, type, results);
        }
      })
    } else {
      if (definition.get('rootElements')) {
        this.populateDefinitionByType(definition.get('rootElements'), type, results);
      } else if (definition.$type) {
        // we have a flow element
        if (definition.$type === type) {
          results.push(definition);
        }
      }
    }
  }

  private setupFormInfoForNewFlow(formData: commons.IFormInfo[]) {
    return formData.map((f) => {
      f.fields = f.fields.map((fd: commons.IFormField) => {
        return this.assignDefaultControlConfig(fd);
      });
      f = this.assignDefaultFormConfig(f);
      return f;
    });
  }

  private setupNewFormInfoForExistingFlow(formInfo: commons.IFormInfo) {
    formInfo.fields = formInfo.fields.map((fd: commons.IFormField) => {
      return this.assignDefaultControlConfig(fd);
    });
    formInfo = this.assignDefaultFormConfig(formInfo);
    return formInfo;
  }

  private setupNewAndExistingFieldsForExistingFlow(extractedFields: commons.IFormField[], existingFields: commons.IFormField[]) {
    return extractedFields.map((extractedField) => {
      const existingField = existingFields.find(existFd => existFd.id === extractedField.id);
      if (existingField) {
        return Object.assign({}, extractedField, {
          controlConfig: existingField.controlConfig
        })
      } else {
        return this.assignDefaultControlConfig(extractedField);
      }
    });
  }

  private setupExistingFieldsForExistingFlow(extractedFields: commons.IFormField[], existingFields: commons.IFormField[]) {
    return extractedFields.map((extractedField) => {
      const existingField = existingFields.find(existFd => existFd.id === extractedField.id);
      return Object.assign({}, extractedField, {
        controlConfig: existingField.controlConfig
      })
    });
  }


  private assignDefaultControlConfig(fd: commons.IFormField) {
    return Object.assign({}, fd, {
      controlConfig: {
        width: 100,
        type: this.getFormControlType(fd)
      }
    });
  }

  private assignDefaultFormConfig(formInfo: commons.IFormInfo) {
    const defaultComponentConfig: commons.IComponentConfig = {
      componentKey: 'dynoForm',
      configData: {
        layoutConfig: {
          colNumber: 1,
          hAlignment: 'left',
          vAlignment: 'center'
        }
      }
    };
    return Object.assign({}, formInfo, {
      componentConfig: defaultComponentConfig
    });
  }

  private getFormControlType(field: commons.IFormField) {
    if (field.controlConfig && field.controlConfig.type) {
      return field.controlConfig.type;
    }
    return this.getControlTypeByFieldType(field.type);
  }

  private getControlTypeByFieldType(type: string) {
    if (fieldTypeToDefaultControlType[type]) {
      return fieldTypeToDefaultControlType[type];
    }
    return 'input';
  }

  private syncFormInfoWithBpmnFormDefinition(formInfo: commons.IFormInfo, formDefinition: any) {
    const extElements = formDefinition.get('extensionElements');
    if (extElements) {
      const formDefinitionFields = extElements.values;
      formInfo.fields.forEach(f => {
        const formField = formDefinitionFields.map(fdf => {
          const children = fdf.$children;
          return children.find(c => c.id === f.id);
        });
        if (formField.length === 1) {
          const fdOfInterest = formField[0];
          Object.keys(f).forEach((key: string) => {
            if (fdOfInterest[key]) {
              fdOfInterest[key] = f[key];
            }
          });
        } else {
          console.warn('found more than one instances of field with id ', f.id);
        }
      })
    }
  }
}
