import {NgModule} from '@angular/core';
import {ProcessDefinitionService} from './process-definition.service';


@NgModule({
  providers: [ProcessDefinitionService],
})
export class ServicesModule {
}
