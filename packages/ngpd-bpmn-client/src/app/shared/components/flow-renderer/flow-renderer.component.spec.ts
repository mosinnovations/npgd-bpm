import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlowRendererComponent } from './flow-renderer.component';

describe('FlowRendererComponent', () => {
  let component: FlowRendererComponent;
  let fixture: ComponentFixture<FlowRendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlowRendererComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlowRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
