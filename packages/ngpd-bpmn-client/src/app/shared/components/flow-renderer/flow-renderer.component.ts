import {Component, Input, OnInit} from '@angular/core';
import * as commons from 'ngpd-bpmn-commons';

@Component({
  selector: 'app-flow-renderer',
  templateUrl: './flow-renderer.component.html',
  styleUrls: ['./flow-renderer.component.scss']
})
export class FlowRendererComponent implements OnInit {

  @Input() processDefinition: commons.IProcessDefinition;

  constructor() { }

  ngOnInit() {
  }

}
