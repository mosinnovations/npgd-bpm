import {NgModule} from '@angular/core';
import {
  MatButtonModule,
  MatCardModule,
  MatDatepickerModule, MatDialogModule,
  MatFormFieldModule, MatIconModule, MatInputModule,
  MatMenuModule, MatSelectModule, MatSidenavModule,
  MatSliderModule, MatSlideToggleModule,
  MatSnackBarModule, MatTableModule, MatToolbarModule, MatTooltipModule
} from '@angular/material';
import {CommonModule} from '@angular/common';
import {ComponentModule} from '../components/component.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { FlowRendererComponent } from './components/flow-renderer/flow-renderer.component';
import {HttpClientModule} from '@angular/common/http';

const SHARED_COMPONENTS = [
  FlowRendererComponent
];

const MATERIAL_COMPONENT = [
  MatButtonModule,
  MatCardModule,
  MatDialogModule,
  MatDatepickerModule,
  MatIconModule,
  MatInputModule,
  MatFormFieldModule,
  MatMenuModule,
  MatSelectModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSidenavModule,
  MatSnackBarModule,
  MatTooltipModule,
  MatToolbarModule,
  MatTableModule
];

const moduleList = [
  CommonModule,
  ComponentModule,
  FormsModule,
  FlexLayoutModule,
  HttpClientModule,
  ReactiveFormsModule,
  ...MATERIAL_COMPONENT
];

@NgModule({
  imports: moduleList,
  exports: [...moduleList, ...SHARED_COMPONENTS],
  declarations: SHARED_COMPONENTS
})
export class SharedModule {
}
