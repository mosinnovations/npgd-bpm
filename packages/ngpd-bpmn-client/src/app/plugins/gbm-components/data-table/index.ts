import {IPluginConfig} from '../../plugin-registry';
import {dataTable} from './data-table.reducer';
import {DataTableComponent} from './data-table.component';
import {DataTableConfigComponent} from './data-table-config/data-table-config.component';

export const dataTableConfig: IPluginConfig = {
  componentKey: 'dataTable',
  component: DataTableComponent,
  configComponent: DataTableConfigComponent,
  allowConfig: true,
  actions: [],
  reducers: {
    dataTable
  }
};
