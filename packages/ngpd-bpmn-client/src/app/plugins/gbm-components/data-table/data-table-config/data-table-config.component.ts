import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-table-config',
  templateUrl: './data-table-config.component.html',
  styleUrls: ['./data-table-config.component.scss']
})
export class DataTableConfigComponent implements OnInit {
  static readonly key = 'dataTable';

  constructor() { }

  ngOnInit() {
  }

}
