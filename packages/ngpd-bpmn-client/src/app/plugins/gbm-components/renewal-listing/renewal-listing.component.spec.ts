import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenewalListingComponent } from './renewal-listing.component';

describe('RenewalListingComponent', () => {
  let component: RenewalListingComponent;
  let fixture: ComponentFixture<RenewalListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenewalListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenewalListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
