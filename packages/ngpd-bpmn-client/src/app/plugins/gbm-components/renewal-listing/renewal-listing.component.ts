import {Component, EventEmitter, OnInit} from '@angular/core';
import {IPluggableComponent} from '../../IPluggable.component';
import {IPluginDataInput} from '../../plugin-item';

@Component({
  selector: 'app-renewal-listing',
  templateUrl: './renewal-listing.component.html',
  styleUrls: ['./renewal-listing.component.scss']
})
export class RenewalListingComponent implements OnInit, IPluggableComponent {
  public onComponentConfigChange: EventEmitter<any>;
  public data: IPluginDataInput;

  constructor() {
  }

  ngOnInit() {
  }
}
