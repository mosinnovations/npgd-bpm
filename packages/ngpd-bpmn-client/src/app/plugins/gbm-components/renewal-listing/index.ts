import {IPluginConfig} from '../../plugin-registry';
import {RenewalListingComponent} from './renewal-listing.component';

export const renewalListConfig: IPluginConfig = {
  componentKey: 'renewalListing',
  component: RenewalListingComponent,
  allowConfig: false,
  configComponent: null,
  actions: [],
  reducers: {}
};
