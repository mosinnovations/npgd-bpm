import {
  AfterViewInit,
  Component,
  ComponentFactoryResolver,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {PluginLoaderDirective} from './plugin-loader.directive';
import {PluginsService} from '../plugins.service';
import {IPluggableComponent} from '../IPluggable.component';
import {NoComponentFound} from '../no-component-found.component';
import {PluginItem} from '../plugin-item';

@Component({
  selector: 'app-plugin-loader',
  templateUrl: './plugin-loader.component.html',
  styleUrls: ['./plugin-loader.component.scss']
})
export class PluginLoaderComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input() pluginKey: string;
  @Input() isConfigComponent: boolean;
  @Input() standaloneFlag: boolean;
  @Input() set data(d) {
    this.inputData = d;
    if(this.componentInstance) {
      this.componentInstance.data = this.inputData;
    }
  }
  @Output() onConfigChange: EventEmitter<any> = new EventEmitter();

  @ViewChild(PluginLoaderDirective) pluginHost: PluginLoaderDirective;

  public inputData: any;
  private componentInstance: any;

  constructor(private componentResolver: ComponentFactoryResolver, private pluginService: PluginsService) {
    this.inputData = {};
    this.isConfigComponent = false;
  }

  public ngOnInit() {
  }

  public ngAfterViewInit(): void {
    console.log('here in plugin loader after view init key = ', this.pluginKey);
    this.loadComponent();
  }

  public ngOnDestroy(): void {
  }

  private loadComponent() {
    setTimeout(() => {
      const plugins = this.pluginService.getPlugins();
      console.log('here plugin = ', plugins);
      const pluginItem: PluginItem = plugins[this.pluginKey];
      const pluginConfig = pluginItem.pluginConfig;
      let pluginComponent;
      if (pluginConfig) {
        if (this.isConfigComponent) {
          pluginComponent = pluginConfig.configComponent;
        } else {
          pluginComponent = pluginConfig.component;
        }
      }
      if (!pluginComponent) {
        pluginComponent = NoComponentFound;
      }
      const componentFactory = this.componentResolver.resolveComponentFactory(pluginComponent);
      const viewContainerRef = this.pluginHost.viewContainerRef;
      viewContainerRef.clear();
      const componentRef = viewContainerRef.createComponent(componentFactory);
      this.componentInstance = componentRef.instance;
      this.componentInstance.data = this.inputData;
      this.componentInstance.standaloneFlag = this.standaloneFlag;
      if (this.componentInstance.onComponentConfigChange) {
        this.componentInstance.onComponentConfigChange
          .subscribe(result => this.onConfigChange.emit(result))
      }

    }, 0); // render on the next tick to avoid post direct check error
  }

}
