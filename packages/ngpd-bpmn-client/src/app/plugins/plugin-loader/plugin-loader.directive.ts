import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[plugin-host]'
})
export class PluginLoaderDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}
