import {NgModule} from '@angular/core';
import {PluginsService} from './plugins.service';
import {CommonModule} from '@angular/common';
import {PluginLoaderComponent} from './plugin-loader/plugin-loader.component';
import {PluginLoaderDirective} from './plugin-loader/plugin-loader.directive';
import {
  COMPONENTS as PluginComponents,
  PLUGIN_REDUCER_INJECTOR, PLUGIN_REDUCERS
} from './plugin-registry';
import {SharedModule} from '../shared/shared.module';
import {NoComponentFound} from './no-component-found.component';
import {StoreModule} from '@ngrx/store';
import { DynoFormTypeDialog } from './core-components/dyno-form/dyno-form.component';

const COMPONENTS = [
  PluginLoaderComponent,
  PluginLoaderDirective,
  NoComponentFound,
  DynoFormTypeDialog,
  ...PluginComponents
];

export function getPluginReducers() {
  return PLUGIN_REDUCERS;
}

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    StoreModule.forFeature('plugins', PLUGIN_REDUCER_INJECTOR)
  ],
  exports: COMPONENTS,
  declarations: COMPONENTS,
  providers: [
    PluginsService,
    {
      provide: PLUGIN_REDUCER_INJECTOR,
      useFactory: getPluginReducers
    }
  ],
  entryComponents: [NoComponentFound, DynoFormTypeDialog, ...PluginComponents]
})
export class PluginsModule {
}
