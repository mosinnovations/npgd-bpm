export interface ILayoutConfig {
  colNumber: number;
  hAlignment: 'left' | 'center' | 'right';
  vAlignment: 'top' | 'center' | 'bottom';
}

export interface IFormConfig {
  layoutConfig: ILayoutConfig;
  componentConfig: {
    key: string;
  }
}
