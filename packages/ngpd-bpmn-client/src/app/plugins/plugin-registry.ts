import {DataTableComponent} from './gbm-components/data-table/data-table.component';
import {RenewalListingComponent} from './gbm-components/renewal-listing/renewal-listing.component';
import {DataTableConfigComponent} from './gbm-components/data-table/data-table-config/data-table-config.component';
import {DynoFormComponent} from './core-components/dyno-form/dyno-form.component';
import {dynoForm} from './core-components/dyno-form/dyno-form.reducer';
import {InjectionToken, Type} from '@angular/core';
import {ActionReducerMap} from '@ngrx/store';
import {dynamicFormConfig} from './core-components/dyno-form';
import {dataTableConfig} from './gbm-components/data-table/index';
import {renewalListConfig} from './gbm-components/renewal-listing/index';
import {DynoFormConfigComponent} from './core-components/dyno-form/dyno-form-config/dyno-form-config.component';
import {DynoFormPaletteComponent} from './core-components/dyno-form/dyno-form-config/form-palette/form-palette.component';
import {DynoFieldPaletteComponent} from './core-components/dyno-form/dyno-form-config/field-palette/field-palette.component';

export interface IPluginConfig {
  componentKey: string;
  component: Type<any>;
  configComponent?: Type<any>;
  allowConfig: boolean;
  actions: any[];
  reducers: any;
}

export const COMPONENTS = [
  DataTableComponent,
  DataTableConfigComponent,
  DynoFormComponent,
  DynoFormConfigComponent,
  DynoFormPaletteComponent,
  DynoFieldPaletteComponent,
  RenewalListingComponent
];

export const PLUGIN_CONFIGS: IPluginConfig[] = [
  dynamicFormConfig,
  dataTableConfig,
  renewalListConfig
];

export const PLUGIN_REDUCERS = PLUGIN_CONFIGS.reduce((reducerMap: any, config: IPluginConfig) => {
   return Object.assign({}, reducerMap, ...config.reducers);
}, {});

export const PLUGIN_REDUCER_INJECTOR = new InjectionToken<ActionReducerMap<any>>('Plugin Reducers');
