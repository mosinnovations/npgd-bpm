## BPM Platform Component Plugin Framework

### Overview

Plugin framework allows developers to create and plug UI components and their associated
reducers and actions into the framework's plugin registry to be accessed and configured as part of the
platform's designer. 

### Architecture

The plugin architecture takes advantage of Angular's dynamic component loading capabilities.

Significant components:

* Plugin Registry (plugin-registry.ts) - Holds all of the registered plugins. 
* Plugin Module (plugin.module.ts) - Loads plugins and all of their dependencies into the Angular context.
* Plugin Service (plugin.service.ts) - Service that returns a list of available plugins. 
* Plugin Loader (app-plugin-loader) - Loads the plugin at runtime.

Example

```html
<app-plugin-loader [pluginKey]="${your_component_key}"
                   [data]="currentComponentData"></app-plugin-loader>
      
```

#### Plugin Interface

To add a new component to the platform, the developer needs to tell the platform about the component she tries to 
add.  She would do this by defining a plugin configuration object and export it from the component's index.ts file. 

Here is an example of the configuration object:

```javascript
export const dataTableConfig: IPluginConfig = {
  componentKey: 'dataTable',
  component: DataTableComponent,
  configComponent: DataTableConfigComponent,
  allowConfig: true,
  actions: [],
  reducers: {
    dataTable
  }
};
```
Let's go through each of the attributes and its purposes:

* `componentKey` - the unique identifier the platform uses to load the proper plugin configuration for this component.
**Note: this ID must be unique**

* `component` - the component class to load for rendering.  **This is what the end users sees**

* `configComponent` (optional) - the associated configuration component that provides an UI for an application admin to configure the
component 

* `allowConfig` (optional, default = false) - tells the framework whether or not to allow admin to configure the compoennt at runtime.

* `actions` (optional, array of ngrx action objects) - defines the supported actions for this component

* `reducers` (optional, array of ngrx reducers) - defines the plugin state and reducers

#### Quick Start:

Let's walk through how to define a new plugin for the platform:

1. Create a package for your component under the appropriate domain folder, for example all GBMA related plugins are housed under
the folder *'gbm-components'* and all out-of-the-box core components are under *'core-components'*.

1. Create *'index.ts'* under the component package and export the component configuration as shown in the *'Plugin Interface'* section.

1. Implement your component with reducers and actions if applicable. 

1. Expose your component by editing the 'plugin-registry.ts' file and add your component class to the *'COMPONENTS'* array.  *(Note, this could
be derived from the pluginConfig interface but unfortunately, any function call during the declaration of Angular components breaks AOT)*

1. Launch the platform, design a flow and click on a user form to be able to select your component from the *'Component Type'* drop down. 

