import {IPluginConfig} from './plugin-registry';
import * as commons from 'ngpd-bpmn-commons';

export interface IPluginDataInput {
  currentFormId: string;
  displayMode?: 'single' | 'multiple';
  processDefinition: commons.IProcessDefinition;
}
export class PluginItem {
  constructor(public pluginConfig: IPluginConfig, public data?: IPluginDataInput) {}
}
