import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-no-component-found',
  template: `<div>No Component Found</div>`
})

export class NoComponentFound implements OnInit {
  constructor() {
  }

  ngOnInit() {
  }
}
