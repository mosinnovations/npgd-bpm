import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynoFormComponent } from './dyno-form.component';

describe('DynoFormComponent', () => {
  let component: DynoFormComponent;
  let fixture: ComponentFixture<DynoFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynoFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
