import {dynoForm} from './dyno-form.reducer';
import {IPluginConfig} from '../../plugin-registry';
import {DynoFormComponent} from './dyno-form.component';
import {DynoFormConfigComponent} from './dyno-form-config/dyno-form-config.component';

export const dynamicFormConfig: IPluginConfig = {
  componentKey: DynoFormComponent.componentKey,
  component: DynoFormComponent,
  configComponent: DynoFormConfigComponent,
  allowConfig: true,
  actions: [],
  reducers: {
    dynoForm
  }
};
