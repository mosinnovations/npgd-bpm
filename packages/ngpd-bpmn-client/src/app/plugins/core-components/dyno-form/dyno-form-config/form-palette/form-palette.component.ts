import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import * as commons from 'ngpd-bpmn-commons';
import {alignmentOptions, layoutOptions} from './form-palette.constant';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';
import {IFormConfig} from '../../../../types';


export interface IFormPaletteModel {
  layoutColumns: number;
  hAlignment: 'left' | 'center' | 'right';
  componentKey: string;
}

@Component({
  selector: 'app-dynamic-form-palette',
  templateUrl: './form-palette.component.html',
  styleUrls: ['./form-palette.component.scss']
})
export class DynoFormPaletteComponent implements OnInit, OnDestroy {

  @Input()
  set currentFormInfo(finfo: commons.IFormInfo) {
    this.formInfo = finfo;
    this.formConfig = finfo.componentConfig.configData as IFormConfig;
    console.log('+++++ form config = ', this.formConfig);
    this.selectedHAlignment = this.formConfig.layoutConfig.hAlignment;
    this.syncToForm(this.formConfig, finfo.componentConfig.componentKey);
  }

  @Output() onFormConfigChange: EventEmitter<commons.IComponentConfig>;

  public formInfo: commons.IFormInfo;
  public formConfig: IFormConfig;
  public layoutOptions: commons.ILookupItem[];
  public alignmentOptions: commons.ILookupItem[];
  public formGroup: FormGroup;
  public selectedHAlignment: string;

  private subscriptions: Subscription[];


  constructor(private fb: FormBuilder) {
    this.layoutOptions = layoutOptions;
    this.alignmentOptions = alignmentOptions;
    this.onFormConfigChange = new EventEmitter();
    this.subscriptions = [];
  }

  ngOnInit() {
    this.subscriptions.push(this.formGroup.valueChanges.debounce(() => Observable.timer(500))
      .subscribe((payload: IFormPaletteModel) => {
        if (this.formGroup.dirty) {
          const newComponentConfig: commons.IComponentConfig = {
            componentKey: payload.componentKey,
            configData: {
              layoutConfig: {
                colNumber: payload.layoutColumns,
                hAlignment: payload.hAlignment,
                vAlignment: 'center'
              }
            }
          };
          this.onFormConfigChange.emit(newComponentConfig);
        }
      }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }


  public isHAlignmentSelected(hAlignment: string): boolean {
    return this.selectedHAlignment === hAlignment;
  }

  public handleAlignmentChange(hAlignment: 'left' | 'center' | 'right') {
    this.selectedHAlignment = hAlignment;
    const currentFormValue = this.formGroup.value;
    const newComponentConfig: commons.IComponentConfig = {
      componentKey: currentFormValue.componentKey,
      configData: {
        layoutConfig: {
          colNumber: currentFormValue.layoutColumns,
          hAlignment: hAlignment,
          vAlignment: 'center'
        }
      }
    };
    this.onFormConfigChange.emit(newComponentConfig);
  }

  private syncToForm(fConfig: IFormConfig, compKey) {
    if (!this.formGroup) {
      this.formGroup = this.fb.group({
        componentKey: compKey,
        layoutColumns: fConfig.layoutConfig.colNumber,
        hAlignment: fConfig.layoutConfig.hAlignment
      });
    } else {
      this.formGroup.reset({
        componentKey: compKey,
        layoutColumns: fConfig.layoutConfig.colNumber,
        hAlignment: fConfig.layoutConfig.hAlignment
      });
    }
  }
}
