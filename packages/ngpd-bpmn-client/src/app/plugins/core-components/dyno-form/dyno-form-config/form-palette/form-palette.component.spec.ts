import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynoFormPaletteComponent as FormPaletteComponent } from './form-palette.component';

describe('FormPaletteComponent', () => {
  let component: FormPaletteComponent;
  let fixture: ComponentFixture<FormPaletteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormPaletteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormPaletteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
