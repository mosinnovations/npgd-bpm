import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynoFormConfigComponent } from './dyno-form-config.component';

describe('DynoFormConfigComponent', () => {
  let component: DynoFormConfigComponent;
  let fixture: ComponentFixture<DynoFormConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynoFormConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynoFormConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
