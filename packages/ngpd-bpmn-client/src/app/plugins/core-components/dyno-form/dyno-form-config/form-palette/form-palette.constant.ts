import * as commons from 'ngpd-bpmn-commons';

export const layoutOptions: commons.ILookupItem[] = [
  {
    label: 'One Column',
    value: 1
  },
  {
    label: 'Two Columns',
    value: 2
  }, {
    label: 'Three Columns',
    value: 3
  }, {
    label: 'Four Columns',
    value: 4
  },
];

export const alignmentOptions: commons.ILookupItem[] = [
  {
    label: 'Align Left',
    value: 'left',
    icon: 'format_align_left'
  },
  {
    label: 'Align Center',
    value: 'center',
    icon: 'format_align_center'
  },
  {
    label: 'Align Right',
    value: 'right',
    icon: 'format_align_right'
  }
];


