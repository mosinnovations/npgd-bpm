import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import * as commons from 'ngpd-bpmn-commons';
import {FormBuilder, FormGroup} from '@angular/forms';
import {generalUtils} from '../../../../../commons/app.utils';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';
import {MatSliderChange} from '@angular/material';
import {uiControls} from '../../../../../services/process-definition.constants';

export interface IFieldPaletteFormModel {
  uiType: string;
  label: string;
  width: number;
  options?: any[] | string;
}

@Component({
  selector: 'app-dyno-field-config',
  templateUrl: './field-palette.component.html',
  styleUrls: ['./field-palette.component.scss']
})
export class DynoFieldPaletteComponent implements OnInit, OnChanges, OnDestroy {

  @Input() field: commons.IFormField;
  @Output() onFormChange: EventEmitter<commons.IFormEvent<commons.IFormField>>;

  public form: FormGroup;

  public sliderConfig: any;
  private subscriptions: Subscription[];

  private currentWidth: number;

  constructor(private fb: FormBuilder) {
    this.sliderConfig = {
      max: 100,
      min: 25,
      step: 25,
      thumbLabel: true
    };
    this.subscriptions = [];
    this.currentWidth = 100;
    this.onFormChange = new EventEmitter();
  }

  ngOnInit() {
    this.setup(this.field);
    this.subscriptions.push(this.form.valueChanges.debounce(() => Observable.timer(500))
      .subscribe((payload: IFieldPaletteFormModel) => {
        // only emit if the form has been changed to avoid emitting first time loading
        if (this.form.dirty) {
          const options = (typeof payload.options === 'string')
            ? payload.options.split(',').map((val: string) => val.trim())
            : [];

          this.onFormChange.emit({
            meta: {
              isValid: this.form.valid
            },
            data: this.applyFormModelToField(this.field, Object.assign({}, payload, {
              width: this.currentWidth,
              options,
            }))
          });
        }
      }));
  }

  ngOnChanges(changes: SimpleChanges): void {
    const {
      field: fieldChange
    } = changes;

    if (fieldChange) {
      if (!this.form) {
        this.setup(fieldChange.currentValue);
      } else {
        this.form.setValue(this.fieldToFormModel(fieldChange.currentValue), {emitEvent: false});
        this.currentWidth = fieldChange.currentValue.controlConfig.width;
      }
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  public get uiControlOptions(): commons.ILookupItem[] {
    return uiControls[this.field.type];
  }

  public handleSliderChange(event: MatSliderChange) {
    this.onFormChange.emit({
      meta: {
        isValid: this.form.valid
      },
      data: this.applyFormModelToField(this.field, Object.assign({}, this.form.value, {
        width: event.value
      }))
    });
    this.currentWidth = event.value;
  }

  private setup(f: commons.IFormField) {
    this.form = this.fb.group(this.fieldToFormModel(f));
    this.currentWidth = this.form.value.width;
  }

  private fieldToFormModel(f: commons.IFormField) {
    if (f.controlConfig) {
      return {
        uiType: f.controlConfig.type,
        options: f.options,
        label: f.label,
        width: generalUtils.withDefault(f.controlConfig.width, 100)
      };
    }
    return {
      uiType: '',
      options: '',
      width: 100,
      label: f.label
    };
  }

  private applyFormModelToField(f: commons.IFormField, model: IFieldPaletteFormModel) {
    return Object.assign({}, f, {
      controlConfig: {
        type: model.uiType,
        width: model.width
      },
      options: model.options,
      label: model.label
    })
  }
}
