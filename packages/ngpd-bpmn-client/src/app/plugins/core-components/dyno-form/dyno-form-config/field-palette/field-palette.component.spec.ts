import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynoFieldPaletteComponent as FieldPaletteComponent } from './field-palette.component';

describe('FieldPaletteComponent', () => {
  let component: FieldPaletteComponent;
  let fixture: ComponentFixture<FieldPaletteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FieldPaletteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FieldPaletteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
