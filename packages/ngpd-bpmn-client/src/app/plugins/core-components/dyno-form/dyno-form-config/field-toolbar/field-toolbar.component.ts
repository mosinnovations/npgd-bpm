import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import * as commons from 'ngpd-bpmn-commons';

export interface IFieldOnClickEvent {
  type: 'edit' | 'hide';
  targetField: commons.IFormField;
}

@Component({
  selector: 'app-field-toolbar',
  templateUrl: './field-toolbar.component.html',
  styleUrls: ['./field-toolbar.component.scss']
})
export class FieldToolbarComponent implements OnInit {

  @Input() field: commons.IFormField;
  @Output() onClick: EventEmitter<IFieldOnClickEvent>;

  constructor() {
    this.onClick = new EventEmitter();
  }

  ngOnInit() {
  }

  public handleEdit() {
    this.onClick.emit({
      type: 'edit',
      targetField: this.field
    });
  }

}
