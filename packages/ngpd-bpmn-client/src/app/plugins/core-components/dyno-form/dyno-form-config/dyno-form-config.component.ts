import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import * as commons from 'ngpd-bpmn-commons';
import {IFormConfig} from '../../../types';

export interface IDynoFormConfig {
  type: 'form' | 'field';
  payload: IFormConfig | commons.IFormField;
}

@Component({
  selector: 'app-dyno-form-config',
  templateUrl: './dyno-form-config.component.html',
  styleUrls: ['./dyno-form-config.component.scss']
})
export class DynoFormConfigComponent implements OnInit {
  @Input() formInfo: commons.IFormInfo;
  @Input() currentFieldId: string;
  @Input() configMode: 'form' | 'field';
  @Output() onConfigChange: EventEmitter<IDynoFormConfig>;
  @Output() onConfigSave: EventEmitter<void>;


  constructor() {
    this.onConfigChange = new EventEmitter();
    this.onConfigSave = new EventEmitter();
  }

  ngOnInit() {
  }


  public get field() {
    if(!this.currentFieldId) {
      return null;
    }
    return this.formInfo.fields.find(fd => fd.id === this.currentFieldId);
  }

  public handleFieldConfigChange(fieldConfig: { meta: any, data: commons.IFormField}) {
    this.onConfigChange.emit({
      type: 'field',
      payload: fieldConfig.data
    });
  }

  public handleFormConfigChange(componentConfig: commons.IComponentConfig) {
    const formConfig = componentConfig.configData as IFormConfig;
    this.onConfigChange.emit({
      type: 'form',
      payload: formConfig
    });
  }

  public handleConfigSave() {
    this.onConfigSave.emit();
  }
}
