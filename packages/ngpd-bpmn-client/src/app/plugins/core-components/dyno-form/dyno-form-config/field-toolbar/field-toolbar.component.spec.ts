import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FieldToolbarComponent } from './field-toolbar.component';

describe('FieldToolbarComponent', () => {
  let component: FieldToolbarComponent;
  let fixture: ComponentFixture<FieldToolbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FieldToolbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FieldToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
