import {Component, EventEmitter, OnInit, Inject} from '@angular/core';
import {IPluggableComponent} from '../../IPluggable.component';
import {IPluginDataInput} from '../../plugin-item';
import * as commons from 'ngpd-bpmn-commons';
import * as immutable from 'immutable';
import {FormBuilder, FormGroup, FormControl} from '@angular/forms';
import {IDynoFormConfig} from './dyno-form-config/dyno-form-config.component';
import {IFormConfig} from '../../types';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

export interface IFormData {
  formGroup: FormGroup;
  formInfo: commons.IFormInfo;
}


@Component({
  selector: 'app-dyno-form',
  templateUrl: './dyno-form.component.html',
  styleUrls: ['./dyno-form.component.scss']
})
export class DynoFormComponent implements OnInit, IPluggableComponent {
  static readonly componentKey = 'dynoForm';

  public standaloneFlag = false;
  public forms: IFormData[];
  public displayMode: 'single' | 'multiple';
  public configMode: 'form' | 'field';
  public currentFormId: string;
  public currentFieldId: string;
  public onComponentConfigChange: EventEmitter<commons.IProcessDefinition>;
  private _data: IPluginDataInput;

  constructor(public dialog: MatDialog, private fb: FormBuilder) {
    this.forms = [];
    this.configMode = 'form';
    this.onComponentConfigChange = new EventEmitter();
    this.displayMode = 'multiple';
  }

  public get data(): IPluginDataInput {
    return this._data;
  }

  public set data(v: IPluginDataInput) {
    console.log(v);
    this._data = v;
    this.initializeForm();
  }

  ngOnInit() {
    this.initializeForm();
  }

  public initializeForm() {
    if (this.data) {
      this.currentFormId = this.data.currentFormId;
      this.constructFormData(this.data.processDefinition.formInfoList);
    }
  }

  public get formData(): IFormData {
    return this.getFormById(this.currentFormId);
  }

  public handleConfigChange(config: IDynoFormConfig, formId: string) {
    if (config.type === 'form') {
      this.handleFormConfigChange(config.payload as IFormConfig, formId);
    } else if(config.type === 'field') {
      this.handleFieldConfigChange(config.payload as commons.IFormField, formId);
    }
  }

  public handleShowFormConfig(formId: string) {
    this.currentFormId = formId;
    this.configMode = 'form';
  }

  public handleShowFieldConfig(fieldId: string) {
    this.currentFieldId = fieldId;
    this.configMode = 'field';
  }

  public handleConfigSave() {
    const newProcDef = Object.assign({}, this._data.processDefinition, {
      formInfoList: this.forms.map(fdata => fdata.formInfo)
    });
    this.onComponentConfigChange.emit(newProcDef);
  }

  public get displayFormEditor() {
    const mode = this.displayMode;
    const formId = this.currentFormId;
    const forms = this.forms;
    if (mode === 'single') {
      if (!formId) {
        return 'no-formid';
      } else if (!forms || forms.length === 0) {
        return 'no-form';
      } else if (forms.length === 1 &&
        (!forms[0].formInfo.fields ||
          forms[0].formInfo.fields.length === 0)) {
        return 'no-fields';
      }
    }
    return 'show';
  }

  public addField() {
    const dialogRef = this.dialog.open(DynoFormTypeDialog);

    dialogRef.afterClosed().subscribe(result => {
      if (!result) { return; }
      const formIndex = this.forms.findIndex(fdata => fdata.formInfo.formId === this.currentFormId);
      const formInfo = this.forms[formIndex].formInfo;
      const nextIndex = formInfo.fields.length;
      const newField: commons.IFormField = {
        label: result.name,
        id: this.currentFormId + '_form_field_' + nextIndex,
        type: result.type,
        controlConfig: {
          type: 'input',
          width: 100,
        },
      };

      const newFields = immutable.List(formInfo.fields).set(nextIndex, newField).toArray();
      const newFormInfo = Object.assign({}, formInfo, { fields: newFields });
      this.forms[formIndex].formGroup.addControl(newField.id, new FormControl());
      this.forms = immutable.List(this.forms).set(formIndex, Object.assign({}, this.forms[formIndex], { formInfo: newFormInfo })).toArray();
    });
  }

  private handleFormConfigChange(formConfig: IFormConfig, formId: string) {
    const formIndex = this.forms.findIndex(fdata => fdata.formInfo.formId === formId);
    const formInfo = this.forms[formIndex].formInfo;
    const newFormInfo = Object.assign({}, formInfo, {
      componentConfig: Object.assign({}, formInfo.componentConfig, {
        configData: formConfig
      })
    });
    this.forms = immutable.List(this.forms).set(formIndex,
      Object.assign({}, this.forms[formIndex], {
        formInfo: newFormInfo
      })).toArray();
  }

  private handleFieldConfigChange(field: commons.IFormField, formId: string) {
    const formIndex = this.forms.findIndex(fdata => fdata.formInfo.formId === formId);
    const formInfo = this.forms[formIndex].formInfo;
    const fieldIndex = formInfo.fields.findIndex(fd => fd.id === field.id);
    const newFields = immutable.List(formInfo.fields).set(fieldIndex,
      Object.assign({}, formInfo.fields[fieldIndex], field)).toArray();
    const newFormInfo = Object.assign({}, formInfo, {
      fields: newFields
    });
    this.forms = immutable.List(this.forms).set(formIndex,
      Object.assign({}, this.forms[formIndex], {
        formInfo: newFormInfo
      })).toArray();
  }

  /**
   * construct forms based on configration.
   *
   * @param {IFormInfo[]} formInfos
   * @param {string} formId
   */
  private constructFormData(formInfos: commons.IFormInfo[]) {
    this.forms = formInfos.filter((f: commons.IFormInfo) => {
      if (!this.currentFormId) {
        return true; // if no form id passed in just display all forms
      } else {
        if (f.formId === this.currentFormId) {
          return true;
        }
      }
      return false;
    }).map((f: commons.IFormInfo) => {
      const fieldConfig = f.fields.reduce((aggr, field: commons.IFormField) => {
        aggr[field.id] = [''];
        return aggr;
      }, {});
      return {
        formGroup: this.fb.group(fieldConfig),
        formInfo: f
      };
    });
  }

  public getFormLayoutHAlignment(form: commons.IFormInfo) {
    const hAlignment = form.componentConfig.configData.layoutConfig.hAlignment;
    switch (hAlignment) {
      case 'left':
        return 'center start';
      case 'center':
        return 'center center';
      default:
        return 'center end';
    }
  }

  public getLayoutConfig(fInfo: commons.IFormInfo) {
    const layoutConfig = fInfo.componentConfig.configData.layoutConfig;
    switch (layoutConfig.colNumber) {
      case 1:
        return {
          layoutType: 'column',
          layoutWrap: 'nowrap',
          fxFlex: '95'
        };
      case 2:
        return {
          layoutType: 'row',
          layoutWrap: 'wrap',
          fxFlex: '45'
        };
      case 3:
        return {
          layoutType: 'row',
          layoutWrap: 'nowrap',
          fxFlex: '29'
        };
      default:
        return {
          layoutType: 'row',
          layoutWrap: 'nowrap',
          fxFlex: '20'
        }
    }
  }

  private getFormById(formId: string): IFormData {
    return this.forms.find(f => f.formInfo.formId === formId);
  }
}

@Component({
  selector: 'dyno-form-type-dialog',
  template: `
  <mat-dialog-content>
    <mat-form-field [style.width]="'100%'">
      <mat-select placeholder="Data type" [(ngModel)]="type">
        <mat-option *ngFor="let option of dataOptions" [value]="option">
          {{ option }}
        </mat-option>
      </mat-select>
    </mat-form-field>
    <mat-form-field [style.width]="'100%'">
      <input matInput placeholder="Field Name" [(ngModel)]="name">
    </mat-form-field>
  </mat-dialog-content>
  <mat-dialog-actions>
    <button mat-button [disabled]="!type || !name" (click)="onClick()">Select</button>
  </mat-dialog-actions>
  `,
})
export class DynoFormTypeDialog {
  public dataOptions = ['string', 'long', 'date', 'boolean'];
  public type: string;
  public name: string;

  constructor( public dialogRef: MatDialogRef<DynoFormTypeDialog>, @Inject(MAT_DIALOG_DATA) public data: any) { }

  public onClick(): void {
    this.dialogRef.close({ type: this.type, name: this.name });
  }

}