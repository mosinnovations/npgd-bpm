import {Action} from '@ngrx/store';

export interface IDynoForm {
  formData: any[];
  selectedFormId: string;
  displayMode: 'single' | 'multiple';
}

const initialState = {
  formData: [],
  selectedFormId: null,
  displayMode: 'multiple'
};

function handleSelectedForm(state: any, selectedFormId: string) {
  return Object.assign({}, state, {
    selectedFormId
  });
}

export const dynoForm = (state: any = initialState, action: any) => {
  switch (action.type) {
    case 'SET_SELECTED_FORM':
      return handleSelectedForm(state, action.payload);
    default:
      return state;
  }
};
