import {Injectable} from '@angular/core';
import {PluginItem} from './plugin-item';
import {IPluginConfig, PLUGIN_CONFIGS} from './plugin-registry';

@Injectable()
export class PluginsService {

  public getPlugins() {
    return PLUGIN_CONFIGS.reduce((aggr: any, pluginConfig: IPluginConfig) => {
      if (!pluginConfig.componentKey) {
        throw new Error('Missing static field "key" for component');
      }
      aggr[pluginConfig.componentKey] = new PluginItem(pluginConfig, null);
      return aggr;
    }, {});
  }
}
