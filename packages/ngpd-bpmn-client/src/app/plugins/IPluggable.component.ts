import {IPluginDataInput} from './plugin-item';
import {EventEmitter} from '@angular/core';

export interface IPluggableComponent {
  data: IPluginDataInput;
  onComponentConfigChange: EventEmitter<any>;
}
