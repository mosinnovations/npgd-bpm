import { ActionReducer, ActionReducerMap, createFeatureSelector, createSelector, MetaReducer } from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';
import { environment } from '../../environments/environment';
import * as commons from 'ngpd-bpmn-commons';
import * as fromRouter from '@ngrx/router-store';
import {InjectionToken} from '@angular/core';
import * as fromErrors from './error-handling.reducer';

export interface IAppState extends commons.IAppState {
  routerReducer: fromRouter.RouterReducerState;
  errors: {
    [featureKey:string]: commons.IError[];
  };
  [key: string]: any;
}

export const REDUCER_TOKEN = new InjectionToken<ActionReducerMap<IAppState>>('root reducers');

export function getReducers(): ActionReducerMap<IAppState> {
  return {
    ...commons.commonReducers,
    routerReducer: fromRouter.routerReducer,
    errors: fromErrors.errors
  };
}

// meta reducers

// NOTE: do not use the arrow notation here 'const logger = () => etc', angular will blow up
export function logger(rdx: ActionReducer<IAppState>): ActionReducer<IAppState> {
  return (state: IAppState, action: any): IAppState => {
    console.log('state: ', state);
    console.log('action: ', action);
    return rdx(state, action);
  };
}

export const metaReducers: MetaReducer<IAppState>[] = !environment.production ? [logger, storeFreeze] : [];

export const getPageContextStateSelector = createFeatureSelector<commons.IPageContext>('pageContext');
export const getErrorsStateSelector = createFeatureSelector<{[featureKey: string]: commons.IError[]}>('errors');

export const getProcessDefinitionErrors = createSelector(
  getErrorsStateSelector,
  (errors: { [featureKey: string]: commons.IError[]}) => errors.processDefinition
);

export const getTopNavStateSelector = createSelector(
  getPageContextStateSelector,
  commons.getTopNavState
);

export const getSideNavStateSelector = createSelector(
  getPageContextStateSelector,
  (pageContext: commons.IPageContext) => pageContext.show.sideNav
);

export const getSideNavItemsSelector = createSelector(
  getPageContextStateSelector,
  (pageContext: commons.IPageContext) => pageContext.sideNavItems
);

export const getProcessDefinitionsSelector = createFeatureSelector<commons.IProcessDefinition[]>('processDefinitions');

export const getProcessDefinitionById = (id: string) => {
  return createSelector(
    getProcessDefinitionsSelector,
    (processDefinitions: commons.IProcessDefinition[]) => {
      return processDefinitions.find((p: commons.IProcessDefinition) => p._id === id);
    }
  );
};
