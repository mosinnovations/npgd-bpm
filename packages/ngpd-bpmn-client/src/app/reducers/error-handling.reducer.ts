import * as commons from 'ngpd-bpmn-commons';
import * as immutable from 'immutable';

const processDefinitionActions = commons.commonActions.processDefinitionActions;

const initialState: {
  [featureKey: string]: commons.IError[];
} = {};

function handleProcessDefinitionDeployFail(state: { [p: string]: commons.IError[] }, payload: commons.IError) {
  //const processDefErrors = state.processDefinition;
  // TODO: leaving this code commented out so that we can reference it later when implementing multiple errors in one snackbar
  // if (processDefErrors) {
  //   return Object.assign({}, state, {
  //     processDefinition: immutable.List(processDefErrors).push(payload).toArray()
  //   });
  // } else {

  //   });
  // }
  return Object.assign({}, state, {
    processDefinition: [payload]
  });
}

export const errors = (state: { [featureKey: string]: commons.IError[] } = initialState, action: any) => {
  switch (action.type) {
    case processDefinitionActions.DEPLOY_PROCESS_DEFINNITION_FAIL ||
    processDefinitionActions.CREATE_PROCESS_DEFINITION_FAIL ||
    processDefinitionActions.UPDATE_PROCESS_DEFINITIONS_FAIL:
      return handleProcessDefinitionDeployFail(state, action.payload);
    default:
      return state;
  }
};
