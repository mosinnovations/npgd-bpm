import {ProcessInstanceComponent} from './process-instance.component';

export const processInstanceRoutes = [
  {
    path: '',
    component: ProcessInstanceComponent
  }
];
