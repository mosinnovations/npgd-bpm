import {Component, Input, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import * as commons from 'ngpd-bpmn-commons';
import {Subscription} from 'rxjs/Subscription';

const processInstanceActions = commons.commonActions.processInstanceActions;

@Component({
  selector: 'app-process-instance',
  templateUrl: './process-instance.component.html',
  styleUrls: ['./process-instance.component.scss']
})
export class ProcessInstanceComponent implements OnInit {

  @Input() processDefinition: commons.IProcessDefinition;
  processInstances: commons.IProcessInstance[];

  private subscriptions: Subscription[];

  constructor(private store: Store<any>) {
    this.subscriptions = [];
  }

  ngOnInit() {
    this.subscriptions.push(this.store.select('processInstances').subscribe((piList: commons.IProcessInstance[]) => {
      this.processInstances = piList;
    }));
    this.store.dispatch(new processInstanceActions.GetProcessInstances(this.processDefinition.definitionId));
  }

  public getProcessInstanceKeys(pi: commons.IProcessInstance) {
    return Object.keys(pi).filter(k => k !== 'links' && k !== 'tasks');
  }

  public getTaskKeys(task: commons.ITask) {
    return Object.keys(task).filter(k => k !== 'formInfo');
  }

  public handleStartNewProcess() {
    if(this.processDefinition) {
      this.store.dispatch(new processInstanceActions.StartProcessInstance(this.processDefinition));
    }
  }

  public handleTaskComplete(task: commons.ITask) {
    if(task) {
      this.store.dispatch(new processInstanceActions.CompleteTask(task.id));
    }
  }

}
