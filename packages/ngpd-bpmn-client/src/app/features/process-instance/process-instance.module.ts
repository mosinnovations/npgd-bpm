import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProcessInstanceComponent} from './process-instance.component';
import {SharedModule} from '../../shared/shared.module';

const COMPONENTS = [
  ProcessInstanceComponent
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class ProcessInstanceModule { }
