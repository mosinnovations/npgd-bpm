import { AuthComponent } from './auth/auth.component';

export const authRoutes = [
  {
    path: '',
    component: AuthComponent
  }
];
