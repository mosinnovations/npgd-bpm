import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthComponent } from './auth/auth.component';
import { RouterModule } from '@angular/router';
import { authRoutes } from './auth.routes';

const COMPONENTS = [
  AuthComponent
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(authRoutes)
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class AuthModule { }
