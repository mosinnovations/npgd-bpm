import { Component, OnInit } from '@angular/core';
import { IAppState } from '../../../reducers/index';
import { Store } from '@ngrx/store';
import * as commons from 'ngpd-bpmn-commons';

const pageContextActions = commons.commonActions.pageContextActions;

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  constructor(private store: Store<IAppState>) {
    this.store.dispatch(new pageContextActions.HideTopNav());
  }

  ngOnInit() {
  }

}
