import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Store } from '@ngrx/store';
import * as commons from 'ngpd-bpmn-commons';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { getProcessDefinitionById, getProcessDefinitionsSelector } from '../../../reducers';
import { Subject } from 'rxjs/Subject';

const processDefinitionActions = commons.commonActions.processDefinitionActions;
const pageContextActions = commons.commonActions.pageContextActions;

@Component({
  selector: 'app-process-definition',
  templateUrl: './process-definitions.component.html',
  styleUrls: ['./process-definitions.component.less']
})
export class ProcessDefinitionsComponent implements OnInit, OnDestroy {

  public processDefinitions: commons.IProcessDefinition[];

  private subscriptions: Subscription[];


  constructor(public dialog: MatDialog,
              @Inject(ActivatedRoute) activeRoute: ActivatedRoute,
              private store: Store<commons.IAppState>) {
    this.subscriptions = [];
    this.processDefinitions = [];
    this.subscriptions.push(store.select(getProcessDefinitionsSelector).subscribe((pdList) => {
      this.processDefinitions = pdList;
    }));
  }

  ngOnInit() {
    this.store.dispatch(new processDefinitionActions.GetProcessDefinitions());
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  public handleOnProcessDefinitionSave(newProcessDefinition: commons.IProcessDefinition) {
    this.store.dispatch(new processDefinitionActions.CreateProcessDefinition(newProcessDefinition));
  }

  // public get currentProcessDefinition() {
  //   if (this.processDefinitions && this.selectedProcessDefinitionId) {
  //     return this.processDefinitions.find((p: commons.IProcessDefinition) => p._id === this.selectedProcessDefinitionId);
  //   }
  //   return null;
  // }
}
