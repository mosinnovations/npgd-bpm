import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import * as commons from 'ngpd-bpmn-commons';
import {MatDialog} from '@angular/material';
import {AddProcessDefinitionDialog} from '../add-process-definition-dialog/add-process-definition.dialog';
import {Subject} from 'rxjs/Subject';
import {Router} from '@angular/router';
import {IInfoCardConfig} from '../../../../components/info-card/info-card.component';

@Component({
  selector: 'app-process-definition-list',
  templateUrl: './process-definition-list.component.html',
  styleUrls: ['./process-definition-list.component.scss']
})
export class ProcessDefinitionListComponent implements OnInit {

  @Input() processDefinitionList: commons.IProcessDefinition[];

  @Output() onDialogConfirm: EventEmitter<commons.IProcessDefinition>;

  constructor(private dialog: MatDialog, private router: Router) {
    this.onDialogConfirm = new EventEmitter();
  }

  ngOnInit() {
  }

  public openAddProcessDefinitionDialog() {
    const dialogRef = this.dialog.open(AddProcessDefinitionDialog, {
      width: '700px',
      panelClass: 'app-dialog-container',
      disableClose: true,
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.onDialogConfirm.emit(result);
    });
  }

  public infoCardConfig(index: number): IInfoCardConfig {
    const p = this.processDefinitionList[index];
    return {
      headlineColor: 'red',
      headlineText: p.name,
      diagramData: p.configXml,
      id: p._id
    }
  }

  public handleEdit(index: number) {
    const processDefinition = this.processDefinitionList[index];
    this.router.navigate(['/process-definition/' + processDefinition._id], {queryParams: {section: 'general'}}).catch(e => {
      throw e;
    });
  }
}
