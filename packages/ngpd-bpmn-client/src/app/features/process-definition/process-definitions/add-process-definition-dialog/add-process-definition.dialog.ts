import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import * as commons from 'ngpd-bpmn-commons';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-add-process-def-dialog',
  templateUrl: './app-process-definition.dialog.html',
  styleUrls: [
    './add-process-definition.dialog.scss'
  ]
})
export class AddProcessDefinitionDialog implements OnInit, OnDestroy {

  public processDefinition: commons.IProcessDefinition;
  public isFormValid: boolean;

  private subscriptions: Subscription[];


  constructor(public dialogRef: MatDialogRef<AddProcessDefinitionDialog>,
              @Inject(MAT_DIALOG_DATA) public data: commons.IProcessDefinition) {
    this.processDefinition = data;
    this.isFormValid = false;
    this.subscriptions = [];
  }

  public ngOnInit(): void {
  }

  public ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  public handleOnBuild() {
    this.dialogRef.close(this.processDefinition);
  }

  public handleOnCancel() {
    this.dialogRef.close('close');
  }

  public handleOnGeneralFormChange(event: commons.IFormEvent<commons.IProcessDefinition>) {
      this.processDefinition = event.data;
      this.isFormValid = event.meta.isValid;
  }
}
