import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessDefinitionFlowComponent } from './process-definition-flow.component';

describe('ProcessDefinitionFlowComponent', () => {
  let component: ProcessDefinitionFlowComponent;
  let fixture: ComponentFixture<ProcessDefinitionFlowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessDefinitionFlowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessDefinitionFlowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
