import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import * as commons from 'ngpd-bpmn-commons';
import {Router} from '@angular/router';
import {MatDialog, MatDialogRef, MatDialogConfig} from '@angular/material';
import {ConfirmationComponent} from '../../../../components/confirmation/confirmation.component';
import {IBpmnEvent} from '../../../../components/bpm-viewer/bpm-viewer.component';
import {Subscription} from 'rxjs/Subscription';
import {IToolbarConfig} from '../../../../components/tool-bar/tool-bar.component';
import {IDefinitionFormChangeEvent} from '../process-definition-components/process-definition-components.component';

export interface IFlowChangeEvent {
  type: 'definitionFormChange' | 'deployFlow';
  data: IDefinitionFormChangeEvent;
}

@Component({
  selector: 'app-process-definition-flow',
  templateUrl: './process-definition-flow.component.html',
  styleUrls: ['./process-definition-flow.component.scss']
})
export class ProcessDefinitionFlowComponent implements OnInit, OnChanges, OnDestroy {

  @Input() processDefinition: commons.IProcessDefinition;
  @Output() onFlowChange: EventEmitter<IFlowChangeEvent>;

  public currentFormId: string;
  public topToolBarConfig: IToolbarConfig;

  private subscriptions: Subscription[];

  constructor(private router: Router, private dialog: MatDialog) {
    this.subscriptions = [];
    this.onFlowChange = new EventEmitter();
    this.topToolBarConfig = {
      show: true,
      items: [
        {
          type: 'action',
          icon: 'edit',
          tooltip: 'Edit Flow',
          execute: this.handleEdit.bind(this)
        },
        {
          type: 'action',
          icon: 'cloud_upload',
          tooltip: 'Deploy Flow',
          execute: this.handleDeploy.bind(this)
        }
      ]
    };
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  public get canvasId() {
    return 'canvas-flow-comp-' + this.processDefinition._id;
  }

  public get diagramXml() {
    return this.processDefinition.configXml;
  }

  public handleBpmEvent(event: IBpmnEvent) {
    // this.currentFormId = event.id;
    if (event.type === 'bpmn:UserTask') {
      const dialogRef = this.openModal('Edit Form Data?');
      dialogRef.afterClosed().subscribe((confirmed) => {
        if (confirmed) {
          this.router.navigate(['/form-builder'], { queryParams: { id: event.id, processId: this.processDefinition._id } });
        }
      });
    }
  }

  public handleEdit() {
    this.router.navigate(['/process-modeler/' + this.processDefinition._id]);
  }

  public handleDeploy() {
    const dialogRef = this.openModal('Deploy?');

    dialogRef.afterClosed().subscribe((confirmed) => {
      if (confirmed) {
          this.onFlowChange.emit({
            type: 'deployFlow',
            data: confirmed
          });
      }
    });
  }

  private openModal(message: string): MatDialogRef<ConfirmationComponent, any> {
    return this.dialog.open(ConfirmationComponent, {
      width: '250px',
      data: {
        message
      }
    });
  }
}
