import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessDefinitionDetailsComponent } from './process-definition-details.component';

describe('ProcessDefinitionDetailsComponent', () => {
  let component: ProcessDefinitionDetailsComponent;
  let fixture: ComponentFixture<ProcessDefinitionDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessDefinitionDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessDefinitionDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
