import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessDefinitionComponentsComponent } from './process-definition-components.component';

describe('ProcessDefinitionFormsComponent', () => {
  let component: ProcessDefinitionComponentsComponent;
  let fixture: ComponentFixture<ProcessDefinitionComponentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessDefinitionComponentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessDefinitionComponentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
