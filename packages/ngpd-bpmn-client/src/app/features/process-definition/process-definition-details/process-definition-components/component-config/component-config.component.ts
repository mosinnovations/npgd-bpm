import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import * as commons from 'ngpd-bpmn-commons';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';
import { componentTypeOptions } from './component-config.constant';
import {IFormConfig} from '../../../../../plugins/types';


export interface IFormPaletteModel {
  layoutColumns: number;
  hAlignment: 'left' | 'center' | 'right';
  componentKey: string;
}

@Component({
  selector: 'app-component-config',
  templateUrl: './component-config.component.html',
  styleUrls: ['./component-config.component.scss']
})
export class ComponentConfigComponent implements OnInit, OnDestroy {

  @Input()
  set currentFormInfo(finfo: commons.IFormInfo) {
    this.formInfo = finfo;
    this.formConfig = finfo.componentConfig;
    this.syncToForm(this.formConfig);
  }

  @Output() onFormConfigChange: EventEmitter<commons.IComponentConfig>;

  public formInfo: commons.IFormInfo;
  public formConfig: commons.IComponentConfig;

  public componentTypeOptions: commons.ILookupItem[];
  public formGroup: FormGroup;

  private subscriptions: Subscription[];


  constructor(private fb: FormBuilder) {
    this.componentTypeOptions = componentTypeOptions;
    this.onFormConfigChange = new EventEmitter();
    this.subscriptions = [];
  }

  ngOnInit() {
    this.subscriptions.push(this.formGroup.valueChanges.debounce(() => Observable.timer(500))
      .subscribe((payload: IFormPaletteModel) => {
        if (this.formGroup.dirty) {
          const newCompConfig: commons.IComponentConfig = {
            componentKey: payload.componentKey,
            configData: {
              layoutConfig: {
                colNumber: payload.layoutColumns,
                hAlignment: payload.hAlignment,
                vAlignment: 'center'
              }
            }
          };
          this.onFormConfigChange.emit(newCompConfig);
        }
      }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  private syncToForm(compConfig: commons.IComponentConfig) {
    const fConfig = compConfig.configData as IFormConfig;
    if (!this.formGroup) {
      this.formGroup = this.fb.group({
        componentKey: compConfig.componentKey,
        layoutColumns: fConfig.layoutConfig.colNumber,
        hAlignment: fConfig.layoutConfig.hAlignment
      });
    } else {
      this.formGroup.reset({
        componentKey: fConfig.componentConfig.key,
        layoutColumns: fConfig.layoutConfig.colNumber,
        hAlignment: fConfig.layoutConfig.hAlignment
      });
    }
  }
}
