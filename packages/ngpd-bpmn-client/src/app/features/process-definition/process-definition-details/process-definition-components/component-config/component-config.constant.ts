import * as commons from 'ngpd-bpmn-commons';

export const componentTypeOptions: commons.ILookupItem[] = [
  {
    label: 'Dynamic Form Builder',
    value: 'dynoForm'
  },
  {
    label: 'Data Table',
    value: 'dataTable'
  },
  {
    label: 'Renewal Listing',
    value: 'renewalListing'
  }
];
