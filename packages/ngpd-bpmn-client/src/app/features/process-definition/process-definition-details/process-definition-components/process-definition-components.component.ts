import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import * as commons from 'ngpd-bpmn-commons';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ICardHeader} from '../../../../components/stat-card/stat-card.component';
import {MatSidenav} from '@angular/material';
import {Subscription} from 'rxjs/Subscription';
import {IPluginDataInput} from '../../../../plugins/plugin-item';
import * as immutable from 'immutable';

export interface IFormData {
  formGroup: FormGroup;
  formInfo: commons.IFormInfo;
}

export interface ILayoutChange {
}

export interface IOrderChange {
}

export interface IDefinitionFormChangeEvent {
  type: 'fieldPropertyChange' | 'formConfigChange';
  formId: string;
  data: commons.IFormField | ILayoutChange | IOrderChange | commons.IProcessDefinition;
}

@Component({
  selector: 'app-process-definition-components',
  templateUrl: './process-definition-components.component.html',
  styleUrls: ['./process-definition-components.component.scss']
})
export class ProcessDefinitionComponentsComponent implements OnInit, OnChanges, OnDestroy {

  @Input() processDefinition: commons.IProcessDefinition;
  @Input() formId: string;
  @Input() mode: 'single' | 'multiple';
  @Output() onDefinitionFormsChange: EventEmitter<commons.IProcessDefinition>;

  @ViewChild(MatSidenav) sideNav: MatSidenav;

  public forms: IFormData[];
  public cardHeaders: ICardHeader[];
  public selectedField: commons.IFormField;
  public selectedFormId: string;

  private subscriptions: Subscription[];

  constructor(private fb: FormBuilder) {
    this.cardHeaders = [];
    this.subscriptions = [];
    this.forms = [];
    this.mode = 'multiple';
    this.onDefinitionFormsChange = new EventEmitter();
  }

  public ngOnInit() {
    this.constructCardHeader(this.processDefinition.formInfoList);
    this.constructFormData(this.processDefinition.formInfoList);
    this.selectedFormId = this.formId;
  }

  public ngOnChanges(changes: SimpleChanges): void {
    const {
      formId: newFormId,
      processDefinition: newProcessDefinition
    } = changes;
    if (newFormId) {
      this.constructFormData(this.processDefinition.formInfoList, newFormId.currentValue);
      // reset selected fields
      this.selectedField = null;
      this.selectedFormId = newFormId.currentValue;
    }
    if (newProcessDefinition) {
      const newProc = newProcessDefinition.currentValue as commons.IProcessDefinition;
      this.constructFormData(newProc.formInfoList, this.formId);
    }
  }

  public ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  public get currentPluginData(): IPluginDataInput {
    return {
      currentFormId: this.selectedFormId,
      processDefinition: this.processDefinition,
    }
  }

  public get selectedFormInfo() {
    if (this.forms && this.selectedFormId) {
      return this.forms.find(fdata => {
        const fInfo = fdata.formInfo;
        return fInfo.formId === this.selectedFormId;
      }).formInfo;
    }
    return null;
  }

  public get displayFormEditor() {
    if (this.mode === 'single') {
      if (!this.formId) {
        return 'no-formid';
      } else if (!this.forms || this.forms.length === 0) {
        return 'no-form';
      } else if (this.forms.length === 1 &&
        (!this.forms[0].formInfo.fields ||
          this.forms[0].formInfo.fields.length === 0)) {
        return 'no-fields';
      }
    }
    return 'show';
  }

  public getComponentKey(formInfo: commons.IFormInfo) {
    const componentConfig = formInfo.componentConfig;
    if (componentConfig.componentKey) {
      return componentConfig.componentKey;
    } else {
      return 'dynoForm';
    }
  }


  public handleOnConfigChange(newProcDef: commons.IProcessDefinition) {
    console.log('here in handle event changes event = ', newProcDef);
    this.onDefinitionFormsChange.emit(newProcDef);
  }


  public handleComponentChange(componentConfig: commons.IComponentConfig) {
    const formInfoIndex = this.processDefinition.formInfoList.findIndex(f => f.formId === this.formId);
    const newFormInfos = immutable.List(this.processDefinition.formInfoList).set(formInfoIndex,
      Object.assign({}, this.processDefinition.formInfoList[formInfoIndex], {
        componentConfig
      })).toArray();
    const newProcDef = Object.assign({}, this.processDefinition, {
      formInfoList: newFormInfos
    });
    this.onDefinitionFormsChange.emit(newProcDef);
  }

  /**
   * construct forms based on configration.
   *
   * @param {IFormInfo[]} formInfos
   * @param {string} formId
   */
  private constructFormData(formInfos: commons.IFormInfo[], formId?: string) {
    this.forms = formInfos.filter((f: commons.IFormInfo) => {
      if (!this.formId) {
        return true; // if no form id passed in just display all forms
      } else {
        if (f.formId === this.formId) {
          return true;
        }
      }
      return false;
    }).map((f: commons.IFormInfo) => {
      const fieldConfig = f.fields.reduce((aggr, field: commons.IFormField) => {
        aggr[field.id] = [''];
        return aggr;
      }, {});
      return {
        formGroup: this.fb.group(fieldConfig),
        formInfo: f
      };
    });
  }

  private constructCardHeader(formInfo: commons.IFormInfo[]) {
    this.cardHeaders = formInfo.map((fi: commons.IFormInfo) => {
      return {
        backgroundColor: 'red',
        icon: 'edit'
      }
    });
  }
}
