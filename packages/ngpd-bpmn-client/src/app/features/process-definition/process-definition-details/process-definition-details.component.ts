import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import * as commons from 'ngpd-bpmn-commons';
import {Store} from '@ngrx/store';
import {getProcessDefinitionById, getProcessDefinitionErrors} from '../../../reducers';
import {Subscription} from 'rxjs/Subscription';
import {ActivatedRoute} from '@angular/router';
import {ISideNavConfig} from '../../../components/side-nav/side-nav.component';
import {ITopNavConfig} from '../../../components/top-nav/top-nav.component';
import {IDefinitionFormChangeEvent} from './process-definition-components/process-definition-components.component';
import * as immutable from 'immutable';
import {IFlowChangeEvent} from './process-definition-flow/process-definition-flow.component';
import {MatSnackBar} from '@angular/material';
import {ErrorSnackbarComponent} from '../../../components/error-snackbar/error-snackbar.component';

const processDefinitionActions = commons.commonActions.processDefinitionActions;
const pageContextActions = commons.commonActions.pageContextActions;

@Component({
  selector: 'app-process-definition-details',
  templateUrl: './process-definition-details.component.html',
  styleUrls: ['./process-definition-details.component.scss']
})
export class ProcessDefinitionDetailsComponent implements OnInit, OnDestroy {
  public section: 'general' | 'flow' | 'forms';


  public processDefinition: commons.IProcessDefinition;
  public sideNavConfig: ISideNavConfig;
  public topNavConfig: ITopNavConfig;
  public topToolBarConfig: ITopNavConfig;

  private subscriptions: Subscription[];
  private commonTopNavItems: commons.INavItem[];

  constructor(private store: Store<commons.IAppState>,
              private snackBar: MatSnackBar,
              private route: ActivatedRoute) {
    this.subscriptions = [];
    this.commonTopNavItems = [
      {
        icon: 'home',
        type: 'link',
        path: '/process-definition'
      }
    ];
    this.subscriptions.push(this.store.select(getProcessDefinitionErrors).subscribe((errors: commons.IError[]) => {
      if (errors) {
        this.snackBar.openFromComponent(ErrorSnackbarComponent, {
          data: errors
        });
      }
    }));
  }

  ngOnInit() {
    this.route.queryParams.subscribe(queryParams => {
      this.section = queryParams.section;
    });
    this.route.params.subscribe((params) => {
      const id = params.id;
      this.store.dispatch(new processDefinitionActions.GetProcessDefinitionById(id));
      this.subscriptions.push(
        this.store.select(getProcessDefinitionById(id))
          .subscribe((processDef: commons.IProcessDefinition) => {
            this.processDefinition = processDef;
            if (this.processDefinition) {
              this.constructTopNav();
              this.constructSideNav();
              this.topToolBarConfig = null;
            }
          })
      );
    });
    this.store.dispatch(new pageContextActions.ShowTopNav());
    this.store.dispatch(new pageContextActions.ShowSideNav());
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  public get sectionToDisplay() {
    if (!this.section) {
      return 'general';
    }
    return this.section;
  }

  public handleOnGeneralFormChange(event: commons.IFormEvent<commons.IProcessDefinition>) {
    console.log('general form event = ', event);
  }

  public handleFlowChange(event: IFlowChangeEvent) {
    switch (event.type) {
      case 'definitionFormChange':
        const newProcDef = event.data as any;
        this.handleDefinitionFormChange(newProcDef);
        break;
      case 'deployFlow':
        this.handleFlowDeploy();
        break;
      default:
        break;
    }
  }

  public handleDefinitionFormChange(newProcDef: commons.IProcessDefinition) {
    const payload: commons.IUpdateProcessDefinitionPayload = {
      updateFrom: 'definitionForms',
      data: newProcDef
    };
    this.store.dispatch(new processDefinitionActions.UpdateProcessDefinition(payload));
  }

  private handleFlowDeploy() {
    this.store.dispatch(new processDefinitionActions.DeployProcessDefinition(this.processDefinition._id));
  }

  private constructSideNav() {
    const path = '/process-definition/' + this.processDefinition._id;
    this.sideNavConfig = {
      show: true,
      sideNavItems: [
        {
          label: 'General Information',
          target: {
            path,
            queryParams: {section: 'general'}
          }
        },
        {
          label: 'Business Flow',
          target: {
            path,
            queryParams: {section: 'flow'}
          }
        },
        {
          label: 'Flow Preview',
          target: {
            path,
            queryParams: {section: 'forms'}
          }
        },
        {
          label: 'Flow Instances',
          target: {
            path,
            queryParams: {section: 'instances'}
          }
        }
      ]
    }
  }

  private constructTopNav() {
    this.topNavConfig = {
      title: this.processDefinition.name,
      show: true,
      items: this.commonTopNavItems
    };
  }
}
