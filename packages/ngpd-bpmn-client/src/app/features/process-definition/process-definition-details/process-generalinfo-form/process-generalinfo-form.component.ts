import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as commons from 'ngpd-bpmn-commons';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-process-generalinfo-form',
  templateUrl: './process-generalinfo-form.component.html',
  styleUrls: ['./process-generalinfo-form.component.scss']
})
export class ProcessGeneralinfoFormComponent implements OnInit, OnDestroy {

  @Input() processDefinition: commons.IProcessDefinition;
  @Input() mode: 'dialog' | 'embedded';
  @Output() onformChange: EventEmitter<commons.IFormEvent<commons.IProcessDefinition>>;

  private subscription: Subscription[];


  public form: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.subscription = [];
    this.onformChange = new EventEmitter();
  }

  public get displayMode() {
    if(!this.mode) {
      return 'embedded';
    }
    return this.mode;
  }

  public ngOnInit() {
    this.form = this.formBuilder.group({
      name: [this.processDefinition.name, Validators.required],
      _id: this.processDefinition._id,
      description: this.processDefinition.description
    });

    this.subscription.push(this.form.valueChanges.debounce(() => Observable.timer(500))
      .subscribe((payload: commons.IProcessDefinition) => {
        if (this.form.dirty) {
          this.onformChange.emit({
            meta: {
              isValid: this.form.valid
            },
            data: payload
          })
        }
      }));
  }

  public ngOnDestroy(): void {
    this.subscription.forEach(s => s.unsubscribe());
  }

  public get isFormValid() {
    return this.form.valid;
  }
}
