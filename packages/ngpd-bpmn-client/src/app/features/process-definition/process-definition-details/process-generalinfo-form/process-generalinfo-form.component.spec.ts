import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessGeneralinfoFormComponent } from './process-generalinfo-form.component';

describe('ProcessGeneralinfoFormComponent', () => {
  let component: ProcessGeneralinfoFormComponent;
  let fixture: ComponentFixture<ProcessGeneralinfoFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessGeneralinfoFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessGeneralinfoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
