import { ProcessDefinitionDetailsComponent } from './process-definition-details/process-definition-details.component';
import {ProcessDefinitionsComponent} from './process-definitions/process-definitions.component';
import {ProcessDefinitionFlowComponent} from './process-definition-details/process-definition-flow/process-definition-flow.component';

export const processDefinitionRoutes = [
  {
    path: '',
    component: ProcessDefinitionsComponent
  },
  {
    path: 'process-definition/:id',
    component: ProcessDefinitionDetailsComponent
  }
];
