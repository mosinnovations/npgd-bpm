import {NgModule} from '@angular/core';
import {ProcessDefinitionsComponent} from './process-definitions/process-definitions.component';
import {processDefinitionRoutes} from './process-definition.routes';
import {RouterModule} from '@angular/router';
import {AddProcessDefinitionDialog} from './process-definitions/add-process-definition-dialog/add-process-definition.dialog';
import {FormsModule} from '@angular/forms';
import {ProcessDefinitionDetailsComponent} from './process-definition-details/process-definition-details.component';
import {ProcessDefinitionListComponent} from './process-definitions/process-definition-list/process-definition-list.component';
import {ProcessGeneralinfoFormComponent} from './process-definition-details/process-generalinfo-form/process-generalinfo-form.component';
import {ProcessDefinitionFlowComponent} from './process-definition-details/process-definition-flow/process-definition-flow.component';
import {ConfirmationComponent} from '../../components/confirmation/confirmation.component';
import {ProcessDefinitionComponentsComponent} from './process-definition-details/process-definition-components/process-definition-components.component';
import {ErrorSnackbarComponent} from '../../components/error-snackbar/error-snackbar.component';
import {DndModule} from 'ng2-dnd';
import {PluginsModule} from '../../plugins/plugins.module';
import {SharedModule} from '../../shared/shared.module';
import {ComponentConfigComponent} from './process-definition-details/process-definition-components/component-config/component-config.component';
import {ProcessInstanceModule} from '../process-instance/process-instance.module';

const COMPONENTS = [
  AddProcessDefinitionDialog,
  ComponentConfigComponent,
  ProcessDefinitionsComponent, // container
  ProcessDefinitionListComponent,
  ProcessDefinitionDetailsComponent,
  ProcessDefinitionFlowComponent,
  ProcessDefinitionComponentsComponent,
  ProcessGeneralinfoFormComponent,
];

@NgModule({
  imports: [
    FormsModule,
    PluginsModule,
    ProcessInstanceModule,
    SharedModule,
    DndModule.forRoot(),
    RouterModule.forChild(processDefinitionRoutes),
  ],
  entryComponents: [
    AddProcessDefinitionDialog,
    ConfirmationComponent,
    ErrorSnackbarComponent,
//    ...pluginClasses
  ],
  providers: [
    // PluginsService
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class ProcessDefinitionModule {
}
