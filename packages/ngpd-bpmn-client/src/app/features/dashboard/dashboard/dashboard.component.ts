import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { IAppState } from '../../../reducers/index';

import * as commons from 'ngpd-bpmn-commons';

const pageContextActions = commons.commonActions.pageContextActions;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private store: Store<IAppState>) {
    this.store.dispatch(new pageContextActions.ShowTopNav());
  }

  ngOnInit() {
  }

}
