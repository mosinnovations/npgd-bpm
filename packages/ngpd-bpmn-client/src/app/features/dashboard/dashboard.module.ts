import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RouterModule } from '@angular/router';
import { dashboardRoutes } from './dashboard.route';
import { MatButtonModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ComponentModule } from '../../components/component.module';

const COMPONENTS = [
  DashboardComponent
];

const MATERIAL_COMPONENTS = [
  MatButtonModule
];

@NgModule({
  imports: [
    CommonModule,
    ComponentModule,
    FlexLayoutModule,
    ...MATERIAL_COMPONENTS,
    RouterModule.forChild(dashboardRoutes)
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class DashboardModule { }
