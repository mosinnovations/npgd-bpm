import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProcessModelerComponent } from './process-modeler/process-modeler.component';
import { processModelerRoutes } from './process-modeler.route';
import { RouterModule } from '@angular/router';
import {MatButtonModule, MatIconModule, MatToolbarModule} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';

const COMPONENTS = [
  ProcessModelerComponent
];

const MATERIAL_COMPONENTS = [
  MatButtonModule,
  MatIconModule,
  MatToolbarModule
];


@NgModule({
  imports: [
    CommonModule,
    ...MATERIAL_COMPONENTS,
    FlexLayoutModule,
    RouterModule.forChild(processModelerRoutes),
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class ProcessModelerModule {
}
