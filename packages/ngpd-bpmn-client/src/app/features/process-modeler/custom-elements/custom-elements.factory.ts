import { ElementFactory as BpmnElementFactory } from 'bpmn-js/lib/features/modeling';
import { DEFAULT_LABEL_SIZE } from 'bpmn-js/lib/util/LabelUtil';

export class CustomElementFactory extends BpmnElementFactory {

  public $inject = ['bpmnFactory', 'moddle'];

  constructor() {
    super();
  }

  public create(elementType: string, attrs: any, parent?: any) {
    const attrType = attrs.type as string;
    if (elementType === 'label') {
      return super.baseCreate(elementType, Object.assign({}, {
        type: 'label'
      }, DEFAULT_LABEL_SIZE, attrs));
    }

    if (/^custom\:/.test(attrType)) {
      if (!attrs.businessObject) {
        attrs.businessObject = {
          type: attrType
        };
      }

      if (attrs.id) {
        attrs.businessObject = Object.assign(attrs.businessObject, {
          id: attrs.id
        });
      }

      if (/\:connection$/.test(attrType)) {
        attrs = Object.assign(attrs, super._getCustomElementSize(attrType));
      }

      return super.baseCreate(elementType, attrs);
    }

    return super.createBpmnElement(elementType, attrs);
  }


  protected _getCustomElementSize(type: string) {
    const shapes = {
      __default: { width: 100, height: 80 },
      'custom:triangle': { width: 40, height: 40 },
      'custom:circle': { width: 140, height: 140 }
    };

    return shapes[type] || shapes.__default;
  }
}
