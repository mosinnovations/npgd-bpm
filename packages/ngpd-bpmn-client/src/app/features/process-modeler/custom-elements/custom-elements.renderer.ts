import BaseRenderer from 'diagram-js/lib/draw/BaseRenderer';
import svgAppend from 'tiny-svg/lib/append';
import svgAttr from 'tiny-svg/lib/attr';
import svgCreate from 'tiny-svg/lib/create';
import * as RenderUtil from 'diagram-js/lib/util/RenderUtil';


export class CustomRenderer extends BaseRenderer {

  public $inject = ['eventBus', 'styles'];

  constructor(eventBus: any, styles: any) {
    super(eventBus, 2000);
  }

  public drawTriangle(p: any, side: number) {
    let points, attrs;
    const halfSide = side / 2;
    const computeStyle = this.styles.computeStyle;
    points = [halfSide, 0, side, side, 0, side];
    attrs = computeStyle(attrs, {
      stroke: '#3CAA82',
      strokeWidth: 2,
      fill: '#3CAA82'
    });
    const polygon = svgCreate('polygon');
    svgAttr(polygon, {
      points
    });
    svgAttr(polygon, attrs);
    svgAppend(p, polygon);
    return polygon;
  }

  public getTrianglePath(element: any) {
    const x = element.x;
    const y = element.y;
    const width = element.width;
    const height = element.height;

    const trianglePath = [
      ['M', x + width / 2, y],
      ['l', width / 2, height],
      ['l', -width, 0],
      ['z']
    ];

    return RenderUtil.componentsToPath(trianglePath);
  }

  public drawCircle(p: any, width: number, height: number) {
    const cx = width / 2;
    const cy = height / 2;
    const computeStyle = this.styles.computeStyle;
    let attrs;
    attrs = computeStyle(attrs, {
      stroke: '#4488aa',
      strokeWidth: 4,
      fill: 'white'
    });
    let circle;
    circle = svgCreate(circle);
    svgAttr(circle, {
      cx,
      cy,
      r: Math.round((width + height) / 4)
    });

    svgAttr(circle, attrs);
    svgAppend(p, circle);

    return circle;
  }

  public getCirclePath(shape: any) {
    const cx = shape.x + shape.width / 2;
    const cy = shape.y + shape.height / 2;
    const radius = shape.width / 2;

    const circlePath = [
      ['M', cx, cy],
      ['m', 0, -radius],
      ['a', radius, radius, 0, 1, 1, 0, 2 * radius],
      ['a', radius, radius, 0, 1, 1, 0, -2 * radius],
      ['z']
    ];

    return RenderUtil.componentsToPath(circlePath);
  }

  public drawCustomConnection(p: any, element: any) {
    let attrs;
    attrs = this.styles.computeStyle(attrs, {
      stroke: '#ff471a',
      strokeWidth: 2
    });

    return svgAppend(p, RenderUtil.createLine(element.waypoints, attrs));
  }

  public getCustomConnectionPath(connection: any) {
    const waypoints = connection.waypoints.map(function (p) {
      return p.original || p;
    });

    const connectionPath = [
      ['M', waypoints[0].x, waypoints[0].y]
    ];

    waypoints.forEach(function (waypoint, index) {
      if (index !== 0) {
        connectionPath.push(['L', waypoint.x, waypoint.y]);
      }
    });

    return RenderUtil.componentsToPath(connectionPath);
  }

  public getConnectionPath(connection: any) {
    const type = connection.type;
    if (type === 'custom:connection') {
      return this.getCustomConnectionPath(connection);
    }
  }

  public canRender(element: any) {
    return /^custom\:/.test(element.type);
  }

  public drawShape(p: any, element: any) {
    const type = element.type;
    if (type === 'custom:triangle') {
      return this.drawTriangle(p, element.width);
    }

    if (type === 'custom:circle') {
      return this.drawCircle(p, element.width, element.height);
    }
  }

  public drawConnection(p, element) {
    const type = element.type;
    if (type === 'custom:connection') {
      return this.drawCustomConnection(p, element);
    }
  }


}
