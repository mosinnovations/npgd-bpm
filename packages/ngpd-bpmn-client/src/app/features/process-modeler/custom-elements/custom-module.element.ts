import { CustomElementFactory } from './custom-elements.factory';

export interface IBPMNJsModule {
  __init__: string[];
  elementFactory: any[];
  customRenderer: any[];
  paletteProvider: any[];
  customRules: any[];
  customUpdater: any[];
  contextPadProvider: any[];
}

export const customModule: IBPMNJsModule = {
  __init__: [
    'customRenderer',
    'paletteProvider',
    'customRules',
    'customUpdater',
    'contextPadProvider'
  ],
  elementFactory: [ 'type', CustomElementFactory],
  customRenderer: [],
  paletteProvider: [],
  customRules: [],
  customUpdater: [],
  contextPadProvider: []
};

