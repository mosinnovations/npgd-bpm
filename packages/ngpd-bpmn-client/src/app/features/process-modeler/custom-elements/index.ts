import Modeler = require('bpmn-js/lib/Modeler');
import * as _ from 'lodash';
import { CustomElementFactory } from './custom-elements.factory';

export class CustomModeler extends Modeler {

  private _customElements: any[];

  static isCustomConnection(element: any): any {
    return element.type === 'custom:connection';
  }

  constructor(config: any) {
    super(config);
    this._customElements = [];
  }

  public addCustomElements(customElements: any[]) {
    if (!_.isArray(customElements)) {
      throw new Error('argument must be an array');
    }

    const shapes = [], connections = [];

    customElements.forEach((customElement: any) => {
      if (CustomModeler.isCustomConnection(customElement)) {
        connections.push(customElement);
      } else {
        shapes.push(customElement);
      }
    });

    shapes.forEach(this.addCustomShape, this);
  }

  private addCustomShape(customElement: any) {
    this._customElements.push(customElement);

    const canvas = this.get('canvas');
    const elementFactory = this.get('elementFactory') as CustomElementFactory;
    const customAttrs = Object.assign({
      businessObject: customElement
    }, customElement);
    const customShape = elementFactory.create('shape', customAttrs);
    return canvas.addShape(customShape);
  }

  private addCustomConnection(customConnection: any) {
    this._customElements.push(customConnection);
    const canvas = this.get('canvas');
    const elementFactory = this.get('elementFactory') as CustomElementFactory;
    const elementRegistry = this.get('elementRegistry') as any;

    const customAttrs = Object.assign({
      businessObject: customConnection
    }, customConnection);

    const connection = elementFactory.create(
      'connection',
      Object.assign(customAttrs, {
        source: elementRegistry.get(customConnection.source),
        target: elementRegistry.get(customConnection.target)
      }),
      elementRegistry.get(customConnection.source).parent
    );

    return canvas.addConnection(connection);
  }

}
