import {AfterViewInit, Component, ElementRef, OnInit} from '@angular/core';
import BpmModeler = require('bpmn-js/lib/Modeler');
import {Store} from '@ngrx/store';
import {getProcessDefinitionById} from '../../../reducers';
import {CustomModeler} from '../custom-elements';
import * as NavigationActions from '../../../actions/navigation.action';
import * as propertiesPanelModule from 'bpmn-js-properties-panel';
import * as propertiesProviderModule from 'bpmn-js-properties-panel/lib/provider/camunda';
import * as camundaModdleDescriptor from 'camunda-bpmn-moddle/resources/camunda.json';
import * as commons from 'ngpd-bpmn-commons';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';

const pageCtxActions = commons.commonActions.pageContextActions;
const processDefActions = commons.commonActions.processDefinitionActions;

const diagramData = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn2:definitions xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:bpmn2="http://www.omg.org/spec/BPMN/20100524/MODEL"
xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI"
xmlns:dc="http://www.omg.org/spec/DD/20100524/DC"
xmlns:di="http://www.omg.org/spec/DD/20100524/DI"
xsi:schemaLocation="http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd"
id="sample-diagram" targetNamespace="http://bpmn.io/schema/bpmn">
  <bpmn2:process id="Process_1" isExecutable="false">
    <bpmn2:startEvent id="StartEvent_1"/>
  </bpmn2:process>
  <bpmndi:BPMNDiagram id="BPMNDiagram_1">
    <bpmndi:BPMNPlane id="BPMNPlane_1" bpmnElement="Process_1">
      <bpmndi:BPMNShape id="_BPMNShape_StartEvent_2" bpmnElement="StartEvent_1">
        <dc:Bounds height="36.0" width="36.0" x="412.0" y="240.0"/>
      </bpmndi:BPMNShape>
    </bpmndi:BPMNPlane>
  </bpmndi:BPMNDiagram>
</bpmn2:definitions>
`;

@Component({
  selector: 'app-process-modeler',
  templateUrl: './process-modeler.component.html',
  styleUrls: ['./process-modeler.component.less']
})
export class ProcessModelerComponent implements OnInit, AfterViewInit {
  public currentProcessDefinition: commons.IProcessDefinition;
  public viewer: BpmModeler;

  private subscription: Subscription[];

  constructor(private store: Store<commons.IAppState>,
              private activatedRoute: ActivatedRoute,
              private hostElement: ElementRef) {
    this.subscription = [];
    store.dispatch(new pageCtxActions.HideTopNav());
    store.dispatch(new pageCtxActions.HideSideNav());
  }

  public ngOnInit() {
  }

  public ngAfterViewInit(): void {
    this.viewer = new CustomModeler({
      additionalModules: [
        propertiesPanelModule,
        propertiesProviderModule
      ],
      moddleExtensions: {
        camunda: camundaModdleDescriptor
      },
      container: '#canvas',
      propertiesPanel: {
        parent: '#js-properties-panel'
      }
    });

    this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        setTimeout(() => {
          this.subscription.push(this.store.select(getProcessDefinitionById(params.id)).subscribe((processDefinition: commons.IProcessDefinition) => {
            this.currentProcessDefinition = processDefinition;
            const data = processDefinition.configXml || diagramData;
            this.viewer.importXML(data, (err) => {
              if (err) {
                console.log('error: ', err);
              }
              // this.viewer.addCustomElements(customElementDeclarations);
            });
          }));
        }, 0);

      }
    });

    this.insertControls();
    this.hideCamundaBrandingImg();
  }

  public handleSave() {
    this.viewer.saveXML({format: true}, (err, xml) => {
      if (err) {
        throw err;
      }

      const newProcessDef = Object.assign({}, this.currentProcessDefinition, {
        configXml: xml
      });
      const payload: commons.IUpdateProcessDefinitionPayload = {
        updateFrom: 'modeler',
        data: newProcessDef
      };
      console.log('doing this thing');
      this.store.dispatch(new processDefActions.UpdateProcessDefinition(payload));
      this.handleBack();
    });
  }

  public handleBack() {
    this.store.dispatch(new NavigationActions.Back());
  }

  public handleUndo() {
    const commandStack = this.viewer.injector.get('commandStack');
    commandStack.undo();
  }

  public handleRedo() {
    const commandStack = this.viewer.injector.get('commandStack');
    commandStack.redo();
  }

  private hideCamundaBrandingImg() {
    const brandingImage = this.hostElement.nativeElement.getElementsByTagName('img');
    if (brandingImage && brandingImage.length > 0) {
      brandingImage[0].setAttribute('style', 'display:none');
    }
  }

  private insertControls() {
    const controlsElement = this.hostElement.nativeElement.querySelector('#controls');
    const canvasElement = this.hostElement.nativeElement.querySelector('.bjs-container');
    canvasElement.append(controlsElement);
  }
}
