import { ProcessModelerComponent } from './process-modeler/process-modeler.component';

export const processModelerRoutes = [
  {
    path: 'process-modeler/:id',
    component: ProcessModelerComponent
  }
];
