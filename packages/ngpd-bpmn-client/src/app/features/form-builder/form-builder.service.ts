import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';

import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { environment } from '../../../environments/environment';
import { appConfig } from '../../commons/app.utils';

const apiUrl = `${environment.gbmApi}/${environment.apiVersion}/form-ref`;
const processUrl = `${appConfig.apiUrl}/processDefinitions`;

@Injectable()
export class FormBuilderService {
  constructor(private http: Http) {
  }

  public getForms() {
    return this.http.get(processUrl).map((res: Response) => {
      const response = res.json ? res.json() : res;
      return response;
    });
  }

  public updateForm(form) {
    return this.http.put(processUrl + '/form', form).map((res: Response) => res.json ? res.json() : res)
      .catch((error: any) => Observable.of(error.json ? error.json() : error));
  }

  public getFormById(id: string) {
    return this.http.get(apiUrl + '/' + id).map((res: Response) => res.json ? res.json() : res)
      .catch((error: any) => Observable.of(error.json ? error.json() : error));
  }

  public createForm(data, id?) {
    return this.http.post(apiUrl, data).map((res: Response) => res.json ? res.json() : res)
      .catch((error: any) => Observable.of(error.json ? error.json() : error));
  }
}


