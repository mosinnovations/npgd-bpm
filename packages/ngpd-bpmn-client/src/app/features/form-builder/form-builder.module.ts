import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSidenavModule, MatButtonModule, MatCardModule, MatListModule, MatIconModule, MatInputModule, MatDialogModule, MatOptionModule, MatSelectModule, MatTooltipModule } from '@angular/material';
import { RouterModule } from '@angular/router';

import { GbmModule } from 'gbm-components';

import { FormBuilderComponent, FormBuilderDialogComponent } from './form-builder.component';
import { FormBuilderService } from './form-builder.service';
import { FormConfigComponent } from './form-config/form-config.component';
import { FormListComponent } from './form-list/form-list.component';
import { FormWorkspaceComponent, NewFieldDialogComponent } from './form-workspace/form-workspace.component';

import { PluginsModule } from '../../plugins/plugins.module';

const COMPONENTS = [
  FormBuilderComponent,
  FormConfigComponent,
  FormWorkspaceComponent,
  FormListComponent,
  FormBuilderDialogComponent,
  NewFieldDialogComponent,
];

const MATERIAL_COMPONENTS = [
  MatSidenavModule,
  MatButtonModule,
  MatCardModule,
  MatDialogModule,
  MatInputModule,
  MatListModule,
  MatIconModule,
  MatOptionModule,
  MatSelectModule,
  MatTooltipModule
];

@NgModule({
  imports: [
    CommonModule,
    ...MATERIAL_COMPONENTS,
    GbmModule,
    FormsModule,
    ReactiveFormsModule,
    PluginsModule,
    FlexLayoutModule,
  ],
  providers: [
    FormBuilderService
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
  entryComponents: COMPONENTS
})
export class FormBuilderModule {
}
