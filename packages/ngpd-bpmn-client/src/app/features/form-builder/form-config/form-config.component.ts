import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, ContentChildren } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { uiControls } from '../../../services/process-definition.constants';
import { configFields, baseConfigFields } from './form-config.constants';

@Component({
  selector: 'app-form-config',
  templateUrl: './form-config.component.html',
  styleUrls: ['./form-config.component.scss']
})
export class FormConfigComponent implements OnInit, OnChanges {
  @Output() public saveConfig: EventEmitter<any> = new EventEmitter();
  @Input() public config: any;

  public fields: any[];
  public configForm: FormGroup;

  private childFields: any[] = [];

  constructor(private fb: FormBuilder) {
  }

  public ngOnInit() {}

  public ngOnChanges(changes: SimpleChanges) {
    if (changes.config && changes.config.currentValue) {
      const { label, inputType, defaultValue, options } = configFields;
      const { inputType: _inputType, defaultValue: _defaultValue, label: _label, type, options: _options, width: _width } = changes.config.currentValue;
      const width = baseConfigFields.width;

      inputType.options = uiControls[type];
      defaultValue.type = type;
      defaultValue.controlConfig.type = inputType.options[0].value;

      // set default value of form fields
      width.defaultValue = _width || width.defaultValue;
      inputType.defaultValue = _inputType;
      defaultValue.defaultValue = _defaultValue;
      label.defaultValue = _label;
      options.defaultValue = _options;
      this.fields = [ label, inputType, options, defaultValue, width ];
      this.constructFormData(this.fields);
    }
  }

  public handleConfigSave() {
    const {label, inputType, options, defaultValue, width} = this.configForm.value;
    const payload = { label, inputType, options, width, defaultValue };
    if (this.configForm.value['inputType'] === 'select' || this.configForm.value['inputType'] === 'multiselect') {
      payload.options = options;
    }
    this.saveConfig.emit(payload);
    this.configForm.markAsPristine();
  }

  public showField(field) {
    if (field.id !== 'options') {
      return true;
    } else {
      return this.configForm.value['inputType'] === 'select' || this.configForm.value['inputType'] === 'multiselect';
    }
  }

  private constructFormData(fields: any[]) {
    const formFields = fields.reduce((accumulator, field) => {
      return { ...accumulator, [field.id]: [ field.defaultValue || '' ] };
    }, {});

    this.configForm = this.fb.group(formFields);
  }
}
