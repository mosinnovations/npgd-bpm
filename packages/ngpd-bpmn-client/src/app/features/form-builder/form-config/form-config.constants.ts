export const configFields: any = {
  label: {
    controlConfig: {
      width: 100,
      type: 'input'
    },
    type: 'string',
    id: 'label',
    label: 'Label'
  },
  options: {
    controlConfig: {
      width: 100,
      type: 'input'
    },
    type: 'string',
    id: 'options',
    label: 'Options (Comma Separated)'
  },
  inputType: {
    options: [
      {
        value: 'input',
        label: 'Input'
      },
      {
        value: 'select',
        label: 'Select'
      },
      {
        value: 'toggle',
        label: 'Toggle'
      },
      {
        value: 'datepicker',
        label: 'Datepicker'
      },
    ],
    controlConfig: {
      width: 100,
      type: 'select'
    },
    type: 'string',
    id: 'inputType',
    label: 'UI Input type'
  },
  defaultValue: {
    options: [],
    controlConfig: {
      width: 100,
      type: 'input'
    },
    type: 'string',
    id: 'defaultValue',
    label: 'Default Value'
  }
};

export const baseConfigFields = {
  width: {
    controlConfig: {
      width: 100,
      type: 'slider',
      min: 10,
      max: 100,
      step: 10,
      thumbLabel: true,
    },
    defaultValue: 100,
    type: 'long',
    id: 'width',
    label: 'Width'
  }
};

export const defaultValueMapping = {
  'input': 'string',
  'select': 'string',
  'datepicker': 'date',
  'toggle': 'boolean',
};
