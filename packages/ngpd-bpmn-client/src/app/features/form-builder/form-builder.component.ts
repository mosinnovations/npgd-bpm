import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilderService } from './form-builder.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-form-builder',
  templateUrl: './form-builder.component.html',
  styleUrls: ['./form-builder.component.scss']
})
export class FormBuilderComponent implements OnInit {
  public currentForm: any;
  public forms: any[] = [];
  public processes: any[] = [];
  public fields: any[] = [];
  public formMap: any = {};
  private currentProcessIndex: number;
  private currentFormIndex: number;

  constructor(public dialog: MatDialog, private route: ActivatedRoute, private router: Router, private formService: FormBuilderService) { }

  public ngOnInit() {
    const { id, processId } = this.route.snapshot.queryParams;

    this.formService.getForms()
      .subscribe(processes => {
        this.processes = processes;
        if (id && processId) {
          this.currentProcessIndex = this.processes.findIndex((process) => process._id === processId);
          this.currentFormIndex = this.processes[this.currentProcessIndex].formInfoList.findIndex((formInfo) => formInfo.formId === id);
          this.currentForm = this.processes[this.currentProcessIndex].formInfoList[this.currentFormIndex];
        }
      });
  }

  public selectCurrentForm(processIndex, formIndex) {
    this.currentForm = this.processes[processIndex].formInfoList[formIndex];
    this.currentProcessIndex = processIndex;
    this.currentFormIndex = formIndex;
    this.router.navigate([], { queryParams: { id: this.currentForm.formId, processId: this.processes[processIndex]._id }, relativeTo: this.route });
  }

  public handleFieldChanage(fields) {
    this.currentForm.fields = fields;
    this.handleFormChange();
  }

  public handleFormChange() {
    this.formService.updateForm(this.currentForm).subscribe(res => {
      console.log('saved!');
    });
  }
}

@Component({
  selector: 'app-form-builder-dialog',
  template: `
  <mat-dialog-content>
    <mat-form-field [style.width]="'100%'">
      <input matInput placeholder="Form Name" [(ngModel)]="name">
    </mat-form-field>
  </mat-dialog-content>
  <mat-dialog-actions>
    <button mat-button [disabled]="!name" (click)="onClick()">Create Form</button>
  </mat-dialog-actions>
  `,
})
export class FormBuilderDialogComponent {
  public name: string;

  constructor(public dialogRef: MatDialogRef<FormBuilderDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }

  public onClick(): void {
    this.dialogRef.close({ name: this.name });
  }

}
