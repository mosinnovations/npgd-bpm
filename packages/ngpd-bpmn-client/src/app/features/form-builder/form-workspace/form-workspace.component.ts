import { Component, OnInit, Input, Inject, Output, EventEmitter, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { uiControls } from '../../../services/process-definition.constants';

@Component({
  selector: 'app-form-workspace',
  templateUrl: './form-workspace.component.html',
  styleUrls: ['./form-workspace.component.scss']
})
export class FormWorkspaceComponent implements OnInit, OnChanges, OnDestroy {
  @Input() public currentPluginData: any;
  @Input() public fields: any[] = [];
  @Input() public title: string;
  @Output() public formChange: EventEmitter<any> = new EventEmitter();
  public form: FormGroup;
  public sections: any[] = [];
  public activeConfig: any;
  public configObject: any;
  public activeIndex: number[];
  public saveDisabled = true;

  private _childFields: any[] = [];
  private _childFieldSubscriptions = [];

  constructor(public dialog: MatDialog, private fb: FormBuilder) { }

  public ngOnInit() {
  }

  public ngOnChanges(changes: SimpleChanges) {
    if (changes.fields) {
      this.constructFormData(changes.fields.currentValue);
    }
  }

  public ngOnDestroy() {
    // this._childFieldSubscriptions.forEach(sub => {sub.unsubscribe()});
  }

  public addField(sectionIndex) {
    const dialogRef = this.dialog.open(NewFieldDialogComponent);

    // TODO: think of field_id strategy for when fields can be reordered
    dialogRef.afterClosed().subscribe(result => {
      if (!result) { return; }
      this.saveDisabled = false;
      this.form.addControl('field_id' + this.sections[sectionIndex].fields.length + '-' + sectionIndex, new FormControl(''));
      this.sections[sectionIndex].fields = this.sections[sectionIndex].fields || [];
      this.sections[sectionIndex].fields.push({
        controlConfig: { type: 'input', width: 100 },
        id: 'field_id' + this.sections[sectionIndex].fields.length + '-' + sectionIndex,
        label: result.name,
        type: result.type,
      });
    });
  }

  public addSection() {
    const dialogRef = this.dialog.open(NewFieldDialogComponent, { data: { hideType: true } });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) { return; }
      this.saveDisabled = false;

      this.sections.push({ name: result.name, fields: [] });
    });
  }


  public handleComponentLoad(component, index, sectionIndex) {
    const { instance } = component;
    this._childFields[sectionIndex] = this._childFields[sectionIndex] || [];
    this._childFields[sectionIndex][index] = instance;
    if (this._childFieldSubscriptions[sectionIndex] && this._childFieldSubscriptions[sectionIndex][index]) { this._childFieldSubscriptions[sectionIndex][index].unsubscribe(); }
    this._childFieldSubscriptions[sectionIndex] = this._childFieldSubscriptions[sectionIndex] || [];
    this._childFieldSubscriptions[sectionIndex][index] = instance.fieldFocus.subscribe(() => {
      this.setActiveConfig(instance, index, sectionIndex);
    });
  }

  public handleSaveConfig(configData) {
    const field = this.sections[this.activeIndex[0]].fields[this.activeIndex[1]];
    field.label = configData.label;
    field.options = (typeof configData.options === 'string') ? configData.options.split(',').map(str => str.trim()) : configData.options;
    field.defaultValue = configData.defaultValue;
    field.controlConfig.width = configData.width;
    field.controlConfig.type = configData.inputType;
    this.sections[this.activeIndex[0]].fields[this.activeIndex[1]] = { ...field };
    this.saveForm();
  }

  public saveForm() {
    this.saveDisabled = true;
    this.formChange.emit(this.sections);
  }

  public editSectionName(sectionIndex, event) {
    const newName = event.target.innerHTML;
    this.sections[sectionIndex].name = newName;
    this.saveDisabled = false;
  }

  private setActiveConfig(instance, index, sectionIndex) {
    this.activeConfig = instance;
    this.configObject = {
      inputType: instance.config.controlConfig.type,
      defaultValue: instance.config.defaultValue,
      width: instance.config.controlConfig.width,
      options: instance.config.options,
      label: instance.config.label,
      type: instance.config.type,
    };
    this.activeIndex = [sectionIndex, index];
  }

  private constructFormData(sections: any[]) {
    const formFields = sections.reduce((accumulator, section) => {
      const mappedFields = section.fields
        ? section.fields.reduce((_accumulator, _field) => {
            return { ..._accumulator, [_field.id]: [_field.defaultValue || ''] };
          }, {})
        : {};
      return { ...accumulator, ...mappedFields };
    }, {});

    this.sections = sections;
    this.form = this.fb.group(formFields);
  }
}

@Component({
  selector: 'app-new-field-dialog',
  template: `
  <mat-dialog-content>
    <mat-form-field [style.width]="'100%'" *ngIf="!hideType">
      <mat-select placeholder="Data type" [(ngModel)]="type">
        <mat-option *ngFor="let option of dataOptions" [value]="option">
          {{ option }}
        </mat-option>
      </mat-select>
    </mat-form-field>
    <mat-form-field [style.width]="'100%'">
      <input matInput placeholder="Field Name" [(ngModel)]="name">
    </mat-form-field>
  </mat-dialog-content>
  <mat-dialog-actions>
    <button mat-button [disabled]="(!type && !hideType) || !name" (click)="onClick()">Select</button>
  </mat-dialog-actions>
  `,
})
export class NewFieldDialogComponent {
  public dataOptions = Object.keys(uiControls);
  public type: string;
  public name: string;
  public hideType = false;

  constructor(public dialogRef: MatDialogRef<NewFieldDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.hideType = data && data.hideType;
  }

  public onClick(): void {
    this.dialogRef.close({ type: this.type, name: this.name });
  }
}
