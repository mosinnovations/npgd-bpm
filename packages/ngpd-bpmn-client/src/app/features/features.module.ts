import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProcessModelerModule } from './process-modeler/process-modeler.module';
import { AuthModule } from './auth/auth.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { ProcessDefinitionModule } from './process-definition/process-definition.module';
import { ProcessInstanceModule } from './process-instance/process-instance.module';
import { FormBuilderModule } from './form-builder/form-builder.module';

@NgModule({
  imports: [
    AuthModule,
    CommonModule,
    DashboardModule,
    FormBuilderModule,
    ProcessDefinitionModule,
    ProcessInstanceModule,
    ProcessModelerModule
  ],
  declarations: []
})
export class FeaturesModule {
}
