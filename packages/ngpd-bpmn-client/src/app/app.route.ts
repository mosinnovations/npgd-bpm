import { FormBuilderComponent } from './features/form-builder/form-builder.component';

export const rootRoutes = [
  {
    path: '', redirectTo: '/auth', pathMatch: 'full'
  },
  {
    path: 'auth',
    loadChildren: './features/auth/auth.module#AuthModule'
  },
  {
    path: 'dashboard',
    loadChildren: './features/dashboard/dashboard.module#DashboardModule'
  },
  {
    path: 'process-modeler',
    loadChildren: './features/process-modeler/process-modeler.module#ProcessModelerModule'
  },
  {
    path: 'process-definition',
    loadChildren: './features/process-definition/process-definition.module#ProcessDefinitionModule'
  },
  {
    path: 'form-builder',
    component: FormBuilderComponent
  }
];
