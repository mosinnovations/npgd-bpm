import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StatCardComponent} from './stat-card/stat-card.component';
import {InfoCardComponent} from './info-card/info-card.component';
import {TopNavComponent} from './top-nav/top-nav.component';
import {
  MatButtonModule,
  MatCardModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatSidenavModule,
  MatSlideToggleModule, MatSnackBarModule,
  MatToolbarModule,
  MatTooltipModule
} from '@angular/material';
import {SideNavComponent} from './side-nav/side-nav.component';
import {RouterModule} from '@angular/router';
import {BpmViewerComponent} from './bpm-viewer/bpm-viewer.component';
import {ConfirmationComponent} from './confirmation/confirmation.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ToolBarComponent} from './tool-bar/tool-bar.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {ErrorSnackbarComponent} from './error-snackbar/error-snackbar.component';

const COMPONENTS = [
  BpmViewerComponent,
  ConfirmationComponent,
  ErrorSnackbarComponent,
  InfoCardComponent,
  StatCardComponent,
  SideNavComponent,
  ToolBarComponent,
  TopNavComponent
];

const MATERIAL_COMPONENTS = [
  MatButtonModule,
  MatCardModule,
  MatDialogModule,
  MatInputModule,
  MatIconModule,
  MatSidenavModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatTooltipModule,
  MatToolbarModule
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    RouterModule,
    ...MATERIAL_COMPONENTS
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class ComponentModule {
}
