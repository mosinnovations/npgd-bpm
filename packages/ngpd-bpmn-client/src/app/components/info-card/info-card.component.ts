import {AfterViewInit, Component, ElementRef, Input, OnInit} from '@angular/core';
import {CustomModeler} from '../../features/process-modeler/custom-elements/index';
import BpmnViewer = require('bpmn-js');

export interface IInfoCardConfig {
  id: string;
  headlineColor: string;
  headlineText: string;
  diagramData?: string;
}

@Component({
  selector: 'app-info-card',
  templateUrl: './info-card.component.html',
  styleUrls: ['./info-card.component.scss']
})
export class InfoCardComponent implements OnInit, AfterViewInit {

  @Input() infoCardConfig: IInfoCardConfig;

  constructor() {
  }

  ngOnInit() {
  }

  public ngAfterViewInit(): void {
  }
}
