import { Component, Input, OnInit } from '@angular/core';

export interface ICardHeader {
  backgroundColor: string;
  icon: string;
}

@Component({
  selector: 'app-stat-card',
  templateUrl: './stat-card.component.html',
  styleUrls: ['./stat-card.component.less']
})
export class StatCardComponent implements OnInit {
  @Input() cardheader: ICardHeader;

  constructor() { }

  ngOnInit() {
  }
}
