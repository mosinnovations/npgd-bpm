import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

export interface IConfirmationConfig {
  message?: string;
}

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit {
  public dialogConfig: IConfirmationConfig;

  constructor(public dialogRef: MatDialogRef <ConfirmationComponent>,
              @Inject(MAT_DIALOG_DATA) public data: IConfirmationConfig) {
    this.dialogConfig = data;
  }

  ngOnInit() {
  }

  public handleYes() {
    this.dialogRef.close(true);
  }

  public handleNo() {
    this.dialogRef.close(false);
  }

}
