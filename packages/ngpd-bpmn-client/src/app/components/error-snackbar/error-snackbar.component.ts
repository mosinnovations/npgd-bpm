import {Component, Inject, OnInit} from '@angular/core';
import {MAT_SNACK_BAR_DATA, MatSnackBarRef} from '@angular/material';
import * as commons from 'ngpd-bpmn-commons';

const MAX_SUMMARY_LENGTH = 100;

@Component({
  selector: 'app-error-snackbar',
  templateUrl: './error-snackbar.component.html',
  styleUrls: ['./error-snackbar.component.scss']
})
export class ErrorSnackbarComponent implements OnInit {

  public errors: commons.IError[];
  public mode: 'details' | 'summary';
  public selectedError: commons.IError;

  constructor(
    private snackBarRef: MatSnackBarRef<any>,
    @Inject(MAT_SNACK_BAR_DATA)public data: commons.IError[]) {
    this.errors = data;
    this.mode = 'summary';
  }

  ngOnInit() {
  }

  public getSummaryDisplay(error: commons.IError) {
    if(error.message.length > MAX_SUMMARY_LENGTH) {
      return error.message.substring(0, MAX_SUMMARY_LENGTH) + '...';
    }
    return error.message;
  }

  public showViewLink(error: commons.IError) {
    if(error.message.length > MAX_SUMMARY_LENGTH) {
      return true;
    }
    return false;
  }

  public handleDismiss() {
    this.snackBarRef.dismiss();
  }

  public handleViewDetails(e: any, error: commons.IError) {
    e.preventDefault();
    this.mode = 'details';
    this.selectedError = error;
  }

  public handleBackToSummary(e: any) {
    e.preventDefault();
    this.mode = 'summary';
    this.selectedError = null;
  }
}
