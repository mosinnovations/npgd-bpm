import {
  AfterViewInit,
  Component,
  ElementRef, EventEmitter,
  Input,
  OnDestroy,
  OnInit, Output
} from '@angular/core';
import BpmnViewer =  require('bpmn-js');
import {Subject} from 'rxjs/Subject';

const BPMN_TYPES = {
  userTask: 'bpmn:UserTask',
  serviceTask: 'bpmn:ServiceTask'
};

export interface IBpmnEvent {
  id: string;
  label: string;
  type: string;

}

@Component({
  selector: 'app-bpm-viewer',
  templateUrl: './bpm-viewer.component.html',
  styleUrls: ['./bpm-viewer.component.scss']
})
export class BpmViewerComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input() canvasId: string;
  @Input() diagramXml: string;
  @Input() size?: 'lg' | 'md' | 'sm';

  @Output() onBpmEvent: EventEmitter<IBpmnEvent>;

  private viewer: BpmnViewer;

  constructor(private hostElement: ElementRef) {
    this.viewer = new BpmnViewer();
    this.onBpmEvent = new EventEmitter();
  }

  ngOnInit() {
  }

  public ngOnDestroy(): void {
    this.viewer.detach();
    this.viewer = null;
  }

  public ngAfterViewInit(): void {
    setTimeout(() => {
      // TODO: sometimes viewer is null here, can not figure out why...
      if (this.viewer) {
        this.viewer.attachTo('#' + this.canvasId);
        this.loadDiagram();
      }
    }, 100);
    const eventBus = this.viewer.get('eventBus');
    eventBus.on('element.click', (event: any) => {
      this.onBpmEvent.emit(event.element);
    });
  }

  public get diagramContainerClass() {
    if (!this.size) {
      return 'bpm-content';
    } else
      return 'bpm-content-' + this.size;
  }


  private loadDiagram() {
    this.viewer.importXML(this.diagramXml, (err) => {
      if (err) {
        console.log('error: ', err);
        throw err;
      }
      this.viewer.get('canvas').zoom('fit-viewport');
    });
    this.hideCamundaBrandingImg();
  }

  private hideCamundaBrandingImg() {
    const brandingImage = this.hostElement.nativeElement.getElementsByTagName('img');
    if (brandingImage && brandingImage.length > 0) {
      brandingImage[0].setAttribute('style', 'display:none');
    }
  }
}

