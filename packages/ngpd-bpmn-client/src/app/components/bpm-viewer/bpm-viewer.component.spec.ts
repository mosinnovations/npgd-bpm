import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BpmViewerComponent } from './bpm-viewer.component';

describe('BpmViewerComponent', () => {
  let component: BpmViewerComponent;
  let fixture: ComponentFixture<BpmViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BpmViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BpmViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
