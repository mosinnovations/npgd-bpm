import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import * as commons from 'ngpd-bpmn-commons';

export interface ITopNavConfig {
  show?: boolean;
  title?: string;
  items?: commons.INavItem[];
}

@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.scss']
})
export class TopNavComponent implements OnInit {

  @Input() config: ITopNavConfig;

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  public get showNavBar() {
    if(!this.config) {
      return
    }
    return !!(this.config && this.config.show);
  }

  public handleClick(item: commons.INavItem) {
    if (item.type === 'link') {
      this.router.navigate([item.path]);
    } else if (item.type === 'action') {
      item.execute();
    }
  }

}
