import {Component, Input, OnInit} from '@angular/core';
import * as commons from 'ngpd-bpmn-commons';

export interface IToolbarConfig {
  show?: boolean;
  items?: commons.INavItem[];
}

@Component({
  selector: 'app-tool-bar',
  templateUrl: './tool-bar.component.html',
  styleUrls: ['./tool-bar.component.scss']
})
export class ToolBarComponent implements OnInit {

  @Input() toolBarConfig: IToolbarConfig;

  constructor() { }

  ngOnInit() {
  }

}
