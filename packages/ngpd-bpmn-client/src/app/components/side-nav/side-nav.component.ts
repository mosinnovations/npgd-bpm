import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

export interface ISideNavItem {
  label: string;
  target?: {
    path: string,
    queryParams?: { [key: string]: any };
  };
  action?: (itemId?: string) => void;
}

export interface ISideNavConfig {
  show?: boolean;
  sideNavItems?: ISideNavItem[];
}

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit {

  @Input() sideNavConfig: ISideNavConfig;

  private activeItemIndex: number;

  constructor(private route: ActivatedRoute) {
    this.activeItemIndex = 0;
  }

  ngOnInit() {

  }

  public isNavItemActive(index: number) {
    const item = this.sideNavConfig.sideNavItems[index];
    if(!item || item.target) {
      return false;
    }
    return this.activeItemIndex === index;
  }

  public executeAction(event: any, menuItem: ISideNavItem, index: number) {
    event.preventDefault();
    this.activeItemIndex = index;
    menuItem.action();
  }

}
