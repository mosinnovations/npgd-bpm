import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

export interface LoaderState {
  show: boolean;
}

@Injectable()
export class LoaderService {

  public loaderSubject = new Subject<LoaderState>();
  public loaderState = this.loaderSubject.asObservable();

  constructor() {
  }

  public show() {
    this.loaderSubject.next({show: true});
  }

  public hide() {
    this.loaderSubject.next({show: false});
  }

}
