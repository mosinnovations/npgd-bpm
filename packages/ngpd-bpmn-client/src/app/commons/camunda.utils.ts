import * as _ from 'lodash';
import {IFormField, IFormInfo} from 'ngpd-bpmn-commons';

export class CamundaUtils {

  constructor() {
  }

  public extractFormFields(definitions: any): IFormInfo[] {
    if(!definitions) {
      return [];
    }
    const rootElements = definitions.get('rootElements');
    const regex = /usertask$/i;
    if (rootElements && rootElements.length > 0) {
      const flowElements: any[] = rootElements[0].flowElements.filter(f => {
        return regex.test(f.$type);
      });
      return flowElements.map(flowElement => {
        const formInfo = this.extractFormInfo(flowElement);
        return {
          ...formInfo,
          fields: this.extracFields(flowElement)
        }
      }) as any;
    }
    return null;
  }

  private extractFormInfo(flowElement: any) {
    return {
      formId: flowElement.id,
      formKey: flowElement.$attrs['camunda:formKey'],
      formName: flowElement.name
    };
  }

  private extracFields(formElement: any): IFormField[] {
    const regexFormData = /formdata$/i;
    const extElement: any = formElement.extensionElements;
    if (extElement) {
      const formData = extElement.values.filter(v => regexFormData.test(v.$type));
      if (formData && formData.length > 0) {
        return _(formData)
          .map(fe => fe.$children)
          .flatten()
          .map((field) => {
            return {
              id: field.id,
              label: field.label,
              type: field.type
            };
          }).value();
      }
    }
    return [];
  }
}

export const camundaUtils = new CamundaUtils();
