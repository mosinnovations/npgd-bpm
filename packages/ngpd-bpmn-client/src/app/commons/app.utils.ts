import {RouterStateSerializer} from '@ngrx/router-store';
import {DefaultUrlSerializer, Params, RouterStateSnapshot, UrlSerializer, UrlTree} from '@angular/router';
import {environment} from '../../environments/environment';

export interface IRouterStateUrl {
  url: string;
  queryParams: Params;
}

export class CustomRouterStateSerializer
  implements RouterStateSerializer<IRouterStateUrl> {
  serialize(routerState: RouterStateSnapshot): IRouterStateUrl {
    const {url} = routerState;
    const queryParams = routerState.root.queryParams;

    return {url, queryParams};
  }
}

export class CustomUrlSerializer implements UrlSerializer {
  parse(url: any): UrlTree {
    let dus = new DefaultUrlSerializer();
    return dus.parse(url);
  }

  serialize(tree: UrlTree): any {
    let dus = new DefaultUrlSerializer(),
      path = dus.serialize(tree);
    // use your regex to replace as per your requirement.
    path = path.replace(/%3F/g, '?');
    path = path.replace(/%3D/g, '=');
    return path;
  }
}

class Config {
  private env: any;

  constructor(env: any) {
    this.env = env;
  }

  public get apiUrl() {
    return `${this.env.apiHost}/${this.env.apiVersion}`;
  }
}

export const appConfig = new Config(environment);

class GeneralUtils {
  constructor() {
  }

  public withDefault(obj: any, defaultValue: any) {
    if(obj instanceof Array) {
      if(!obj || obj.length === 0) {
        return defaultValue;
      }
    }
    if(!obj) {
      return defaultValue;
    }
    return obj;
  }
}

export const generalUtils = new GeneralUtils();
