import { ConnectionBackend, Http, RequestOptions, RequestOptionsArgs, Request, Response } from '@angular/http';
import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { LoaderService } from './loader.service';

@Injectable()
export class HttpWrapper extends Http {

  constructor(private backend: ConnectionBackend,
              private defaultOptions: RequestOptions,
              private loaderService: LoaderService,
              private injector: Injector) {
    super(backend, defaultOptions);
  }

  public request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
    return this.intercept(super.request(url, options));
  }

  public get(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.intercept(super.get(url, options));
  }

  public post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.intercept(super.post(url, body, options));
  }

  public put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.intercept(super.put(url, body, options));
  }

  public delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.intercept(super.delete(url, options));
  }

  public intercept(observable: Observable<Response>): Observable<Response> {
    this.loaderService.show();
    return observable.catch((error, source) => {
      let message;
      if (error.json) {
        message = error.json().message;
      } else if (error.statusText) {
        message = error.statusText;
      } else {
        message = '';
      }

      if (error) {
        return Observable.throw(error);
      } else {
        return Observable.empty();
      }
    }).do((response: Response) => {
    }).finally(() => {
      this.loaderService.hide();
    });
  }
}
