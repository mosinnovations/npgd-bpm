import {BrowserModule} from '@angular/platform-browser';
import {Injector, NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {FeaturesModule} from './features/features.module';
import {RouterModule, UrlSerializer} from '@angular/router';
import {MatButtonModule, MatNativeDateModule, MatSidenavModule, MatToolbarModule} from '@angular/material';
import {rootRoutes} from './app.route';
import {StoreModule} from '@ngrx/store';
import {getReducers, metaReducers, REDUCER_TOKEN} from './reducers';
import {RouterStateSerializer, StoreRouterConnectingModule} from '@ngrx/router-store';
import {environment} from '../environments/environment';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {EffectsModule} from '@ngrx/effects';
import {CustomRouterStateSerializer, CustomUrlSerializer} from './commons/app.utils';
import {RouterEffects} from './effects/router.effect';
import {ProcessDefinitionsEffect} from './effects/process-definitions.effect';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Http, HttpModule, RequestOptions, XHRBackend} from '@angular/http';
import {LoaderService} from './commons/loader.service';
import {HttpWrapper} from './commons/http.wrapper';
import {ComponentModule} from './components/component.module';
import {ServicesModule} from './services/services.module';
import {ProcessInstanceEffect} from './effects/process-instance.effect';

const MATERIAL_MODULES = [
  MatButtonModule,
  MatSidenavModule,
  MatNativeDateModule,
  MatToolbarModule
];

const EFFECTS = [
  ProcessDefinitionsEffect,
  ProcessInstanceEffect,
  RouterEffects
];

/**
 * Override http provider
 */
export function httpFactory(xhrBackend: XHRBackend,
                            requestOptions: RequestOptions,
                            loaderService: LoaderService,
                            injector: Injector) {
  return new HttpWrapper(xhrBackend, requestOptions, loaderService, injector);
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ComponentModule,
    FeaturesModule,
    HttpModule,
    ...MATERIAL_MODULES,
    RouterModule.forRoot(rootRoutes, {useHash: true}),
    ServicesModule,
    StoreModule.forRoot(REDUCER_TOKEN, {metaReducers}),
    StoreRouterConnectingModule,
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    EffectsModule.forRoot(EFFECTS)
  ],
  providers: [
    LoaderService,
    {provide: Http, useFactory: httpFactory, deps: [XHRBackend, RequestOptions, LoaderService, Injector]},
    {provide: RouterStateSerializer, useClass: CustomRouterStateSerializer},
    {provide: UrlSerializer, useClass: CustomUrlSerializer},
    {provide: REDUCER_TOKEN, useFactory: getReducers}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
