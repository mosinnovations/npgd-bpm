import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import {Action} from '@ngrx/store';

@Injectable()
export class ErrorHandlingEffect {

  @Effect()
  deployProcessDefinition$ = this.actions$.filter((action: Action) => {
    const regex = /fail/i;
    return regex.test(action.type);

  }).map((action: any) => action.payload)


  constructor(private actions$: Actions) {
  }


}
