import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import {Effect, Actions} from '@ngrx/effects';
import * as RouterActions from '../actions/navigation.action';
import * as commons from 'ngpd-bpmn-commons';
import {Store} from '@ngrx/store';

const processDefinitionActions = commons.commonActions.processDefinitionActions;

@Injectable()
export class RouterEffects {
  @Effect({dispatch: false})
  navigate$ = this.actions$.ofType(RouterActions.GO)
    .map((action: RouterActions.Go) => action.payload)
    .do(({path, query: queryParams, extras}) =>
      this.router.navigate(path, {queryParams, ...extras}));

  @Effect({dispatch: false})
  navigateBack$ = this.actions$.ofType(RouterActions.BACK)
    .do(() => this.location.back());

  @Effect({dispatch: false})
  navigateForward$ = this.actions$.ofType(RouterActions.FORWARD)
    .do(() => this.location.forward());

  // navigate after certain effects
  @Effect({dispatch: false})
  goToProcessDefinitionDetails$ = this.actions$.ofType(processDefinitionActions.CREATE_PROCESS_DEFINITION_SUCCESS)
    .map((action: any) => action.payload)
    .do((p: commons.IProcessDefinition) => this.router.navigate(['/process-definition/' + p._id], {
      queryParams: {
        section: 'general'
      }
    }));

  @Effect({dispatch: false})
  logRoute$ = this.actions$.ofType('ROUTER_NAVIGATION')
    .map((action: any) => action.payload)
    .filter((payload: any) => {
      console.log('payload 1 = ', payload);
      const routerState = payload.routerState;
      return /\/process-definition\/\S*/g.test(routerState.url);
    }).do((payload: any) => {
      console.log('payload here = ', payload);
      //this.router.navigate(payload.routerState.url);
    });


  constructor(private actions$: Actions,
              private store$: Store<commons.IAppState>,
              private router: Router,
              private location: Location) {
  }
}
