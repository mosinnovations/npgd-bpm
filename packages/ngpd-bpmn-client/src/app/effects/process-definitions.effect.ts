import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import * as commons from 'ngpd-bpmn-commons';
import {appConfig} from '../commons/app.utils';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {ProcessDefinitionService} from '../services/process-definition.service';

const apiUrl = `${appConfig.apiUrl}/processDefinitions`;
const processDefinitionActions = commons.commonActions.processDefinitionActions;


// we can check store for cached process definition and only call server if missing
@Injectable()
export class ProcessDefinitionsEffect {
  @Effect()
  createProcessDefinition$ = this.actions$.ofType(processDefinitionActions.CREATE_PROCESS_DEFINITION)
    .map((action: any) => action.payload)
    .switchMap((processDefinition: commons.IProcessDefinition) => {
      return this.processDefinitionService.syncFormMetaDataWithBpmnXML(processDefinition);
    })
    .switchMap(({processDefinition: newProcessDefinition, formData}) => {
      const payload = Object.assign({}, newProcessDefinition, {
        formInfo: formData,
        deployed: false
      });
      return this.createProcessDefinition(payload).map((result: Response) => {
        return new processDefinitionActions.CreateProcessDefinitionSuccess(result.json());
      }).catch(e => Observable.of(new processDefinitionActions.CreateProcessDefinitionFail(e)));
    });

  @Effect()
  getProcessDefinitions$ = this.actions$.ofType(processDefinitionActions.GET_PROCESS_DEFINITIONS)
    .map((action: any) => action.payload)
    .switchMap(() => {
      return this.getProcessDefintions().map((result: Response) => {
        return new processDefinitionActions.GetProcessDefinitionsSuccess(result.json());
      }).catch(e => Observable.of(new processDefinitionActions.GetProcessDefinitionFail(e)));
    });

  @Effect()
  getProcessDefinitionById$ = this.actions$.ofType(processDefinitionActions.GET_PROCESS_DEFINITION_BY_ID)
    .map((action: any) => action.payload)
    .switchMap((id: string) => {
      return this.getProcessDefinitionById(id).map((result: Response) => {
        return new processDefinitionActions.GetProcessDefinitionByIdSuccess(result.json());
      }).catch(e => Observable.of(new processDefinitionActions.GetProcessDefinitionByIdFail(e)));
    });

  @Effect()
  updateProcessDefinition$ = this.actions$.ofType(processDefinitionActions.UPDATE_PROCESS_DEFINITIONS)
    .map((action: any) => action.payload)
    .switchMap((payload: commons.IUpdateProcessDefinitionPayload) => {
      const processDefinition = payload.data;
      if(payload.updateFrom === 'modeler') {
        return this.processDefinitionService.syncFormMetaDataWithBpmnXML(processDefinition);
      } else {
        return this.processDefinitionService.syncBpmnXMLWithFormMetaData(processDefinition);
      }
    })
    .switchMap(({processDefinition, formData}) => {
      const payload = Object.assign({}, processDefinition, {
        formInfoList: formData
      });
      return this.updateProcessDefinition(payload).map((result: Response) => {
        return new processDefinitionActions.UpdateProcessDefinitionSuccess(result.json());
      }).catch(e => Observable.of(new processDefinitionActions.UpdateProcessDefinitionFail(e)));
    });

  @Effect()
  deployProcessDefinition$ = this.actions$.ofType(processDefinitionActions.DEPLOY_PROCESS_DEFINNITION)
    .map((action: any) => action.payload)
    .switchMap((id: string) => {
      return this.deployProcessDefinition(id).map((result: Response) => {
        return new processDefinitionActions.DeployProcessDefinitionSuccess(result.json());
      }).catch((e: Response) => {
        return Observable.of(new processDefinitionActions.DeployProcessDefinitionFail(e.json()))
      });
    });

  constructor(private actions$: Actions,
              private http: Http,
              private processDefinitionService: ProcessDefinitionService) {
  }

  private getProcessDefintions() {
    return this.http.get(apiUrl);
  }

  private createProcessDefinition(processDefinition: commons.IProcessDefinition) {
    return this.http.post(apiUrl, processDefinition);
  }

  private getProcessDefinitionById(id: string) {
    return this.http.get(apiUrl + '/' + id);
  }

  private updateProcessDefinition(processDefinition: commons.IProcessDefinition) {
    return this.http.put(apiUrl + '/' + processDefinition._id, processDefinition);
  }

  private deployProcessDefinition(id: string) {
    return this.http.post(apiUrl + '/' + id + '/deploy', {});
  }
}


