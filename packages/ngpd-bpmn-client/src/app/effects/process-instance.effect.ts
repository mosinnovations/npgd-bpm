import {appConfig} from '../commons/app.utils';
import * as commons from 'ngpd-bpmn-commons';
import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import {HttpClient} from '@angular/common/http';
import {Action} from '@ngrx/store';

const apiUrl = `${appConfig.apiUrl}/processInstances`;
const taskApiUrl = `${appConfig.apiUrl}/tasks`;

const processInstanceActions = commons.commonActions.processInstanceActions;

@Injectable()
export class ProcessInstanceEffect {

  @Effect()
  getProcessInstances$ = this.actions$.ofType(processInstanceActions.GET_PROCESS_INSTANCES)
    .map((action: any) => action.payload)
    .switchMap((procDefid: string) => {
      return this.getProcessInstances(procDefid).map((result: any) => {
        console.log('here process instances result = ', result);
        return new processInstanceActions.GetProcessInstancesSuccess(result);
      });
    });

  @Effect()
  startProcessInstances$ = this.actions$.ofType(processInstanceActions.START_PROCESS_INSTANCE)
    .map((action: any) => action.payload)
    .switchMap((processDefinition: commons.IProcessDefinition) => {
      return this.startProcessInstance(processDefinition.definitionId).map((result: any) => {
        console.log('here process instances result = ', result);
        return new processInstanceActions.StartProcessInstanceSuccess(result);
      });
    });

  @Effect()
  completeTask$ = this.actions$.ofType(processInstanceActions.COMPLETE_TASK)
    .map((action: any) => action.payload)
    .switchMap((taskId: string) => {
      return this.completeTask(taskId).map((result: any) => {
        console.log('here process instances result = ', result);
        return new processInstanceActions.CompleteTaskSucess(result);
      });
    });

  constructor(private actions$: Actions, private http: HttpClient) {
  }

  private getProcessInstances(procDefid: string) {
    return this.http.get(apiUrl + '?processDefinitionId=' + procDefid);
  }

  private startProcessInstance(definitionId: string) {
    return this.http.post(`${apiUrl}/start/${definitionId}`, {
      variables: {
        initiator: {
          value: 'Leo Jin',
          type: 'String'
        }
      }
    });
  }

  private completeTask(taskId: string) {
    return this.http.post(`${taskApiUrl}/${taskId}/complete`, {
      variables: {
        initiator: {
          value: 'Leo Jin',
          type: 'String'
        }
      }
    });
  }
}
