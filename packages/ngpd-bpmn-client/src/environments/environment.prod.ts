export const environment = {
  production: true,
  apiHost: 'http://localhost:3100',
  apiVersion: 'v1'
};
