import { Observable } from 'rxjs/Observable';

export interface ITask {
    taskSlug: string;
    execute: (options?: any) => Observable<any>;
}