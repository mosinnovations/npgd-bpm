import { IExecutionPlan } from './plan.model';
import { ITask } from './task.model';
import { Observable } from 'rxjs/Observable';
import { appUtils } from '../utils/AppUtils';

const logger = appUtils.getLogger('PlanExecutor');

class PlanExecutor {
    private plan: IExecutionPlan;

    constructor(plan: IExecutionPlan) {
        this.plan = plan;
    }

    public start() {
        logger.info(`-----Plan ${this.plan.planSlug} execution starting...`);
        let count = 0;
        const allTasks = this.plan.tasks.map((task: ITask) => {
            logger.info(`${count++}. -----Execute task '${task.taskSlug}'`);
            return task.execute();
        });
        Observable.concat(...allTasks).subscribe((res) => {
            logger.info(res);
            logger.info(`-----Un-subscribing task subscription`);
        });
    }
}

export const planExecutor = (plan: IExecutionPlan) => new PlanExecutor(plan);