import { ITask } from './task.model';
import { Observable } from 'rxjs/Observable';

export interface IExecutionPlan {
    planSlug: string;
    tasks: ITask[];
}