import * as express from 'express';
import * as Joi from 'joi';
import { appUtils } from "../utils/AppUtils";

const logger = appUtils.getLogger('request-validator-mw');

/**
 * JOI validation middleware
 */
export interface IValidationSchema {
    body?: any;
    query?: any;
    params?: any;
}

export interface IValidator {
    [action: string]: {
        [path: string]: IValidationSchema
    }
}

const validate = (value: any, validationSchema: any, res: express.Response, next: express.NextFunction) => {
    const options = appUtils.getJoiOptions();
    logger.debug(`Joi validation option: ${JSON.stringify(options)}`);
    Joi.validate(value, validationSchema, options, (err: Joi.ValidationError) => {
        if (err) {
            res.status(400).send(err.details);
        } else {
            next();
        }
    });
};

export const requestValidate = (schema: IValidationSchema) => {
    const {
        body: payloadSchema,
        query: querySchema,
        params: paramSchema
    } = schema;
    return (req: express.Request, res: express.Response, next: express.NextFunction) => {
        if (payloadSchema) {
            validate(req.body, payloadSchema, res, next);
        } else if (querySchema) {
            validate(req.query, querySchema, res, next);
        } else if (paramSchema) {
            validate(req.params, paramSchema, res, next);
        } else {
            logger.error(`cannot determine value type, currently supported types are 'body', 'query', and 'params'`);
        }
    };
};
