import { Logger } from 'ngpd-merceros-logging-adapter';
import * as configs from 'nconf';
import * as dotenv from 'dotenv';
import * as path from 'path';

const configDefaults = {

}

class AppUtils {
    public bootstrapped = false;

    constructor() {
    }

    public bootstrap() {
        if (this.bootstrapped) {
            return;
        }
        /**
         * Load environment variables from .env file, where API keys and passwords are configured.
         */
        dotenv.config({path: path.join(__dirname, '..', '.env')});
        configs.env({separator: '__'});
        const loggerConfig = configs.get('logger');
        for (const property in loggerConfig) {
            if (loggerConfig.hasOwnProperty(property)) {
                if (loggerConfig[property] === 'true') {
                    loggerConfig[property] = true;
                }
                if (loggerConfig[property] === 'false') {
                    loggerConfig[property] = false;
                }
            }
        }
        Logger.init('ngpd-guidance-api', loggerConfig);
        this.bootstrapped = true;
    }

    public getLogger(category: string) {
        this.bootstrap();
        return Logger.getInstance(category);
    }

    public getJoiOptions() {
        this.bootstrap();
        let joiConfig = configs.get('joi')['options'];
        if (joiConfig) {
            this.convertStringToBooleans(joiConfig);
        } else {
            joiConfig = {
                allowUnknown: true
            };
        }
        return joiConfig;
    }

    public getBpmApiUrl(): string {
        this.bootstrap();
        const url = configs.get('bpm')['server'].url;
        if(!url) {
            throw new Error('missing bpm server url in env config');
        }
        return  url;
    }

    public getApiVersion(): string {
        this.bootstrap();
        return configs.get('app')['api'].version || 'v1';
    }

    public getExecutionEnv(): string {
        this.bootstrap();
        return configs.get('NODE_ENV');
    }

    /**
     * Converts all string values to boolean values
     * if they are 'true' or 'false'
     *
     * Note* this function modifies the original object (not immutable)
     *
     * @param obj
     */
    private convertStringToBooleans(obj: any) {
        Object.keys(obj).forEach((k: string) => {
            const val = obj[k];
            if (/\btrue\b|\bfalse\b/.test(val)) {
                obj[k] = Boolean(val);
            }
            if (typeof(val) === 'object') {
                this.convertStringToBooleans(val);
            }
        });
    }
}

export const appUtils = new AppUtils();