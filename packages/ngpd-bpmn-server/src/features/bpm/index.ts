export {
    default as processDefinitionRoutes
} from './process-definition/process-definition.routes';
export {
    default as processInstanceRoutes
} from './process-instance/process-instance.routes';
export {
    default as taskRoutes
} from './tasks/tasks.route';