import * as express from 'express';
import * as controller from './tasks.controller';

const router = express.Router();

router.post('/:id/complete', controller.completeTask);
export default router;