import * as restler from "restler";
import {appUtils} from '../../../utils/AppUtils';
import {AsyncSubject} from 'rxjs/AsyncSubject';

export const completeTask = (taskId: string, variables: any) => {
    const asyncSubject = new AsyncSubject();
    const apiUrl = `${appUtils.getBpmApiUrl()}/task/${taskId}/complete`;
    restler
        .postJson(apiUrl, variables)
        .on('complete', function (data: any) {
            if (data instanceof Error) {
                asyncSubject.error(data);
            } else {
                asyncSubject.next(data);
            }
            asyncSubject.complete();
        });
    return asyncSubject.asObservable();
};