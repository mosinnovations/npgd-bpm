import * as express from 'express';
import * as taskService from './tasks.service';

export const completeTask = (req: express.Request, res: express.Response) => {
    const {
        id
    } = req.params;
    const variables = req.body;
    taskService.completeTask(id, variables).subscribe(() => {
        res.status(200).send({
            completed: true
        });
    }, (err: Error) => {
        res.status(500).send(err);
    })
};