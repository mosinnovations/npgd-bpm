import * as express from 'express';
import * as controller from './process-definition.controller';
import { requestValidate } from '../../../middlewares/request-validator.middleware';
import { validator } from './process-definition.validator';

const router = express.Router();

router.post('/', requestValidate(validator['post']['/']), controller.createProcessDefinition);
router.put('/form', controller.updateFormData);
router.put('/:id', requestValidate(validator['put']['/:id']), controller.updateProcessDefinition);
router.get('/', requestValidate(validator['get']['/']), controller.getProcessDefinitions);
router.get('/:id', requestValidate(validator['get']['/:id']), controller.getProcessDefinition);
router.delete('/:id', requestValidate(validator['delete']['/:id']), controller.deleteProcessDefinition);

// operators
router.post('/:id/deploy', requestValidate(validator['post']['/:id/deploy']), controller.deployProcessDefinition);

export default router;