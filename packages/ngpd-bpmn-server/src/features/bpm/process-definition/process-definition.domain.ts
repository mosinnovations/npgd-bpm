import * as commons from 'ngpd-bpmn-commons';
import * as mongoose from 'mongoose';
import * as paginate from 'mongoose-paginate';

const Schema = mongoose.Schema;

export interface IProcessDefinition extends commons.IProcessDefinition, mongoose.Document {
}

const ProcessDefinitionSchema = new Schema({
    name: {
        type: String
    },
    description: {
        type: String
    },
    configXml: {
        type: String
    },
    deployed: {
        type: Boolean
    },
    definitionId: {
        type: String
    },
    definitionVersion: {
        type: Number
    },
    formInfoList: [Schema.Types.Mixed],
    componentConfig: {
        type: Schema.Types.Mixed
    }
}, {
    timestamps: true
});

ProcessDefinitionSchema.plugin(paginate);

export const ProcessDefinition = mongoose.model<IProcessDefinition>('Process', ProcessDefinitionSchema);