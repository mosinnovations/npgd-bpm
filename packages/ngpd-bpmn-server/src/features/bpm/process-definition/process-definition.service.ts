import {processDefinitionRepository} from './process-definition.repository';
import {IProcessDefinition} from './process-definition.domain';
import * as restler from 'restler'
import {AsyncSubject} from 'rxjs/AsyncSubject';
import {appUtils} from '../../../utils/AppUtils';
import * as commons from 'ngpd-bpmn-commons';
import {Observable} from 'rxjs/Observable';

const logger = appUtils.getLogger('ProcessDefinitionService');

interface IProcessDefDeployed {
    id: string;
    version: number;
    deploymentId: number;
}

interface IDeployedPayload {
    deployedProcessDefinitions: {
        [key: string]: IProcessDefDeployed;
    };
}

class ProcessDefinitionService {
    constructor() {
    }

    public deploy(processId: string) {
        return processDefinitionRepository.getProcessDefinition(processId)
            .switchMap((processDefinition: IProcessDefinition) => {
                return this.deployProcessDefinition(processDefinition).map((result: IDeployedPayload) => {
                    return {
                        deployedPayload: result,
                        processDefinition
                    }
                });
            })
            .switchMap(({deployedPayload, processDefinition}) => {
                if(deployedPayload.deployedProcessDefinitions) {
                    processDefinition.definitionId = Object.keys(deployedPayload.deployedProcessDefinitions)[0];
                    processDefinition.deployed = true;
                    processDefinition.definitionVersion = deployedPayload.deployedProcessDefinitions[processDefinition.definitionId].version;
                    return processDefinitionRepository.updateProcessDefinition(processDefinition, processDefinition.id).map(() => {
                        return deployedPayload;
                    })
                }
                return Observable.of(deployedPayload);
            });
    }

    private deployProcessDefinition(processDefinition: IProcessDefinition) {
        const subject: AsyncSubject<IDeployedPayload> = new AsyncSubject();
        restler.post(`${appUtils.getBpmApiUrl()}/deployment/create`, {
            multipart: true,
            data: {
                'deployment-name': processDefinition.name,
                'enable-duplicate-filtering': true,
                'deployment-source': processDefinition.name, // TODO: this should be process app id..
                'data': (restler as any).data(`${processDefinition._id}.bpmn`, 'text/xml', new Buffer(processDefinition.configXml))
            }
        }).on('complete', (result) => {
            if (result.type) {
            }
            if (result instanceof Error) {
                logger.error((result as Error).message);
                subject.error(result);
            } else {
                if (result.type) {
                    switch (result.type) {
                        case 'ProcessEngineException':
                            logger.error('ProcessEngineException', result.message);
                            const httpError: commons.IHttpError = {
                                status: 500,
                                code: 'bpm.processEngine.exception',
                                message: result.message
                            };
                            subject.error(httpError);
                            return;
                        default:
                            break;
                    }
                }
                logger.info('===== success fully deployed process definition with id ' + processDefinition._id);
                subject.next(result);
            }
            subject.complete();
        });
        return subject.asObservable();
    }
}

export const processDefinitionService = new ProcessDefinitionService();