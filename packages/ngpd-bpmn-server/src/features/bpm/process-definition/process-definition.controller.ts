import * as express from 'express';
import {processDefinitionRepository} from './process-definition.repository';
import * as commons from 'ngpd-bpmn-commons';
import * as _ from 'lodash';
import {IProcessDefinition} from './process-definition.domain';
import {processDefinitionService} from './process-definition.service';


export const getProcessDefinitions = (req: express.Request, res: express.Response) => {
    processDefinitionRepository.getProcessDefinitions().subscribe((result) => {
        res.status(200).send(result);
    }, (error: commons.IHttpError) => {
        res.status(500).send(error);
    });
};

export const getProcessDefinition = (req: express.Request, res: express.Response) => {
    const {
        id
    } = req.params;
    processDefinitionRepository.getProcessDefinition(id).subscribe((result) => {
        res.status(200).send(result);
    }, (error: commons.IHttpError) => {
        res.status(500).send(error);
    });
};

export const createProcessDefinition = (req: express.Request, res: express.Response) => {
    const processDef = _.omit(req.body, '_id') as IProcessDefinition;
    processDefinitionRepository.createProcessDefinition(processDef).subscribe((result: IProcessDefinition) => {
        res.status(200).send(result);
    }, (error: commons.IHttpError) => {
        res.status(500).send(error);
    });
};

export const updateProcessDefinition = (req: express.Request, res: express.Response) => {
    const processDef = req.body;
    const {
        id
    } = req.params;
    processDefinitionRepository.updateProcessDefinition(processDef, id).subscribe((result) => {
        res.status(200).send(result);
    }, (error: commons.IHttpError) => {
        res.status(500).send(error);
    });
};

export const deleteProcessDefinition = (req: express.Request, res: express.Response) => {
    const {
        id
    } = req.params;
    processDefinitionRepository.deleteProcessDefinition(id).subscribe((result) => {
        res.status(200).send(result);
    }, (error: commons.IHttpError) => {
        res.status(500).send(error);
    });
};

export const deployProcessDefinition = (req: express.Request, res: express.Response) => {
    const {
        id
    } = req.params;
    processDefinitionService.deploy(id).subscribe((result) => {
        res.status(200).send(result);
    }, (error: commons.IHttpError) => {
        res.status(500).send(error);
    });
};

export const updateFormData = (req: express.Request, res: express.Response) => {
    const { formId } = req.body;
    processDefinitionRepository.getProcessByFormId(formId).subscribe((result: IProcessDefinition) => {
        if (!result) {
            return res.status(404).send();
        }
        const formIndex = result.formInfoList.findIndex(form => form.formId === formId);
        const formInfoList = [...result.formInfoList];
        formInfoList[formIndex] = req.body;
        result.formInfoList = formInfoList;
        result.save().then((saved: IProcessDefinition) => {
            res.status(200).send(result);
        });
    }, (error: commons.IHttpError) => {
        res.status(500).send(error);
    });
};
