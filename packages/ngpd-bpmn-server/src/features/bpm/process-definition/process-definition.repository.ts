import {IProcessDefinition, ProcessDefinition} from './process-definition.domain';
import {Observable} from 'rxjs/Observable';

class ProcessDefinitionRepository {
    constructor() {
    }

    public getProcessDefinitions() {
        return Observable.fromPromise(ProcessDefinition.find({}));
    }

    public getProcessDefinition(id: string) {
        return Observable.fromPromise(ProcessDefinition.findById(id));
    }

    public getProcessByFormId(formId: string) {
        return Observable.fromPromise(ProcessDefinition.findOne({ ['formInfoList.formId']: formId }));
    }

    public createProcessDefinition(proc: IProcessDefinition) {
        return Observable.fromPromise(ProcessDefinition.create(proc));
    }

    public updateProcessDefinition(proc: IProcessDefinition, procId: string) {
        return Observable.fromPromise(ProcessDefinition.findByIdAndUpdate(procId, proc, {new: true}));
    }

    public deleteProcessDefinition(procId: string) {
        return Observable.fromPromise(ProcessDefinition.findByIdAndRemove(procId));
    }

    public getProcessDefinitionByDefinitionId(definitionId: string) {
        return Observable.fromPromise(ProcessDefinition.find({
            definitionId
        }));
    }
}

export const processDefinitionRepository = new ProcessDefinitionRepository();