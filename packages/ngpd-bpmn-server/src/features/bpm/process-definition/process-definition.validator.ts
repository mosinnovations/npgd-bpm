import * as Joi from 'joi';
import {IValidator} from '../../../middlewares/request-validator.middleware';

export const validator: IValidator = {
    delete: {
        '/:id': {
            params: {
                id: Joi.string().required()
            }
        }
    },
    get: {
        '/': {
            query: {
                pageIndex: Joi.number().optional(),
                pageSize: Joi.number().optional()
            }
        },
        '/:id': {
            params: {
                id: Joi.string().required()
            }
        }
    },
    post: {
        '/': {
            body: {
                name: Joi.string().required(),
                description: Joi.string().optional(),
                configuration: Joi.string().optional(),
                definitionId: Joi.string().optional().allow(null),
                deployed: Joi.boolean().default(false),
                formInfo: Joi.array().items({
                    formId: Joi.string(),
                    formKey: Joi.string(),
                    formName: Joi.string().optional(),
                    fields: Joi.array().optional(),
                    formConfig: Joi.object({
                        layoutConfig: Joi.object({
                            colNumber: Joi.number(),
                            hAlignment: Joi.string(),
                            vAlignment: Joi.string()
                        }),
                        componentConfig: Joi.object({
                            key: Joi.string()
                        })
                    })
                }).allow([], null).optional()
            }
        },
        '/:id/deploy': {
            params: {
                id: Joi.string().required()
            }
        }
    },
    put: {
        '/:id': {
            params: {
                id: Joi.string().required()
            },
            body: {
                name: Joi.string().required(),
                description: Joi.string().optional(),
                configuration: Joi.string().optional()
            }
        }
    }
};