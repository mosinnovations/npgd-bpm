import * as express from 'express';
import {requestValidate} from '../../../middlewares/request-validator.middleware';
import * as controller from './process-instance.controller';

const router = express.Router();

router.get('/', controller.getProcessInstances);
router.post('/start/:processDefinitionId', controller.startProcessInstance);
router.post('/task/:id', controller.getTasksForProcessInstance);
export default router;