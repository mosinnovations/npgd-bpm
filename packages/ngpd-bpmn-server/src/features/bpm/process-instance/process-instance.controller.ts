import * as express from 'express';
import * as processInstanceService from './process-instance.service';
import * as commons from 'ngpd-bpmn-commons';
import * as Rx from 'rxjs';

export const getProcessInstances = (req: express.Request, res: express.Response) => {
    const {
        processDefinitionId
    } = req.query;
    processInstanceService.getProcessInstanceWithTasks(processDefinitionId)
        .switchMap((pInstances: commons.IGetProcessInstancePayload[]) => {
            if(!pInstances || pInstances.length === 0) {
                return Rx.Observable.of([]);
            }
            return Rx.Observable.forkJoin(
                ...pInstances.map((payload: commons.IGetProcessInstancePayload) => {
                    const pInstance = Object.assign({}, payload.processInstance, {
                        tasks: payload.tasks
                    });
                    return processInstanceService.mergeTasksWithFormInfo( processDefinitionId, pInstance);
                })
            );
        })
        .subscribe((result: any[]) => {
            res.status(200).send(result);
        }, (error: any) => {
            res.status(500).send(error);
        });
};

export const startProcessInstance = (req: express.Request, res: express.Response) => {
    const {
        processDefinitionId
    } = req.params;
    const payload = req.body;
    processInstanceService.startProcessInstance(processDefinitionId, payload)
        .subscribe((result: any) => {
            res.status(200).send(result);
        }, (error: any) => {
            res.status(500).send(error);
        })
};

export const getTasksForProcessInstance = (req: express.Request, res: express.Response) => {
    const {
        id
    } = req.params;
    processInstanceService.getProcessInstanceTasks(id)
        .subscribe((result: any) => {
            res.status(200).send(result);
        }, (error: any) => {
            res.status(500).send(error);
        });
};