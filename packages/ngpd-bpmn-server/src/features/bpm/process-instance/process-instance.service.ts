import * as restler from 'restler';
import * as Rx from 'rxjs';
import {appUtils} from '../../../utils/AppUtils';
import * as commons from 'ngpd-bpmn-commons';
import {AsyncSubject} from 'rxjs/AsyncSubject';
import {processDefinitionRepository} from '../process-definition/process-definition.repository';

export const getProcessInstances = (processDefId?: string) => {
    const asyncSubject: Rx.AsyncSubject<commons.IProcessInstance[] | Error> = new Rx.AsyncSubject();
    let apiUrl;
    if (!processDefId) {
        apiUrl = `${appUtils.getBpmApiUrl()}/process-instance`
    } else {
        apiUrl = `${appUtils.getBpmApiUrl()}/process-instance?processDefinitionId=${processDefId}`
    }
    restler
        .get(apiUrl)
        .on('complete', function (data: commons.IProcessInstance[]) {
            if (data instanceof Error) {
                asyncSubject.error(data);
            } else {
                asyncSubject.next(data);
            }
            asyncSubject.complete();
        });
    return asyncSubject.asObservable();
};

export const mergeTasksWithFormInfo = (processDefinitionId: string, processInstance: commons.IProcessInstance) => {
    return processDefinitionRepository.getProcessDefinitionByDefinitionId(processDefinitionId)
        .map((processDefinitions: commons.IProcessDefinition[]) => {
            if(processDefinitions.length !== 1) {
                throw new Error('Error: found more than one process definition for id ' + processDefinitionId);
            }
            const processDefinition = processDefinitions[0];
            const formInfoList = processDefinition.formInfoList;
            let taskList = processInstance.tasks;
            taskList = taskList.map((task: commons.ITask) => {
                const formInfo = formInfoList.find((fInfo: commons.IFormInfo) => fInfo.formId === task.taskDefinitionKey);
                return Object.assign({}, task, {
                    formInfo
                })
            });
            return Object.assign({}, processInstance, {
                tasks: taskList
            });
        });
};

export const getProcessInstanceWithTasks = (processDefinitionId: string) => {
    return getProcessInstances(processDefinitionId)
        .mergeMap((processInstanceList: commons.IProcessInstance[]) => {
            if(!processInstanceList || processInstanceList.length === 0) {
                return Rx.Observable.of([]);
            }
            return Rx.Observable
                .forkJoin(...processInstanceList
                    .map((pi: commons.IProcessInstance) =>
                        getProcessInstanceTasks(pi.id)
                            .map((tasks) => {
                                return {
                                    processInstance: pi,
                                    tasks
                                };
                            })));
        });
};

export const getProcessInstanceTasks = (instanceId: string) => {
    const asyncSubject: Rx.AsyncSubject<any[]> = new AsyncSubject();
    const apiUrl = `${appUtils.getBpmApiUrl()}/task/?processInstanceId=${instanceId}`;
    restler
        .get(apiUrl)
        .on('complete', function (data: any[]) {
            if (data instanceof Error) {
                asyncSubject.error(data);
            } else {
                asyncSubject.next(data);
            }
            asyncSubject.complete();
        });
    return asyncSubject.asObservable();
};

export const startProcessInstance = (processDefintionId: string, variables: any) => {
    const asyncSubject: Rx.AsyncSubject<commons.IProcessInstance[] | Error> = new Rx.AsyncSubject();
    const apiUrl = `${appUtils.getBpmApiUrl()}/process-definition/${processDefintionId}/start`;
    restler.postJson(apiUrl, variables)
        .on('complete', (result: any) => {
            if (result instanceof Error) {
                asyncSubject.error(result);
            } else {
                asyncSubject.next(result);
            }
            asyncSubject.complete();
        });
    return asyncSubject.asObservable();
};