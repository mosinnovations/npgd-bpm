import * as express from 'express';
import * as controller from './project.controller';
import { requestValidate } from "../../middlewares/request-validator.middleware";
import { validator } from "./project.validator";

const router = express.Router();

router.post('/', requestValidate(validator['post']['/']), controller.createProject);
router.put('/:id', requestValidate(validator['put']['/:id']), controller.updateProject);
router.get('/', requestValidate(validator['get']['/']), controller.getProjects);
router.get('/:id', requestValidate(validator['get']['/:id']), controller.getProjectById);
router.delete('/:id', requestValidate(validator['delete']['/:id']), controller.deleteProjectById);

export default router;