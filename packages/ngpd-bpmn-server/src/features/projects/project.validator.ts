import * as Joi from 'joi';
import { IValidator } from "../../middlewares/request-validator.middleware";

export const validator: IValidator = {
    delete: {
        '/:id': {
            params: {
                id: Joi.string().required()
            }
        }
    },
    get: {
        '/': {
            query: {
                userId: Joi.string().optional(),
                pageIndex: Joi.number().optional(),
                pageSize: Joi.number().optional()
            }
        },
        '/:id': {
            params: {
                id: Joi.string().required()
            }
        }
    },
    post: {
        '/': {
            body: {
                name: Joi.string().required(),
                url: Joi.string().optional(),
                status: Joi.string().optional()
            }
        },
    },
    put: {
        '/:id': {
            params: {
                id: Joi.string().required()
            },
            body: {
                name: Joi.string().required(),
                url: Joi.string().optional(),
                status: Joi.string().optional()
            }
        }
    }
};