import * as commons from 'ngpd-bpmn-commons';
import * as mongoose from 'mongoose';
import * as paginate from 'mongoose-paginate';

const Schema = mongoose.Schema;

export interface IProject extends commons.IProject, mongoose.Document {
}

const ProjectSchema = new Schema({
    name: {
        type: String
    },
    url: {
        type: String
    },
    status: {
        type: String
    }
}, {
    timestamps: true
});

ProjectSchema.plugin(paginate);

export const Project = mongoose.model<IProject>('Project', ProjectSchema);