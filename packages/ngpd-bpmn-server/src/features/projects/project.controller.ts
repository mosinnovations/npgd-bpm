import * as express from 'express';
import * as commons from 'ngpd-bpmn-commons';

import { IProject } from './project.schema';
import { projectRepository } from './project.repository';
import { CastError } from "mongoose";

export const createProject = (req: express.Request, res: express.Response) => {
    const project: IProject = req.body;
    projectRepository.createProject(project).subscribe((result) => {
        res.status(200).send(result);
    }, (error: commons.IHttpError) => {
        res.status(500).send(error);
    });
};

export const updateProject = (req: express.Request, res: express.Response) => {
    const project: IProject = req.body;
    const projectId: string = req.params.id;
    projectRepository.updateProject(projectId, project).subscribe((result) => {
        res.status(200).send(result);
    }, (error: commons.IHttpError) => {
        res.status(500).send(error);
    });
};

export const getProjects = (req: express.Request, res: express.Response) => {
    const {
        pageIndex,
        pageSize
    } = req.query;
    projectRepository.getProjects(pageIndex, pageSize).subscribe((results) => {
        if (results && results.length > 0) {
            res.status(200).send(results);
        } else {
            res.status(404).send('empty project collection');
        }
    }, (error: commons.IHttpError) => {
        res.send(error).status(500);
    });
};

export const getProjectById = (req: express.Request, res: express.Response) => {
    const {
        id
    } = req.params;
    projectRepository.getProjectById(id).subscribe((result: IProject) => {
        if (!result || Object.keys(result).length === 0) {
            res.status(404).send('No project with id ' + id + ' was found');
        } else {
            res.status(200).send(result);
        }
    }, error => {
        res.status(500).send(error);
    });
};

export const deleteProjectById = (req: express.Request, res: express.Response) => {
    const {
        id
    } = req.params;
    projectRepository.deleteProjectById(id).subscribe((result) => {
        res.status(200).send(true);
    }, error => {
        let reply: commons.IError;
        let httpStatus = 500;
        if(error instanceof CastError) {
            const cs = error as CastError;
            reply = {
                code: 'delete.projects.notfound',
                message: cs.message
            };
            httpStatus = 404;
        } else {
            reply = error;
        }
        res.status(httpStatus).send(reply);
    });

};



