import * as Rx from 'rxjs';

import { IProject, Project } from './project.schema';
import { PaginateResult } from 'mongoose';

class ProjectRepository {

    constructor() {
    }

    public createProject(project: IProject) {
        return Rx.Observable.fromPromise(Project.create(project));
    }

    public updateProject(projectId: string, project: IProject) {
        return Rx.Observable.fromPromise(Project.findByIdAndUpdate(projectId, project));
    }

    public getProjects(pageIndex?: number, pageSize?: number): Rx.Observable<IProject[]> {
        if (!pageIndex || !pageSize) {
            return Rx.Observable.fromPromise(Project.find({}))
                .map((pds: any[]) => {
                    return pds.map((p: any) => {
                        return p.toObject();
                    })
                });
        } else {
            return Rx.Observable.fromPromise(
                Project.paginate({}, {offset: pageIndex, limit: pageSize}) as any
            ).map((pds: PaginateResult<IProject[]>) => {
                if (pds.total > 0) {
                    return pds.docs.map((doc: any) => {
                        return doc.toObject() as IProject;
                    });
                }
                return [];
            });
        }
    }

    public getProjectById(id: string) {
        return Rx.Observable.fromPromise(Project.findById(id));
    }

    public deleteProjectById(id: string) {
        return Rx.Observable.fromPromise(Project.findByIdAndRemove(id));
    }
}

export const projectRepository = new ProjectRepository();