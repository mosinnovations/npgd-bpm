const data = {
    allowUnknown: 'true',
    nested: {
        foo: 'false',
        bar: true,
        tt: 'not boolean',
        num: '6',
        lev: {
            another: 'false',
            hello: 'world'
        }
    }
};


function convertValToBoolean(obj: any) {
    Object.keys(obj).forEach((k:string) => {
        const val = obj[k];
        if(/\btrue\b|\bfalse\b/.test(val)){
            obj[k] = Boolean(val);
        }
        if(typeof(val) === 'object'){
            convertValToBoolean(val);
        }
    });
}

convertValToBoolean(data);

data
