import { appUtils } from "../src/utils/AppUtils";
import * as faker from 'faker';

class TestUtils {
    constructor() {
    }

    public enhanceUrlPath(path: string) {
        return `/${appUtils.getApiVersion()}${path}`;
    }

    public enhanceMockedDocs(docData: any) {
        if (docData instanceof Array) {
           return (docData as Array<any>).map(doc => {
               return this.enhanceMockDoc(doc);
            });
        }
        return this.enhanceMockDoc(docData);
    }

    /**
     * enhance mock docs with extra meta properties usually
     * populated by db.
     *
     * @param doc
     */
    private enhanceMockDoc(doc: any) {
        let enahncedDoc = Object.assign({}, doc, {
            _id: faker.random.uuid()
        });
        enahncedDoc = Object.assign({}, enahncedDoc, {
            toObject: () => enahncedDoc
        });
        return enahncedDoc;
    }
}

export const testUtils = new TestUtils();