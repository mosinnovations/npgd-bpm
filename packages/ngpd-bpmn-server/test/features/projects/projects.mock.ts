import * as commons from 'ngpd-bpmn-commons';

export const project_started: commons.IProject = {
    name: 'project-1',
    url: 'www.project-1.com',
    status: 'start'
};

export const project_inprogres: commons.IProject = {
    name: 'project-2',
    url: 'www.project-2.com',
    status: 'in-progress'
};

export const projects = [
    project_started,
    project_inprogres
];