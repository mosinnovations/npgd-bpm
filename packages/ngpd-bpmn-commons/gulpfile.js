const gulp = require('gulp');
const ts = require('gulp-typescript');
const tsProject = ts.createProject('tsconfig.json');
const tsTestProject = ts.createProject('tsconfig.test.json');
const sourcemaps = require('gulp-sourcemaps');
const path = require('path');
const mocha = require('gulp-mocha');
const shell = require('shelljs');

const distDir = 'lib';
const srcDir = 'src';
const testDir = 'test';
const buildDir = 'build';

// compile typescript
gulp.task('build', () => {
  return gulp.src(srcDir + '/**/*.ts', {base: srcDir})
    .pipe(sourcemaps.init())
    .pipe(tsProject())
    .pipe(sourcemaps.write({
      sourceRoot: function (file) {
        const sourceFile = path.join(file.cwd, file.sourceMap.file);
        return path.relative(path.dirname(sourceFile), file.cwd);
      }
    }))
    .pipe(gulp.dest(distDir));
});

gulp.task('clean', () => {
  shell.rm('-rf', buildDir);
  shell.rm('-rf', distDir);
});

gulp.task('test', ['compile-tests'], () => {
  gulp.src(buildDir + '/test/**/*.spec.js', {read: false})
    .pipe(mocha({reporter: 'spec'}));
});

gulp.task('compile-tests', () => {
  return gulp.src([srcDir + '/**/*.ts', testDir + '/**/*.ts'], {base: '.'})
    .pipe(sourcemaps.init())
    .pipe(tsTestProject())
    .pipe(sourcemaps.write({
      sourceRoot: function (file) {
        const sourceFile = path.join(file.cwd, file.sourceMap.file);
        return path.relative(path.dirname(sourceFile), file.cwd);
      }
    }))
    .pipe(gulp.dest(buildDir));
});

