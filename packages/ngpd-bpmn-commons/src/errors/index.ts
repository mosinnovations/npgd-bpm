export interface IError {
    code: string;
    message: string;
    meta?: any;
}

export interface IHttpError extends IError {
    status: any;
}