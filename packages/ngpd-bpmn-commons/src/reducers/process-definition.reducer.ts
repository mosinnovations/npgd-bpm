import * as fromProcessDefinitions from '../actions/process-definition.action';
import * as immutable from 'immutable';

export interface IProcessDefinition {
    _id: any;
    name: string;
    description?: string;
    configXml?: string; // the bpmn 2.0 xml config
    formInfoList?: IFormInfo[];
    deployed: boolean;
    definitionVersion?: number;
    definitionId?: string; // id returned by the bpm engine post deployment
}

export interface IComponentConfig {
    componentKey: string;
    configData: any;
}

export interface IFormField {
    label: string;
    type: 'string' | 'integer' | 'date' | 'long' | 'boolean';
    id: string;
    options?: any[];
    controlConfig: {
        type: 'input' | 'select' |
            'radio' | 'textarea' |
            'checkbox' | 'toggle' |
            'datePicker';
        width: string | number;
    }
}

export interface IFormInfo {
    formId: string;
    formKey: string;
    formName: string;
    fields: IFormField[],
    componentConfig: IComponentConfig;
}

const initialState: IProcessDefinition[] = [];

function handleCreateProcessDefinitionSuccess(processDefinitions: IProcessDefinition[], newDefinition: IProcessDefinition) {
    return immutable.List(processDefinitions).push(newDefinition).toArray();
}

function handleGetProcessDefinitionsSuccess(loadedProcessDefinitions: IProcessDefinition[]) {
    return loadedProcessDefinitions;
}

function handleGetProcessDefinitionByIdSuccess(processDefinitions: IProcessDefinition[], latestProcDef: IProcessDefinition) {
    const index = processDefinitions.findIndex(p => p._id === latestProcDef._id);
    if(index === -1) {
        return immutable.List(processDefinitions).push(latestProcDef).toArray();
    } else {
        return immutable.List(processDefinitions).set(index, latestProcDef).toArray();
    }

}

function handleUpdateProcessDefinitionSuccess(processDefinitions: IProcessDefinition[], upatedProcDef: IProcessDefinition) {
    const index = processDefinitions.findIndex(p => p._id === upatedProcDef._id);
    return immutable.List(processDefinitions).set(index, upatedProcDef).toArray();
}

export function processDefinitions(state: IProcessDefinition[] = initialState, action: any): IProcessDefinition[] {
    switch (action.type) {
        case fromProcessDefinitions.CREATE_PROCESS_DEFINITION_SUCCESS:
            return handleCreateProcessDefinitionSuccess(state, action.payload as IProcessDefinition);
        case fromProcessDefinitions.GET_PROCESS_DEFINITIONS_SUCCESS:
            return handleGetProcessDefinitionsSuccess(action.payload as IProcessDefinition[]);
        case fromProcessDefinitions.GET_PROCESS_DEFINITION_BY_ID_SUCCESS:
            return handleGetProcessDefinitionByIdSuccess(state, action.payload);
        case fromProcessDefinitions.UPDATE_PROCESS_DEFINITIONS_SUCCESS:
            return handleUpdateProcessDefinitionSuccess(state, action.payload);
        default:
            return state;
    }
};

