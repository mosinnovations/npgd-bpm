import * as pageCtxActions from '../actions/page-context.action';
import * as immutable from 'immutable';
import {INavItem} from './index';


export interface IPageContext {
    show: {
        topNav: boolean;
        sideNav: boolean;
    };
    sideNavItems: INavItem[];
}

const initialState: IPageContext = {
    show: {
        topNav: false,
        sideNav: false
    },
    sideNavItems: []
};


function handleShowTopNav(state: IPageContext) {
    const nested = immutable.fromJS(state);
    return nested.updateIn(['show', 'topNav'], (value: any) => true).toJS();
}

function handleHideTopNav(state: IPageContext) {
    const nested = immutable.fromJS(state);
    return nested.updateIn(['show', 'topNav'], (value: any) => false).toJS();
}

function handleShowSideNav(pageContext: IPageContext) {
    const nested = immutable.fromJS(pageContext);
    return nested.updateIn(['show', 'sideNav'], (value: any) => true).toJS();
}

function handleHideSideNav(pageContext: IPageContext) {
    const nested = immutable.fromJS(pageContext);
    return nested.updateIn(['show', 'sideNav'], (value: any) => false).toJS();
}

function handleUpdateSideNavItems(pageContext: IPageContext, navItems: INavItem[]) {
    return Object.assign({}, pageContext, {
        sideNavItems: navItems
    });
}

export function reducer(state = initialState, action: pageCtxActions.Actions): IPageContext {
    switch (action.type) {
        case pageCtxActions.SHOW_TOPNAV:
            return handleShowTopNav(state);
        case pageCtxActions.HIDE_TOPNAV:
            return handleHideTopNav(state);
        case pageCtxActions.SHOW_SIDENAV:
            return handleShowSideNav(state);
        case pageCtxActions.HIDE_SIDENAV:
            return handleHideSideNav(state);
        case pageCtxActions.UPDATE_SIDENAV_ITEMS:
            return handleUpdateSideNavItems(state, action.payload);
        default:
            return state;
    }
}

export const getTopNavState = (state: IPageContext) => state.show.topNav;
