import {COMPLETE_TASK, COMPLETE_TASK_SUCCESS, GET_PROCESS_INSTANCES_SUCCESS} from '../actions/process-instance.action';

export interface IProcessInstance {
    id: string;
    definitionId: string;
    businessKey?: string;
    caseInstanceId?: string;
    ended: boolean;
    suspended: boolean;
    tenantId?: string;
    tasks?: ITask[];
}

export interface ITask {
    id: string;
    name: string;
    assignee: string;
    created: string;
    due: string;
    description?: string;
    executionId: string;
    owner: string;
    processDefinitionId: string;
    processInstanceId: string;
    taskDefinitionKey: string;
    suspended: boolean;
    formKey: string;
    tenantId: string;
}

export interface IGetProcessInstancePayload {
    processInstance: IProcessInstance,
    tasks: ITask[];
}

const initialState: IProcessInstance[] = [];

function handleCompleteTaskSuccess(instances: IProcessInstance[]) {

}

export function processInstances (state: IProcessInstance[] = initialState, action: any) {
    switch (action.type) {
        case GET_PROCESS_INSTANCES_SUCCESS:
            return action.payload;
        case COMPLETE_TASK_SUCCESS:
            return handleCompleteTaskSuccess(action.payload);
        default:
            return state;
    }
}