import * as fromProject from './project.reducer';
import {IProject} from './project.reducer';
import * as fromProcessDefinition from './process-definition.reducer';
import {IProcessDefinition} from './process-definition.reducer';
import {IPageContext, reducer as pageContext} from './page-context.reducer';
import * as fromProcessInstances from './process-instance.reducer';
import * as processDefinitionActions from '../actions/process-definition.action';
import * as pageContextActions from '../actions/page-context.action';
import {IProcessInstance} from './process-instance.reducer';
// NOTE: why do we need this unused import? See https://github.com/Microsoft/TypeScript/issues/5711

export interface IAppState {
    projects: IProject[],
    pageContext: IPageContext;
    processDefinitions: IProcessDefinition[];
    processInstances: IProcessInstance[];
}

export interface ILookupItem {
    label: string;
    value: string | number;
    icon?: string;
}

export interface IFormEvent <T>{
    meta: {
        isValid: boolean;
    };
    data: T;
}

export interface INavItem {
    label?: string;
    icon?: string;
    type: 'link' | 'action';
    tooltip?: string;
    path?: string;
    execute?: () => void;
}

export const commonReducers = {
    projects: fromProject.projects,
    pageContext,
    processDefinitions: fromProcessDefinition.processDefinitions,
    processInstances: fromProcessInstances.processInstances
};