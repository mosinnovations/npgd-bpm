import * as fromProject from '../actions/project.actions';

import { IAppState } from './index';
import { IProject } from './project.reducer';

export interface IProject {
  id?: string;
  name: string;
  url?: string;
  status: 'in-progress' | 'success' | 'fail' | 'start';
}

const initialState: IProject[] = [];

export const projects = (projs = initialState, action: any) => {
  switch (action.type) {
    case fromProject.ActionTypes.LOAD_PROJECTS_SUCCESS:
      return handleLoadProjectSuccess(action.payload);
    default:
      return projs;
  }
};

function handleLoadProjectSuccess(payload: IProject[]) {
  return payload;
}

// selectors
export const getProjects = (state: IAppState) => state.projects;
