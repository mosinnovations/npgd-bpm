import { Action } from '@ngrx/store';
import {INavItem} from '../reducers';

export const SHOW_TOPNAV = '[Layout] Show TopNav';
export const HIDE_TOPNAV = '[Layout] Hide TopNav';
export const SHOW_SIDENAV = '[Layout] Show SideNav';
export const HIDE_SIDENAV = '[Layout] Hide SideNav';
export const UPDATE_SIDENAV_ITEMS = '[Layout] Update Side Items';

export class ShowTopNav implements Action {
    readonly type = SHOW_TOPNAV;
}

export class HideTopNav implements Action {
    readonly type = HIDE_TOPNAV;
}

export class ShowSideNav implements Action {
    readonly type = SHOW_SIDENAV;
}

export class HideSideNav implements Action {
    readonly type = HIDE_SIDENAV;
}

export class UpdateSideNavItems implements Action {
    readonly type = UPDATE_SIDENAV_ITEMS;
    constructor(public payload: INavItem[]) {}
}

export type Actions =
    ShowTopNav |
    HideTopNav |
    ShowSideNav |
    HideSideNav |
    UpdateSideNavItems;
