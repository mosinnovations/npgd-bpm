import { Action } from '@ngrx/store';
import { IProject } from '../reducers/project.reducer';

export const ActionTypes = {
  CREATE_PROJECT: '[Project] Create project',
  CREATE_PROJECT_SUCCESS: '[Project] Create project success',
  CREATE_PROJECT_FAIL: '[Project] Create project fail',
  LOAD_PROJECTS: '[Project] Load Projects',
  LOAD_PROJECTS_SUCCESS: '[Project] Load Projects success',
  LOAD_PROJECTS_FAIL: '[Project] Load Projects fail'
};

export class AddProjectAction implements Action {
  type: string = ActionTypes.CREATE_PROJECT;
  constructor(public payload: IProject) { }
}
export class AddProjectActionSuccess implements Action {
  type: string = ActionTypes.CREATE_PROJECT_SUCCESS;
  constructor(public payload: IProject) { }
}
export class AddProjectActionFail implements Action {
  type: string = ActionTypes.CREATE_PROJECT_FAIL;
  constructor(public payload: Error) { }
}

export class LoadProjects implements Action {
  type: string = ActionTypes.LOAD_PROJECTS;
  constructor() {}
}
export class LoadProjectsSuccess implements Action {
  type: string = ActionTypes.LOAD_PROJECTS_SUCCESS;
  constructor(public payload?: IProject[]) {}
}
export class LoadProjectsFail implements Action {
  type: string = ActionTypes.LOAD_PROJECTS_FAIL;
  constructor(public payload: Error) {}
}
