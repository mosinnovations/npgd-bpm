import {Action} from '@ngrx/store';
import {IFormField, IProcessDefinition} from '../reducers/process-definition.reducer';

export interface IUpdateProcessDefinitionPayload {
    updateFrom: 'modeler' | 'definitionForms';
    data: IProcessDefinition;
}

export const CREATE_PROCESS_DEFINITION = '[Process Definition] Create Process Definition';
export const CREATE_PROCESS_DEFINITION_SUCCESS = '[Process Definition] Create Process Definition Success';
export const CREATE_PROCESS_DEFINITION_FAIL = '[Process Definition] Create Process Definition Fail';
export const GET_PROCESS_DEFINITIONS = '[Process Definition] Get Process Definitions';
export const GET_PROCESS_DEFINITIONS_SUCCESS = '[Process Definition] Get Process Definitions Success';
export const GET_PROCESS_DEFINITIONS_FAIL = '[Process Definition] Get Process Definitions Fail';
export const UPDATE_PROCESS_DEFINITIONS = '[Process Definition] Update Process Definition';
export const UPDATE_PROCESS_DEFINITIONS_SUCCESS = '[Process Definition] Update Process Definition Success';
export const UPDATE_PROCESS_DEFINITIONS_FAIL = '[Process Definition] Update Process Definition Fail';
export const GET_PROCESS_DEFINITION_BY_ID = '[Process Definition] Get Process Definition By Id';
export const GET_PROCESS_DEFINITION_BY_ID_SUCCESS = '[Process Definition] Get Process Definition By Id Success';
export const GET_PROCESS_DEFINITION_BY_ID_FAIL = '[Process Definition] Get Process Definition By Id Fail';
export const DEPLOY_PROCESS_DEFINNITION = '[Process Definition] Deploy Process Definition';
export const DEPLOY_PROCESS_DEFINNITION_SUCCESS = '[Process Definition] Deploy Process Definition Success';
export const DEPLOY_PROCESS_DEFINNITION_FAIL = '[Process Definition] Deploy Process Definition Failed';

export class CreateProcessDefinition implements Action {
    readonly type = CREATE_PROCESS_DEFINITION;

    constructor(public payload: IProcessDefinition) {
    }
}

export class CreateProcessDefinitionSuccess implements Action {
    readonly type = CREATE_PROCESS_DEFINITION_SUCCESS;

    constructor(public payload: IProcessDefinition) {
    }
}

export class CreateProcessDefinitionFail implements Action {
    readonly type = CREATE_PROCESS_DEFINITION_FAIL;

    constructor(public payload: Error) {
    }
}

export class GetProcessDefinitions implements Action {
    readonly type = GET_PROCESS_DEFINITIONS;

    constructor() {
    }
}

export class GetProcessDefinitionsSuccess implements Action {
    readonly type = GET_PROCESS_DEFINITIONS_SUCCESS;

    constructor(public payload: IProcessDefinition[]) {
    }
}

export class GetProcessDefinitionFail implements Action {
    readonly type = GET_PROCESS_DEFINITIONS_FAIL;

    constructor(public payload: Error) {
    }
}

export class UpdateProcessDefinition implements Action {
    readonly type = UPDATE_PROCESS_DEFINITIONS;

    constructor(public payload: IUpdateProcessDefinitionPayload) {
    }
}

export class UpdateProcessDefinitionSuccess implements Action {
    readonly type = UPDATE_PROCESS_DEFINITIONS_SUCCESS;

    constructor(public payload: IProcessDefinition) {
    }
}

export class UpdateProcessDefinitionFail implements Action {
    readonly type = UPDATE_PROCESS_DEFINITIONS_FAIL;

    constructor(public error: Error) {
    }
}

export class GetProcessDefinitionById implements Action {
    readonly type = GET_PROCESS_DEFINITION_BY_ID;

    constructor(public payload: string) {
    }
}

export class GetProcessDefinitionByIdSuccess implements Action {
    readonly type = GET_PROCESS_DEFINITION_BY_ID_SUCCESS;

    constructor(public payload: IProcessDefinition) {
    }
}

export class GetProcessDefinitionByIdFail implements Action {
    readonly type = GET_PROCESS_DEFINITION_BY_ID_FAIL;

    constructor(public payload: Error) {
    }
}

export class DeployProcessDefinition implements Action {
    readonly type = DEPLOY_PROCESS_DEFINNITION;

    constructor(public payload: string) {
    }
}

export class DeployProcessDefinitionSuccess implements Action {
    readonly type = DEPLOY_PROCESS_DEFINNITION_SUCCESS;

    constructor(public payload: string) {
    }
}

export class DeployProcessDefinitionFail implements Action {
    readonly type = DEPLOY_PROCESS_DEFINNITION_FAIL;

    constructor(public payload: Error) {
    }
}

export type Actions =
    CreateProcessDefinition |
    CreateProcessDefinitionSuccess |
    CreateProcessDefinitionFail |
    DeployProcessDefinition |
    DeployProcessDefinitionSuccess |
    DeployProcessDefinitionFail |
    GetProcessDefinitions |
    GetProcessDefinitionsSuccess |
    GetProcessDefinitionFail |
    GetProcessDefinitionById |
    GetProcessDefinitionByIdSuccess |
    GetProcessDefinitionByIdFail |
    UpdateProcessDefinition |
    UpdateProcessDefinitionSuccess |
    UpdateProcessDefinitionFail;
