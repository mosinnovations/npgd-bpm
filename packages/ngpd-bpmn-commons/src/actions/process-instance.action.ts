import {Action} from '@ngrx/store';
import {IProcessInstance} from '../reducers/process-instance.reducer';
import {IProcessDefinition, ITask} from '../';

export const GET_PROCESS_INSTANCES = '[Process Instance] Get Process Instance';
export const GET_PROCESS_INSTANCES_SUCCESS = '[Process Instance] Get Process Instance Success';
export const GET_PROCESS_INSTANCES_FAIL = '[Process Instance] Get Process Instance Fail';
export const START_PROCESS_INSTANCE = '[Process Instance] Start Process Instance';
export const START_PROCESS_INSTANCE_SUCCESS = '[Process Instance] Start Process Instance Success';
export const START_PROCESS_INSTANCE_FAIL = '[Process Instance] Start Process Instance Fail';
export const COMPLETE_TASK = '[Process Instance] Complete Task';
export const COMPLETE_TASK_SUCCESS = '[Process Instance] Complete Task Success';
export const COMPLETE_TASK_FAIL = '[Process Instance] Complete Task Fail';

export class GetProcessInstances implements Action {
    readonly type = GET_PROCESS_INSTANCES;

    constructor(public payload: string) {
    }
}

export class GetProcessInstancesSuccess implements Action {
    readonly type = GET_PROCESS_INSTANCES_SUCCESS;

    constructor(public payload: IProcessInstance[]) {
    }
}

export class GetProcessInstancesFail implements Action {
    readonly type = GET_PROCESS_INSTANCES_FAIL;

    constructor(public payload: Error) {
    }
}

export class StartProcessInstance implements Action {
    readonly type = START_PROCESS_INSTANCE;

    constructor(public payload: IProcessDefinition) {
    }
}

export class StartProcessInstanceSuccess implements Action {
    readonly type = START_PROCESS_INSTANCE_SUCCESS;

    constructor(public payload: IProcessDefinition) {
    }
}

export class StartProcessInstanceFail implements Action {
    readonly type = START_PROCESS_INSTANCE_FAIL;

    constructor(public payload: IProcessDefinition) {
    }
}

export class CompleteTask implements Action {
    readonly type = COMPLETE_TASK;

    constructor(public payload: string) { // task id
    }
}

export class CompleteTaskSucess implements Action {
    readonly type = COMPLETE_TASK_SUCCESS;

    constructor(public payload: ITask) {
    }
}

export class CompleteTaskFail implements Action {
    readonly type = COMPLETE_TASK_FAIL;

    constructor(public error: Error) {
    }
}
