import * as projectActions from './actions/project.actions';
import * as pageContextActions from './actions/page-context.action';
import * as processDefinitionActions from './actions/process-definition.action';
import * as processInstanceActions from './actions/process-instance.action';
// global exports
export {
    IAppState,
    ILookupItem,
    IFormEvent,
    INavItem,
    commonReducers
} from './reducers';

export const commonActions = {
    projectActions,
    pageContextActions,
    processDefinitionActions,
    processInstanceActions
};

export { IError, IHttpError } from './errors';

// project exports
export { 
    getProjects as getProjectsSelector,
    IProject
} from './reducers/project.reducer';

// process definition exports
export {
    IProcessDefinition,
    IFormField,
    IFormInfo,
    IComponentConfig
} from './reducers/process-definition.reducer';

export {
    IUpdateProcessDefinitionPayload
} from './actions/process-definition.action';

// process instance exports
export {
    IProcessInstance,
    IGetProcessInstancePayload,
    ITask
} from './reducers/process-instance.reducer';


// page context
export {
    IPageContext,
    getTopNavState
} from './reducers/page-context.reducer';


