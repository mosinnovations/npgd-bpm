import { IProject } from "../src/reducers/project.reducer";

export const mockProjects: IProject[] = [
    {
        name: 'project-1',
        url: 'www.pj.com',
        status: 'start'
    },
    {
        name: 'project-2',
        url: 'www.pj2.com',
        status: 'in-progress'
    }
];
