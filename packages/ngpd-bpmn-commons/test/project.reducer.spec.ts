import { IProject, projects as projectReducer } from '../src/reducers/project.reducer';
import * as projectActions from '../src/actions/project.actions';
import { mockProjects } from './mocks';
import {expect} from 'chai';

describe(`Test project reducers`, function () {
    let initialState: IProject[];

    beforeEach(function () {
        initialState = [];

    });

    it(`'Load project' action should return a new state with projects`, function () {
        const results = projectReducer(initialState, new projectActions.LoadProjectsSuccess(mockProjects))
        expect(results).to.equal(mockProjects);
    });
});