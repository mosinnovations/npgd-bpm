export interface IProcessDefinition {
    _id: any;
    name: string;
    description?: string;
    configXml?: string;
    formInfoList?: IFormInfo[];
    deployed: boolean;
    definitionVersion?: number;
    definitionId?: string;
}
export interface IComponentConfig {
    componentKey: string;
    configData: any;
}
export interface IFormField {
    label: string;
    type: 'string' | 'integer' | 'date' | 'long' | 'boolean';
    id: string;
    options?: any[];
    controlConfig: {
        type: 'input' | 'select' | 'radio' | 'textarea' | 'checkbox' | 'toggle' | 'datePicker';
        width: string | number;
    };
}
export interface IFormInfo {
    formId: string;
    formKey: string;
    formName: string;
    fields: IFormField[];
    componentConfig: IComponentConfig;
}
export declare function processDefinitions(state: IProcessDefinition[] | undefined, action: any): IProcessDefinition[];
