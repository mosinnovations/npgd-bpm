import * as pageCtxActions from '../actions/page-context.action';
import { INavItem } from './index';
export interface IPageContext {
    show: {
        topNav: boolean;
        sideNav: boolean;
    };
    sideNavItems: INavItem[];
}
export declare function reducer(state: IPageContext | undefined, action: pageCtxActions.Actions): IPageContext;
export declare const getTopNavState: (state: IPageContext) => boolean;
