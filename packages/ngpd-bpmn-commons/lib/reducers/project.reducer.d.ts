import { IAppState } from './index';
import { IProject } from './project.reducer';
export interface IProject {
    id?: string;
    name: string;
    url?: string;
    status: 'in-progress' | 'success' | 'fail' | 'start';
}
export declare const projects: (projs: IProject[] | undefined, action: any) => IProject[];
export declare const getProjects: (state: IAppState) => IProject[];
