import * as fromProject from './project.reducer';
import { IProject } from './project.reducer';
import * as fromProcessDefinition from './process-definition.reducer';
import { IProcessDefinition } from './process-definition.reducer';
import { IPageContext } from './page-context.reducer';
import * as fromProcessInstances from './process-instance.reducer';
import * as pageContextActions from '../actions/page-context.action';
import { IProcessInstance } from './process-instance.reducer';
export interface IAppState {
    projects: IProject[];
    pageContext: IPageContext;
    processDefinitions: IProcessDefinition[];
    processInstances: IProcessInstance[];
}
export interface ILookupItem {
    label: string;
    value: string | number;
    icon?: string;
}
export interface IFormEvent<T> {
    meta: {
        isValid: boolean;
    };
    data: T;
}
export interface INavItem {
    label?: string;
    icon?: string;
    type: 'link' | 'action';
    tooltip?: string;
    path?: string;
    execute?: () => void;
}
export declare const commonReducers: {
    projects: (projs: fromProject.IProject[] | undefined, action: any) => fromProject.IProject[];
    pageContext: (state: IPageContext | undefined, action: pageContextActions.Actions) => IPageContext;
    processDefinitions: (state: fromProcessDefinition.IProcessDefinition[] | undefined, action: any) => fromProcessDefinition.IProcessDefinition[];
    processInstances: (state: fromProcessInstances.IProcessInstance[] | undefined, action: any) => any;
};
