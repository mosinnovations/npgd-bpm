export interface IProcessInstance {
    id: string;
    definitionId: string;
    businessKey?: string;
    caseInstanceId?: string;
    ended: boolean;
    suspended: boolean;
    tenantId?: string;
    tasks?: ITask[];
}
export interface ITask {
    id: string;
    name: string;
    assignee: string;
    created: string;
    due: string;
    description?: string;
    executionId: string;
    owner: string;
    processDefinitionId: string;
    processInstanceId: string;
    taskDefinitionKey: string;
    suspended: boolean;
    formKey: string;
    tenantId: string;
}
export interface IGetProcessInstancePayload {
    processInstance: IProcessInstance;
    tasks: ITask[];
}
export declare function processInstances(state: IProcessInstance[] | undefined, action: any): any;
