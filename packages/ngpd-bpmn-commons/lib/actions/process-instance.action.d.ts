import { Action } from '@ngrx/store';
import { IProcessInstance } from '../reducers/process-instance.reducer';
import { IProcessDefinition, ITask } from '../';
export declare const GET_PROCESS_INSTANCES = "[Process Instance] Get Process Instance";
export declare const GET_PROCESS_INSTANCES_SUCCESS = "[Process Instance] Get Process Instance Success";
export declare const GET_PROCESS_INSTANCES_FAIL = "[Process Instance] Get Process Instance Fail";
export declare const START_PROCESS_INSTANCE = "[Process Instance] Start Process Instance";
export declare const START_PROCESS_INSTANCE_SUCCESS = "[Process Instance] Start Process Instance Success";
export declare const START_PROCESS_INSTANCE_FAIL = "[Process Instance] Start Process Instance Fail";
export declare const COMPLETE_TASK = "[Process Instance] Complete Task";
export declare const COMPLETE_TASK_SUCCESS = "[Process Instance] Complete Task Success";
export declare const COMPLETE_TASK_FAIL = "[Process Instance] Complete Task Fail";
export declare class GetProcessInstances implements Action {
    payload: string;
    readonly type: string;
    constructor(payload: string);
}
export declare class GetProcessInstancesSuccess implements Action {
    payload: IProcessInstance[];
    readonly type: string;
    constructor(payload: IProcessInstance[]);
}
export declare class GetProcessInstancesFail implements Action {
    payload: Error;
    readonly type: string;
    constructor(payload: Error);
}
export declare class StartProcessInstance implements Action {
    payload: IProcessDefinition;
    readonly type: string;
    constructor(payload: IProcessDefinition);
}
export declare class StartProcessInstanceSuccess implements Action {
    payload: IProcessDefinition;
    readonly type: string;
    constructor(payload: IProcessDefinition);
}
export declare class StartProcessInstanceFail implements Action {
    payload: IProcessDefinition;
    readonly type: string;
    constructor(payload: IProcessDefinition);
}
export declare class CompleteTask implements Action {
    payload: string;
    readonly type: string;
    constructor(payload: string);
}
export declare class CompleteTaskSucess implements Action {
    payload: ITask;
    readonly type: string;
    constructor(payload: ITask);
}
export declare class CompleteTaskFail implements Action {
    error: Error;
    readonly type: string;
    constructor(error: Error);
}
