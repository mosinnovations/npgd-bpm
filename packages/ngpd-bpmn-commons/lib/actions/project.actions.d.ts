import { Action } from '@ngrx/store';
import { IProject } from '../reducers/project.reducer';
export declare const ActionTypes: {
    CREATE_PROJECT: string;
    CREATE_PROJECT_SUCCESS: string;
    CREATE_PROJECT_FAIL: string;
    LOAD_PROJECTS: string;
    LOAD_PROJECTS_SUCCESS: string;
    LOAD_PROJECTS_FAIL: string;
};
export declare class AddProjectAction implements Action {
    payload: IProject;
    type: string;
    constructor(payload: IProject);
}
export declare class AddProjectActionSuccess implements Action {
    payload: IProject;
    type: string;
    constructor(payload: IProject);
}
export declare class AddProjectActionFail implements Action {
    payload: Error;
    type: string;
    constructor(payload: Error);
}
export declare class LoadProjects implements Action {
    type: string;
    constructor();
}
export declare class LoadProjectsSuccess implements Action {
    payload: IProject[] | undefined;
    type: string;
    constructor(payload?: IProject[] | undefined);
}
export declare class LoadProjectsFail implements Action {
    payload: Error;
    type: string;
    constructor(payload: Error);
}
