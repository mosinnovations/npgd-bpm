export var ActionTypes = {
    CREATE_PROJECT: '[Project] Create project',
    CREATE_PROJECT_SUCCESS: '[Project] Create project success',
    CREATE_PROJECT_FAIL: '[Project] Create project fail',
    LOAD_PROJECTS: '[Project] Load Projects',
    LOAD_PROJECTS_SUCCESS: '[Project] Load Projects success',
    LOAD_PROJECTS_FAIL: '[Project] Load Projects fail'
};
var AddProjectAction = (function () {
    function AddProjectAction(payload) {
        this.payload = payload;
        this.type = ActionTypes.CREATE_PROJECT;
    }
    return AddProjectAction;
}());
export { AddProjectAction };
var AddProjectActionSuccess = (function () {
    function AddProjectActionSuccess(payload) {
        this.payload = payload;
        this.type = ActionTypes.CREATE_PROJECT_SUCCESS;
    }
    return AddProjectActionSuccess;
}());
export { AddProjectActionSuccess };
var AddProjectActionFail = (function () {
    function AddProjectActionFail(payload) {
        this.payload = payload;
        this.type = ActionTypes.CREATE_PROJECT_FAIL;
    }
    return AddProjectActionFail;
}());
export { AddProjectActionFail };
var LoadProjects = (function () {
    function LoadProjects() {
        this.type = ActionTypes.LOAD_PROJECTS;
    }
    return LoadProjects;
}());
export { LoadProjects };
var LoadProjectsSuccess = (function () {
    function LoadProjectsSuccess(payload) {
        this.payload = payload;
        this.type = ActionTypes.LOAD_PROJECTS_SUCCESS;
    }
    return LoadProjectsSuccess;
}());
export { LoadProjectsSuccess };
var LoadProjectsFail = (function () {
    function LoadProjectsFail(payload) {
        this.payload = payload;
        this.type = ActionTypes.LOAD_PROJECTS_FAIL;
    }
    return LoadProjectsFail;
}());
export { LoadProjectsFail };

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9hY3Rpb25zL3Byb2plY3QuYWN0aW9ucy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFHQSxNQUFNLENBQUMsSUFBTSxXQUFXLEdBQUc7SUFDekIsY0FBYyxFQUFFLDBCQUEwQjtJQUMxQyxzQkFBc0IsRUFBRSxrQ0FBa0M7SUFDMUQsbUJBQW1CLEVBQUUsK0JBQStCO0lBQ3BELGFBQWEsRUFBRSx5QkFBeUI7SUFDeEMscUJBQXFCLEVBQUUsaUNBQWlDO0lBQ3hELGtCQUFrQixFQUFFLDhCQUE4QjtDQUNuRCxDQUFDO0FBRUY7SUFFRSwwQkFBbUIsT0FBaUI7UUFBakIsWUFBTyxHQUFQLE9BQU8sQ0FBVTtRQURwQyxTQUFJLEdBQVcsV0FBVyxDQUFDLGNBQWMsQ0FBQztJQUNGLENBQUM7SUFDM0MsdUJBQUM7QUFBRCxDQUhBLEFBR0MsSUFBQTs7QUFDRDtJQUVFLGlDQUFtQixPQUFpQjtRQUFqQixZQUFPLEdBQVAsT0FBTyxDQUFVO1FBRHBDLFNBQUksR0FBVyxXQUFXLENBQUMsc0JBQXNCLENBQUM7SUFDVixDQUFDO0lBQzNDLDhCQUFDO0FBQUQsQ0FIQSxBQUdDLElBQUE7O0FBQ0Q7SUFFRSw4QkFBbUIsT0FBYztRQUFkLFlBQU8sR0FBUCxPQUFPLENBQU87UUFEakMsU0FBSSxHQUFXLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQztJQUNWLENBQUM7SUFDeEMsMkJBQUM7QUFBRCxDQUhBLEFBR0MsSUFBQTs7QUFFRDtJQUVFO1FBREEsU0FBSSxHQUFXLFdBQVcsQ0FBQyxhQUFhLENBQUM7SUFDMUIsQ0FBQztJQUNsQixtQkFBQztBQUFELENBSEEsQUFHQyxJQUFBOztBQUNEO0lBRUUsNkJBQW1CLE9BQW9CO1FBQXBCLFlBQU8sR0FBUCxPQUFPLENBQWE7UUFEdkMsU0FBSSxHQUFXLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQztJQUNQLENBQUM7SUFDN0MsMEJBQUM7QUFBRCxDQUhBLEFBR0MsSUFBQTs7QUFDRDtJQUVFLDBCQUFtQixPQUFjO1FBQWQsWUFBTyxHQUFQLE9BQU8sQ0FBTztRQURqQyxTQUFJLEdBQVcsV0FBVyxDQUFDLGtCQUFrQixDQUFDO0lBQ1YsQ0FBQztJQUN2Qyx1QkFBQztBQUFELENBSEEsQUFHQyxJQUFBIiwiZmlsZSI6ImFjdGlvbnMvcHJvamVjdC5hY3Rpb25zLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQWN0aW9uIH0gZnJvbSAnQG5ncngvc3RvcmUnO1xuaW1wb3J0IHsgSVByb2plY3QgfSBmcm9tICcuLi9yZWR1Y2Vycy9wcm9qZWN0LnJlZHVjZXInO1xuXG5leHBvcnQgY29uc3QgQWN0aW9uVHlwZXMgPSB7XG4gIENSRUFURV9QUk9KRUNUOiAnW1Byb2plY3RdIENyZWF0ZSBwcm9qZWN0JyxcbiAgQ1JFQVRFX1BST0pFQ1RfU1VDQ0VTUzogJ1tQcm9qZWN0XSBDcmVhdGUgcHJvamVjdCBzdWNjZXNzJyxcbiAgQ1JFQVRFX1BST0pFQ1RfRkFJTDogJ1tQcm9qZWN0XSBDcmVhdGUgcHJvamVjdCBmYWlsJyxcbiAgTE9BRF9QUk9KRUNUUzogJ1tQcm9qZWN0XSBMb2FkIFByb2plY3RzJyxcbiAgTE9BRF9QUk9KRUNUU19TVUNDRVNTOiAnW1Byb2plY3RdIExvYWQgUHJvamVjdHMgc3VjY2VzcycsXG4gIExPQURfUFJPSkVDVFNfRkFJTDogJ1tQcm9qZWN0XSBMb2FkIFByb2plY3RzIGZhaWwnXG59O1xuXG5leHBvcnQgY2xhc3MgQWRkUHJvamVjdEFjdGlvbiBpbXBsZW1lbnRzIEFjdGlvbiB7XG4gIHR5cGU6IHN0cmluZyA9IEFjdGlvblR5cGVzLkNSRUFURV9QUk9KRUNUO1xuICBjb25zdHJ1Y3RvcihwdWJsaWMgcGF5bG9hZDogSVByb2plY3QpIHsgfVxufVxuZXhwb3J0IGNsYXNzIEFkZFByb2plY3RBY3Rpb25TdWNjZXNzIGltcGxlbWVudHMgQWN0aW9uIHtcbiAgdHlwZTogc3RyaW5nID0gQWN0aW9uVHlwZXMuQ1JFQVRFX1BST0pFQ1RfU1VDQ0VTUztcbiAgY29uc3RydWN0b3IocHVibGljIHBheWxvYWQ6IElQcm9qZWN0KSB7IH1cbn1cbmV4cG9ydCBjbGFzcyBBZGRQcm9qZWN0QWN0aW9uRmFpbCBpbXBsZW1lbnRzIEFjdGlvbiB7XG4gIHR5cGU6IHN0cmluZyA9IEFjdGlvblR5cGVzLkNSRUFURV9QUk9KRUNUX0ZBSUw7XG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBwYXlsb2FkOiBFcnJvcikgeyB9XG59XG5cbmV4cG9ydCBjbGFzcyBMb2FkUHJvamVjdHMgaW1wbGVtZW50cyBBY3Rpb24ge1xuICB0eXBlOiBzdHJpbmcgPSBBY3Rpb25UeXBlcy5MT0FEX1BST0pFQ1RTO1xuICBjb25zdHJ1Y3RvcigpIHt9XG59XG5leHBvcnQgY2xhc3MgTG9hZFByb2plY3RzU3VjY2VzcyBpbXBsZW1lbnRzIEFjdGlvbiB7XG4gIHR5cGU6IHN0cmluZyA9IEFjdGlvblR5cGVzLkxPQURfUFJPSkVDVFNfU1VDQ0VTUztcbiAgY29uc3RydWN0b3IocHVibGljIHBheWxvYWQ/OiBJUHJvamVjdFtdKSB7fVxufVxuZXhwb3J0IGNsYXNzIExvYWRQcm9qZWN0c0ZhaWwgaW1wbGVtZW50cyBBY3Rpb24ge1xuICB0eXBlOiBzdHJpbmcgPSBBY3Rpb25UeXBlcy5MT0FEX1BST0pFQ1RTX0ZBSUw7XG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBwYXlsb2FkOiBFcnJvcikge31cbn1cbiJdLCJzb3VyY2VSb290IjoiLi4ifQ==
