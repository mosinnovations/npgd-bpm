import { Action } from '@ngrx/store';
import { IProcessDefinition } from '../reducers/process-definition.reducer';
export interface IUpdateProcessDefinitionPayload {
    updateFrom: 'modeler' | 'definitionForms';
    data: IProcessDefinition;
}
export declare const CREATE_PROCESS_DEFINITION = "[Process Definition] Create Process Definition";
export declare const CREATE_PROCESS_DEFINITION_SUCCESS = "[Process Definition] Create Process Definition Success";
export declare const CREATE_PROCESS_DEFINITION_FAIL = "[Process Definition] Create Process Definition Fail";
export declare const GET_PROCESS_DEFINITIONS = "[Process Definition] Get Process Definitions";
export declare const GET_PROCESS_DEFINITIONS_SUCCESS = "[Process Definition] Get Process Definitions Success";
export declare const GET_PROCESS_DEFINITIONS_FAIL = "[Process Definition] Get Process Definitions Fail";
export declare const UPDATE_PROCESS_DEFINITIONS = "[Process Definition] Update Process Definition";
export declare const UPDATE_PROCESS_DEFINITIONS_SUCCESS = "[Process Definition] Update Process Definition Success";
export declare const UPDATE_PROCESS_DEFINITIONS_FAIL = "[Process Definition] Update Process Definition Fail";
export declare const GET_PROCESS_DEFINITION_BY_ID = "[Process Definition] Get Process Definition By Id";
export declare const GET_PROCESS_DEFINITION_BY_ID_SUCCESS = "[Process Definition] Get Process Definition By Id Success";
export declare const GET_PROCESS_DEFINITION_BY_ID_FAIL = "[Process Definition] Get Process Definition By Id Fail";
export declare const DEPLOY_PROCESS_DEFINNITION = "[Process Definition] Deploy Process Definition";
export declare const DEPLOY_PROCESS_DEFINNITION_SUCCESS = "[Process Definition] Deploy Process Definition Success";
export declare const DEPLOY_PROCESS_DEFINNITION_FAIL = "[Process Definition] Deploy Process Definition Failed";
export declare class CreateProcessDefinition implements Action {
    payload: IProcessDefinition;
    readonly type: string;
    constructor(payload: IProcessDefinition);
}
export declare class CreateProcessDefinitionSuccess implements Action {
    payload: IProcessDefinition;
    readonly type: string;
    constructor(payload: IProcessDefinition);
}
export declare class CreateProcessDefinitionFail implements Action {
    payload: Error;
    readonly type: string;
    constructor(payload: Error);
}
export declare class GetProcessDefinitions implements Action {
    readonly type: string;
    constructor();
}
export declare class GetProcessDefinitionsSuccess implements Action {
    payload: IProcessDefinition[];
    readonly type: string;
    constructor(payload: IProcessDefinition[]);
}
export declare class GetProcessDefinitionFail implements Action {
    payload: Error;
    readonly type: string;
    constructor(payload: Error);
}
export declare class UpdateProcessDefinition implements Action {
    payload: IUpdateProcessDefinitionPayload;
    readonly type: string;
    constructor(payload: IUpdateProcessDefinitionPayload);
}
export declare class UpdateProcessDefinitionSuccess implements Action {
    payload: IProcessDefinition;
    readonly type: string;
    constructor(payload: IProcessDefinition);
}
export declare class UpdateProcessDefinitionFail implements Action {
    error: Error;
    readonly type: string;
    constructor(error: Error);
}
export declare class GetProcessDefinitionById implements Action {
    payload: string;
    readonly type: string;
    constructor(payload: string);
}
export declare class GetProcessDefinitionByIdSuccess implements Action {
    payload: IProcessDefinition;
    readonly type: string;
    constructor(payload: IProcessDefinition);
}
export declare class GetProcessDefinitionByIdFail implements Action {
    payload: Error;
    readonly type: string;
    constructor(payload: Error);
}
export declare class DeployProcessDefinition implements Action {
    payload: string;
    readonly type: string;
    constructor(payload: string);
}
export declare class DeployProcessDefinitionSuccess implements Action {
    payload: string;
    readonly type: string;
    constructor(payload: string);
}
export declare class DeployProcessDefinitionFail implements Action {
    payload: Error;
    readonly type: string;
    constructor(payload: Error);
}
export declare type Actions = CreateProcessDefinition | CreateProcessDefinitionSuccess | CreateProcessDefinitionFail | DeployProcessDefinition | DeployProcessDefinitionSuccess | DeployProcessDefinitionFail | GetProcessDefinitions | GetProcessDefinitionsSuccess | GetProcessDefinitionFail | GetProcessDefinitionById | GetProcessDefinitionByIdSuccess | GetProcessDefinitionByIdFail | UpdateProcessDefinition | UpdateProcessDefinitionSuccess | UpdateProcessDefinitionFail;
