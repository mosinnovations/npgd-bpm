import { Action } from '@ngrx/store';
import { INavItem } from '../reducers';
export declare const SHOW_TOPNAV = "[Layout] Show TopNav";
export declare const HIDE_TOPNAV = "[Layout] Hide TopNav";
export declare const SHOW_SIDENAV = "[Layout] Show SideNav";
export declare const HIDE_SIDENAV = "[Layout] Hide SideNav";
export declare const UPDATE_SIDENAV_ITEMS = "[Layout] Update Side Items";
export declare class ShowTopNav implements Action {
    readonly type: string;
}
export declare class HideTopNav implements Action {
    readonly type: string;
}
export declare class ShowSideNav implements Action {
    readonly type: string;
}
export declare class HideSideNav implements Action {
    readonly type: string;
}
export declare class UpdateSideNavItems implements Action {
    payload: INavItem[];
    readonly type: string;
    constructor(payload: INavItem[]);
}
export declare type Actions = ShowTopNav | HideTopNav | ShowSideNav | HideSideNav | UpdateSideNavItems;
