import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, EventEmitter } from '@angular/core';

@Component({
  selector: 'mercer-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectComponent implements OnInit {
  public config: any = {};
  public group: any = {};
  public fieldFocus: EventEmitter<boolean> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
