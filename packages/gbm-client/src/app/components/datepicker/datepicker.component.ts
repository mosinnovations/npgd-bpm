import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, EventEmitter } from '@angular/core';

@Component({
  selector: 'gbm-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatepickerComponent implements OnInit {
  public config: any = {};
  public group: any = {};
  public fieldFocus: EventEmitter<boolean> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
