import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { InputComponent } from './input/input.component';
import { ToggleComponent } from './toggle/toggle.component';
import { SelectComponent } from './select/select.component';
import { DatepickerComponent } from './datepicker/datepicker.component';
import { MercerOSModule } from 'merceros-ui-components';
import { MultiselectComponent } from './multiselect/multiselect.component';

export const COMPONENTS = [
  InputComponent,
  ToggleComponent,
  SelectComponent,
  MultiselectComponent,
  DatepickerComponent,
]

@NgModule({
  declarations: [
    ...COMPONENTS,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MercerOSModule,
  ],
  exports: [...COMPONENTS],
  entryComponents: [
    ...COMPONENTS
  ],
  providers: [],
})
export class ComponentsModule { }
