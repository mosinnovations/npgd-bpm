import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, EventEmitter } from '@angular/core';

@Component({
  selector: 'gbm-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InputComponent implements OnInit {
  public config: any = {};
  public group: any = {};
  public fieldFocus: EventEmitter<boolean> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
