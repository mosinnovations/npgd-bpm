import { InputComponent } from './input/input.component';
import { ToggleComponent } from './toggle/toggle.component';
import { SelectComponent } from './select/select.component';
import { DatepickerComponent } from './datepicker/datepicker.component';

export const FORM_COMPONENTS = {
  toggle: ToggleComponent,
  input: InputComponent,
  select: SelectComponent,
  datePicker: DatepickerComponent,
};
