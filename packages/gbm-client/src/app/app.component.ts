import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import { MosConfigurationService } from 'merceros-ui-components/services';

@Component({
  selector: 'mercer-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit {

  constructor(public mosConfig: MosConfigurationService) { }

  ngOnInit() {
  }

}
