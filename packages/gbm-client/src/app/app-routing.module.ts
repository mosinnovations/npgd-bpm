import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { WorkspaceComponent } from './features/workspace/workspace.component';

const routes: Routes = [
  {
    path: 'process-list', component: WorkspaceComponent
  },
  {
    path: '*', redirectTo: ''
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: false, preloadingStrategy: PreloadAllModules })
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
