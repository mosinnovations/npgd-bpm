import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MercerOSModule } from 'merceros-ui-components';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ServicesModule } from './services/services.module';
import { FeaturesModule } from './features/features.module';

import 'rxjs/add/operator/switchMap';


@NgModule({
  imports: [
    BrowserAnimationsModule,
    MercerOSModule,
    CommonModule,
    AppRoutingModule,
    ServicesModule,
    FeaturesModule,
  ],
  declarations: [AppComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
