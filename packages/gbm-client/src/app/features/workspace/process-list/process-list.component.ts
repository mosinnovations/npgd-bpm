import { Component, ViewEncapsulation, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-process-list',
  templateUrl: './process-list.component.html',
  styleUrls: ['./process-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProcessListComponent {
  @Input() processes: any[];
  @Input() currentProcessId: string;
  @Input() disableSubmit = true;
  @Output() selectProcess: EventEmitter<any> = new EventEmitter();
  constructor() { }

  public handleClickProcess(process: any) {
    this.selectProcess.emit(process);
  }
}
