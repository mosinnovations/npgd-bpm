import { Component, OnInit, Input, ViewEncapsulation, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormDataService } from '../../../services/form-data.service';

@Component({
  selector: 'gbm-right-side-nav',
  templateUrl: './right-side-nav.component.html',
  styleUrls: ['./right-side-nav.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RightSideNavComponent implements OnInit {
  progress = 10;
  @Input() progressTitle = '';
  public forms: any[] = [];
  public contacts: any[] = [];
  public form: any;

  constructor(private formService: FormDataService, private cdr: ChangeDetectorRef) { }

  ngOnInit() {
    this.formService.getStandaloneForms()
      .subscribe((forms: any[]) => {
        this.forms = forms.map((form) => {
          return { ...form, formId: form._id };
        });
        this.form = this.forms[0];
        this.cdr.detectChanges();

      });
  }

  handleFormSubmit($event) {
    this.contacts.push($event.collectedData);
  }

}
