import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BPMNModelService } from '../../services/bpmn-model.services';
import { FormDataService } from '../../services/form-data.service';
import * as commons from 'ngpd-bpmn-commons';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'gbm-workspace',
  templateUrl: './workspace.component.html',
  styleUrls: ['./workspace.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkspaceComponent implements OnInit {
  public currentProcess: any;
  public currentProcessData: any;
  public currentProcessProgress: number;
  // public processInstances: commons.IProcessInstance[];
  public processInstances: any[];
  private _processes: any[];

  constructor(private modelService: BPMNModelService,
    private formDataService: FormDataService,
    private cdr: ChangeDetectorRef) {
    this.processInstances = [];
  }

  public ngOnInit() {
    this.modelService.getProcessDefintions().subscribe((processes: any[]) => {
      this.processes = processes;
      this.cdr.detectChanges();
    });
  }

  public handleSelectProcess(process: any) {
    if (this.currentProcess && this.currentProcess._id === process._id) {
      this.currentProcess = null;
    } else {
      this.currentProcess = process;
      this.modelService.getProcessInstance(this.currentProcess.definitionId).subscribe((instances: commons.IProcessInstance[]) => {
        this.processInstances = this.mapProcesses(instances);
        if (!this.processInstances[0]) {
          this.handleStartNewProcess(this.currentProcess);
        }
        this.cdr.detectChanges();
      });
    }
  }

  public handleCompleteTask(payload: { taskId: string; dataCollected: any }) {
    let variables: any;
    if (payload.dataCollected) {
      const collectedData = payload.dataCollected.collectedData;
      variables = {
        variables: Object.keys(collectedData).reduce((aggr: any, k: string) => {
          const v = collectedData[k];
          aggr[k] = {
            value: v,
            type: 'String'
          };
          return aggr;
        }, {})
      };
    }

    this.modelService.completeTask(payload.taskId, variables).switchMap((res) => {
      return this.modelService.getProcessInstance(this.currentProcess.definitionId)
        .map((_res) => (_res.json) ? _res.json() : _res)
        .catch((e) => Observable.of(e));
    }).subscribe((res) => {
      this.processInstances = this.mapProcesses(res);
      this.cdr.detectChanges();
    });
  }

  public handleStartNewProcess(processDef: commons.IProcessDefinition) {
    this.modelService.startNewProcess(processDef.definitionId).switchMap((res) => {
      return this.modelService.getProcessInstance(processDef.definitionId)
        .map((_res) => (_res.json) ? _res.json() : _res)
        .catch((e) => Observable.of(e));
    }).subscribe((res) => {
      this.processInstances = this.mapProcesses(res);
      this.cdr.detectChanges();
    });
  }

  public handleFormSubmit(formData: any) {
    this.formDataService.submitFormData(formData)
      .subscribe((res) => {
        console.log(res);
        console.log(this.currentProcess);
      });
  }

  private mapProcesses(processInstances) {
    return processInstances.map((instance) => {
      const tasks = instance.tasks.reduce((accum: any[], curr: any) => {
        const fields = [];
        curr.formInfo.fields.forEach((section) => {
          fields.push({ ...curr, fields: section.fields, name: section.name });
        });
        return accum.concat(fields);
      }, []);
      return { ...instance, tasks };
    });
  }

  get processes() {
    return this._processes ? [...this._processes] : [];
  }

  set processes(processes: any[]) {
    if (processes && processes.length) {
      this._processes = [...processes];
    }
  }
}
