import {
  Component,
  OnInit,
  OnChanges,
  SimpleChanges,
  ViewEncapsulation,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Input,
  Output,
  EventEmitter
} from '@angular/core';

@Component({
  selector: 'gbm-dynamic-workspace',
  templateUrl: './dynamic-workspace.component.html',
  styleUrls: ['./dynamic-workspace.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DynamicWorkspaceComponent implements OnInit, OnChanges {
  @Input() process: any;
  @Input() processInstances: any[];
  @Input() collectedData: any;
  @Output() submitForm: EventEmitter<any> = new EventEmitter();
  @Output() onStartProcess: EventEmitter<any> = new EventEmitter();
  @Output() onCompleteTask: EventEmitter<any> = new EventEmitter();

  constructor(private cdr: ChangeDetectorRef) {
  }

  public ngOnInit() {
  }

  public ngOnChanges(changes: SimpleChanges) {
    console.log(this.processInstances);
    if (changes.collectedData && this.collectedData) {
      const processFormMap = {};
      this.process.formInfoList.forEach((formInfo, index) => {
        processFormMap[formInfo.formId] = index;
      });

      // this.collectedData.forEach((formData: any) => {
      //   const formInfoIndex = processFormMap[formData.formId];
      //   this.process.formInfoList[formInfoIndex].collectedData = formData.collectedData;
      //   this.process.formInfoList[formInfoIndex].closed = true;
      // });


      this.cdr.detectChanges();
    }
  }

  public handleFormSubmit(form: any) {
    this.submitForm.emit(form);
  }

  public handleCompleteTask(taskId: string, dataCollected?: any) {
    this.onCompleteTask.emit({
      taskId,
      dataCollected
    });
  }

  public handleStartNewProcess() {
    this.onStartProcess.emit(this.process);
  }

}
