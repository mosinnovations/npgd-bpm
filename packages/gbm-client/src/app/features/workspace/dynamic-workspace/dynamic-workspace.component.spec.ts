import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicWorkspaceComponent } from './dynamic-workspace.component';

describe('DynamicWorkspaceComponent', () => {
  let component: DynamicWorkspaceComponent;
  let fixture: ComponentFixture<DynamicWorkspaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicWorkspaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicWorkspaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
