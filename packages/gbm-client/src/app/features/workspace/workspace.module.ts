import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GbmModule, GbmFormFieldService } from 'gbm-components';
import { MercerOSModule } from 'merceros-ui-components';

import { ComponentsModule } from '../../components/components.module';
import { WorkspaceComponent } from './workspace.component';
import { ProcessListComponent } from './process-list/process-list.component';
import { DynamicWorkspaceComponent } from './dynamic-workspace/dynamic-workspace.component';
import { RightSideNavComponent } from './right-side-nav/right-side-nav.component';
import { FormFieldService } from '../../services/form-field.service';

export const COMPONENTS = [
  WorkspaceComponent,
  DynamicWorkspaceComponent,
  RightSideNavComponent,
  ProcessListComponent
];

@NgModule({
  declarations: [
    ...COMPONENTS,
  ],
  exports: [
    ...COMPONENTS
  ],
  imports: [
    CommonModule, ComponentsModule, MercerOSModule, GbmModule
  ],
  providers: [{
    provide: GbmFormFieldService,
    useClass: FormFieldService
  }],
})
export class WorkspaceModule { }
