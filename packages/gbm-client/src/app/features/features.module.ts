import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicPreviewComponent } from './dynamic-preview/dynamic-preview.component';
import { ComponentsModule } from '../components/components.module';
import { MercerOSModule } from 'merceros-ui-components';
import { WorkspaceModule } from './workspace/workspace.module';

export const FEATURE_MODULES = [
  WorkspaceModule
];

@NgModule({
  declarations: [
    DynamicPreviewComponent,
  ],
  imports: [
    CommonModule, ComponentsModule, MercerOSModule, ...FEATURE_MODULES
  ],
  providers: [],
})
export class FeaturesModule { }
