import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router/src/router_state';

@Component({
  selector: 'app-dynamic-preview',
  templateUrl: './dynamic-preview.component.html',
  styleUrls: ['./dynamic-preview.component.css']
})
export class DynamicPreviewComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
