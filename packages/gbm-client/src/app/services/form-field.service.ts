import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { ToggleComponent } from '../components/toggle/toggle.component';
import { InputComponent } from '../components/input/input.component';
import { SelectComponent } from '../components/select/select.component';
import { DatepickerComponent } from '../components/datepicker/datepicker.component';
import { GbmFormFieldService } from 'gbm-components';
import { MultiselectComponent } from '../components/multiselect/multiselect.component';

const FORM_COMPONENTS = {
  multiselect: MultiselectComponent,
  toggle: ToggleComponent,
  input: InputComponent,
  select: SelectComponent,
  datepicker: DatepickerComponent,
};

@Injectable()
export class FormFieldService extends GbmFormFieldService {
  public _formComponents: any;

  constructor() {
    super();
    this._formComponents = FORM_COMPONENTS;
  }
}


