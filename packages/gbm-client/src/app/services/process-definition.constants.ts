import * as commons from 'ngpd-bpmn-commons';

export const uiControls: {
  [key: string]: commons.ILookupItem[];
} = {
    string: [
      {
        label: 'Text Input',
        value: 'input'
      },
      {
        label: 'Select List',
        value: 'select'
      }
    ],
    long: [
      {
        label: 'Text Input',
        value: 'input'
      },
      {
        label: 'Select List',
        value: 'select'
      }
    ],
    date: [
      {
        label: 'Text Input',
        value: 'input'
      },
      {
        label: 'Date Picker',
        value: 'datePicker'
      }
    ],
    boolean: [
      {
        label: 'Radio Button',
        value: 'radio'
      },
      {
        label: 'Toggle',
        value: 'toggle'
      }
    ]
  };
