class GeneralUtils {
  constructor() {
  }

  public withDefault(obj: any, defaultValue: any) {
    if (obj instanceof Array) {
      if (!obj || obj.length === 0) {
        return defaultValue;
      }
    }
    if (!obj) {
      return defaultValue;
    }
    return obj;
  }
}

export const generalUtils = new GeneralUtils();
