import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';

import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { environment } from '../../environments/environment';

const apiUrl = `${environment.api}/${environment.apiVersion}/forms`;
const formRefUrl = `${environment.api}/${environment.apiVersion}/form-ref`;

export interface FormData {
  formId: string;
  userId: string;
  collectedData: any;
}

@Injectable()
export class FormDataService {
  constructor(private http: Http) {
  }

  public submitFormData(formResults: FormData) {
    return this.http.post(apiUrl, formResults)
      .map((res: Response) => res.json ? res.json() : res)
      .catch((error: any) => Observable.of(error.json ? error.json() : error));
  }

  public updateFormData(id: string, formResults: FormData) {
    return this.http.put(`${apiUrl}/${id}`, formResults)
      .map((res: Response) => res.json ? res.json() : res)
      .catch((error: any) => Observable.of(error.json ? error.json() : error));
  }

  public getFormData(formIds: string[]) {
    const queryString = formIds.reduce((query: string, formId: string) => {
      return query ? `${query}&id=${formId}` : `id=${formId}`;
    }, '');

    return this.http.get(`${apiUrl}?${queryString}`)
      .map((res: Response) => res.json ? res.json() : res)
      .catch((error: any) => Observable.of(error.json ? error.json() : error));
  }

  public getStandaloneForms() {
    return this.http.get(formRefUrl)
      .map((res: Response) => res.json ? res.json() : res)
      .catch((error: any) => Observable.of(error.json ? error.json() : error));
  }
}


