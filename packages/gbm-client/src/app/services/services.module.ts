
import { NgModule } from '@angular/core';
import { BPMNModelService } from './bpmn-model.services';
import { FormDataService } from './form-data.service';
import { HttpModule } from '@angular/http';

@NgModule({
  providers: [
    BPMNModelService,
    FormDataService,
  ],
  imports: [
    HttpModule,
  ]
})
export class ServicesModule {}
