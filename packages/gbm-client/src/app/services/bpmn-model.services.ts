import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';

import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {environment} from '../../environments/environment';

const processDefApiiUrl = `${environment.modelApi}/${environment.apiVersion}/processDefinitions`;

const processInstanceUrl = `${environment.modelApi}/${environment.apiVersion}/processInstances`;
const taskUrl = `${environment.modelApi}/${environment.apiVersion}/tasks`;

@Injectable()
export class BPMNModelService {
  constructor(private http: Http) {
  }

  public getProcessDefintions() {
    return this.http.get(processDefApiiUrl).map((res: Response) => res.json ? res.json() : res)
      .catch((error: any) => Observable.of(error.json ? error.json() : error));
  }

  public getProcessDefinitionById(id: string) {
    return this.http.get(processDefApiiUrl + '/' + id);
  }

  public getProcessInstance(definitionId: string) {
    return this.http.get(processInstanceUrl + '?processDefinitionId=' + definitionId).map((res: Response) => res.json ? res.json() : res)
      .catch((error: any) => Observable.of(error.json ? error.json() : error));
  }

  public startNewProcess(definitionId: string) {
    return this.http.post(processInstanceUrl + '/start/' + definitionId, {
      variables:
        {
          initiator: {
            value: 'Leo Jin', "type": "String"
          }
        }
    }).map((res: Response) => res.json ? res.json() : res)
      .catch((error: any) => Observable.of(error.json ? error.json() : error));
  }

  public completeTask(taskId: string, variables: any) {
    if(!variables) {
      variables = {
        variables: {
          taskCompleted: {
            value: true,
            type: 'Boolean'
          }
        }
      }
    }
    return this.http.post(`${taskUrl}/${taskId}/complete`, variables).map((res: Response) => res.json ? res.json() : res)
      .catch((error: any) => Observable.of(error.json ? error.json() : error));
  }
}


