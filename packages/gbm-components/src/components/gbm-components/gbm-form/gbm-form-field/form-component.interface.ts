import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';

export interface FormFieldConfig {
  id: string;
  label: string;
  controlConfig: {
    type: string;
    width?: number;
    className?: string;
  };
}

// export interface FormField extends Component {
//   config: FormFieldConfig;
//   group: FormGroup;
// }

export abstract class FormField {
  config: FormFieldConfig;
  group: FormGroup;
}