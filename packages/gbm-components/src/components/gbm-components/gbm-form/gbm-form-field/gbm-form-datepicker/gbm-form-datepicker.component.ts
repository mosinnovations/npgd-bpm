import { Component, OnInit, EventEmitter } from '@angular/core';
import { FormField, FormFieldConfig } from '../form-component.interface';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-gbm-form-datepicker',
  templateUrl: './gbm-form-datepicker.component.html',
  styleUrls: ['./gbm-form-datepicker.component.css']
})
export class GbmFormDatepickerComponent implements OnInit, FormField {
  public config: FormFieldConfig;
  public group: FormGroup;
  public fieldFocus: EventEmitter<boolean> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
