import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GbmFormInputComponent } from './gbm-form-input.component';

describe('GbmFormInputComponent', () => {
  let component: GbmFormInputComponent;
  let fixture: ComponentFixture<GbmFormInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GbmFormInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GbmFormInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
