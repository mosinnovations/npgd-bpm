import { Component, OnInit, EventEmitter } from '@angular/core';
import { FormField, FormFieldConfig } from '../form-component.interface';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-gbm-form-input',
  templateUrl: './gbm-form-input.component.html',
  styleUrls: ['./gbm-form-input.component.css']
})
export class GbmFormInputComponent implements OnInit, FormField {
  public config: FormFieldConfig;
  public group: FormGroup;
  public fieldFocus: EventEmitter<boolean> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
