import { Component, OnInit, EventEmitter } from '@angular/core';
import { FormField, FormFieldConfig } from '../form-component.interface';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-gbm-form-slider',
  templateUrl: './gbm-form-slider.component.html',
  styleUrls: ['./gbm-form-slider.component.css']
})
export class GbmFormSliderComponent implements OnInit, FormField {
  public config: FormFieldConfig;
  public group: FormGroup;
  public fieldFocus: EventEmitter<boolean> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
