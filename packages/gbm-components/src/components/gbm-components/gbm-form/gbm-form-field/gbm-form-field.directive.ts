import {
  AfterViewInit,
  ComponentFactoryResolver,
  ComponentRef,
  Directive,
  forwardRef,
  EventEmitter,
  Host,
  Inject,
  Input,
  OnInit,
  Optional,
  Output,
  ViewContainerRef
} from '@angular/core';
import { FormGroup } from '@angular/forms';

import { GbmFormFieldService } from './gbm-form-field.service';
import { GbmFormParent } from './gbm-form-parent.model';

@Directive({
  selector: '[gbmFormField]',
})
export class GbmFormFieldDirective implements OnInit {
  @Input() config: any = {};
  @Input() group: FormGroup;
  @Output() componentLoad: EventEmitter<any> = new EventEmitter();
  private component: ComponentRef<any>;

  constructor(
    private resolver: ComponentFactoryResolver,
    private container: ViewContainerRef,
    private formFieldService: GbmFormFieldService) {
    }

  ngOnInit() {
  }

  ngOnChanges() {
    this.initializeComponent();
  }

  private initializeComponent() {
    const component = this.formFieldService.getComponent(this.config.controlConfig.type);
    this.container.clear();
    
    if (component) {
      const factory = this.resolver.resolveComponentFactory<any>(component);
      this.component = this.container.createComponent(factory);
      this.componentLoad.emit(this.component);
      this.component.instance.config = this.config;
      this.component.instance.group = this.group;
      if (this.config.controlConfig.className) {
        this.component.location.nativeElement.classList.add(this.config.controlConfig.className);
      }
    }
  }
}
