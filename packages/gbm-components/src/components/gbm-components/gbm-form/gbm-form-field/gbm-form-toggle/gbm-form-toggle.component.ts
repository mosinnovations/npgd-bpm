import { Component, OnInit, EventEmitter } from '@angular/core';
import { FormField, FormFieldConfig } from '../form-component.interface';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'gbm-form-toggle',
  templateUrl: './gbm-form-toggle.component.html',
  styleUrls: ['./gbm-form-toggle.component.css']
})
export class GbmFormToggleComponent implements OnInit, FormField {
  public config: FormFieldConfig;
  public group: FormGroup;
  public fieldFocus: EventEmitter<boolean> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
