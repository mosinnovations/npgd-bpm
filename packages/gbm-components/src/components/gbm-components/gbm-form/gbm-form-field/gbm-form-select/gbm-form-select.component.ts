import { Component, OnInit, EventEmitter } from '@angular/core';
import { FormField, FormFieldConfig } from '../form-component.interface';
import { FormGroup } from '@angular/forms';
@Component({
  selector: 'app-gbm-form-select',
  templateUrl: './gbm-form-select.component.html',
  styleUrls: ['./gbm-form-select.component.css']
})
export class GbmFormSelectComponent implements OnInit, FormField {
  public config: FormFieldConfig;
  public group: FormGroup;
  public fieldFocus: EventEmitter<boolean> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
