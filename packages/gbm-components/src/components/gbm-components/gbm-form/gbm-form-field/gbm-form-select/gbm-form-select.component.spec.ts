import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GbmFormSelectComponent } from './gbm-form-select.component';

describe('GbmFormSelectComponent', () => {
  let component: GbmFormSelectComponent;
  let fixture: ComponentFixture<GbmFormSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GbmFormSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GbmFormSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
