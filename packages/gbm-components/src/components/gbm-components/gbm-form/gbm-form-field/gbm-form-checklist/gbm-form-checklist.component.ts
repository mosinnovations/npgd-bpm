import { Component, OnInit, EventEmitter } from '@angular/core';
import { FormField, FormFieldConfig } from '../form-component.interface';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-gbm-form-checklist',
  templateUrl: './gbm-form-checklist.component.html',
  styleUrls: ['./gbm-form-checklist.component.css']
})
export class GbmFormChecklistComponent implements OnInit {
  public config: FormFieldConfig;
  public group: FormGroup;
  public fieldFocus: EventEmitter<boolean> = new EventEmitter();
  public selectedOptions: boolean[] = [];

  constructor() { }

  ngOnInit() {
  }

  public updateForm() {
    this.fieldFocus.emit(true);
    console.log(this.group, this.config.id);
    this.group.setValue({ [this.config.id]: this.selectedOptions });
  }
}
