import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GbmFormChecklistComponent } from './gbm-form-checklist.component';

describe('GbmFormChecklistComponent', () => {
  let component: GbmFormChecklistComponent;
  let fixture: ComponentFixture<GbmFormChecklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GbmFormChecklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GbmFormChecklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
