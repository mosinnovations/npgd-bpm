import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { GbmFormToggleComponent } from './gbm-form-toggle/gbm-form-toggle.component';
import { GbmFormInputComponent } from './gbm-form-input/gbm-form-input.component';
import { GbmFormSelectComponent } from './gbm-form-select/gbm-form-select.component';
import { GbmFormDatepickerComponent } from './gbm-form-datepicker/gbm-form-datepicker.component';
import { GbmFormSliderComponent } from './gbm-form-slider/gbm-form-slider.component';
import { GbmFormChecklistComponent } from './gbm-form-checklist/gbm-form-checklist.component';

const FORM_COMPONENTS = {
  toggle: GbmFormToggleComponent,
  multiselect: GbmFormChecklistComponent,
  input: GbmFormInputComponent,
  select: GbmFormSelectComponent,
  datepicker: GbmFormDatepickerComponent,
  slider: GbmFormSliderComponent
};

@Injectable()
export class GbmFormFieldService {
  public _formComponents = FORM_COMPONENTS;

  constructor() {
  }

  public getComponent(type: string = '') {
    // TODO: How to handle no component?
    return this._formComponents[type.toLowerCase()];
  }

  public setComponent(keyOrSet: string | { [key: string]: any }, component?: any) {
    const set = (typeof keyOrSet === 'string' && component)
      ? { [keyOrSet]: component }
      : (typeof keyOrSet === 'object')
        ? keyOrSet
        : {};

    this._formComponents = { ...this._formComponents, ...set };
  }
}


