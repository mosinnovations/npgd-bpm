import { Component, OnInit, OnChanges, SimpleChanges, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { GbmFormParent } from './gbm-form-field/gbm-form-parent.model';

@Component({
  selector: 'app-gbm-form',
  templateUrl: './gbm-form.component.html',
  styleUrls: ['./gbm-form.component.css']
})
export class GbmFormComponent extends GbmFormParent implements OnInit, OnChanges {
  static readonly componentKey = 'gbm-dynoForm';

  @Input() public data: any;
  @Input() public currentFormId: string;
  @Input() public collectedData: any;
  @Output() public formSubmit: EventEmitter<any> = new EventEmitter();
  @Output() public onCompleteTask: EventEmitter<any> = new EventEmitter<any>();

  public form: FormGroup;
  public config: any[];

  private childFields: any[] = [];

  constructor(private fb: FormBuilder) {
    super();
  }

  public ngOnInit() {
    this.constructFormData(this.data.formData.fields);
  }

  public registerFormField(instance, index) {
    this.childFields[index] = instance;
  }

  public ngOnChanges(changes: SimpleChanges) {
    if (changes.collectedData && this.form) {
      this.form.patchValue(this.collectedData);
    }

    if (changes.data) {
      this.constructFormData(this.data.formData.fields);      
    }
  }

  public submitForm() {
    const payload = { formId: this.currentFormId, collectedData: this.form.value };
    this.formSubmit.emit(payload);
  }

  public completeTask() {
    const payload = { formId: this.currentFormId, collectedData: this.form.value };
    this.onCompleteTask.emit(payload);
  }

  private constructFormData(fields: any[]) {
    const formFields = fields.reduce((accumulator, field) => {
      // TODO: create map to add custom default values, validators, etc.
      return { ...accumulator, [field.id]: [''] };
    }, {});

    this.config = fields;
    this.form = this.fb.group(formFields);
  }
}
