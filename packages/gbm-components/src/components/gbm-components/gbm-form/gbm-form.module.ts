import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatSlideToggleModule,
  MatSliderModule,
  MatSelectModule
} from '@angular/material';

import { GbmFormComponent } from './gbm-form.component';
import { GbmFormFieldDirective } from './gbm-form-field/gbm-form-field.directive';
import { GbmFormFieldService } from './gbm-form-field/gbm-form-field.service';
import { GbmFormChecklistComponent } from './gbm-form-field/gbm-form-checklist/gbm-form-checklist.component';
import {
  GbmFormDatepickerComponent
} from './gbm-form-field/gbm-form-datepicker/gbm-form-datepicker.component';
import { GbmFormInputComponent } from './gbm-form-field/gbm-form-input/gbm-form-input.component';
import { GbmFormSelectComponent } from './gbm-form-field/gbm-form-select/gbm-form-select.component';
import { GbmFormSliderComponent } from './gbm-form-field/gbm-form-slider/gbm-form-slider.component';
import { GbmFormToggleComponent } from './gbm-form-field/gbm-form-toggle/gbm-form-toggle.component';

const MATERIAL_MODULES = [
  MatButtonModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatSlideToggleModule,
  MatSliderModule,
  MatSelectModule
];

const GBM_COMPONENTS = [
  GbmFormComponent,
  GbmFormChecklistComponent,
  GbmFormDatepickerComponent,
  GbmFormInputComponent,
  GbmFormSelectComponent,
  GbmFormSliderComponent,
  GbmFormToggleComponent,
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    ...MATERIAL_MODULES
  ],
  exports: [
    ...GBM_COMPONENTS,
    GbmFormFieldDirective
  ],
  entryComponents: [
    ...GBM_COMPONENTS
  ],
  declarations: [
    ...GBM_COMPONENTS,
    GbmFormFieldDirective
  ],
  providers: [
    GbmFormFieldService
  ],

})
export class GbmModule { }