# -*- mode: ruby -*-
# vi: set ft=ruby :

# initial bash script
# $script = <<SCRIPT
#   echo provisioning...
#   apt-get update
# SCRIPT

# Determine host os
module OS
  def OS.windows?
      (/cygwin|mswin|mingw|bccwin|wince|emx/ =~ RUBY_PLATFORM) != nil
  end

  def OS.mac?
      (/darwin/ =~ RUBY_PLATFORM) != nil
  end

  def OS.unix?
      !OS.windows?
  end

  def OS.linux?
      OS.unix? and not OS.mac?
  end
end

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
    # The most common configuration options are documented and commented below.
    # For a complete reference, please see the online documentation at
    # https://docs.vagrantup.com.
  
    # Every Vagrant development environment requires a box. You can search for
    # boxes at https://vagrantcloud.com/search.
    config.vm.box = "ubuntu/trusty64"
  
    config.vm.provider "virtualbox" do |v|
      v.memory = 3000
      v.cpus = 1
    end

    if OS.windows?
      puts "Launching from windows host"
    elsif OS.mac?
      puts "Launching from macos host"
    elsif OS.linux?
      puts "Launching from linux host"
    else
      puts "Launching from unknown host"
    end
  
    # Disable automatic box update checking. If you disable this, then
    # boxes will only be checked for updates when the user runs
    # `vagrant box outdated`. This is not recommended.
    # config.vm.box_check_update = false
  
    # Create a forwarded port mapping which allows access to a specific port
    # within the machine from a port on the host machine. In the example below,
    # accessing "localhost:8080" will access port 80 on the guest machine.
    # NOTE: This will enable public access to the opened port
    # config.vm.network "forwarded_port", guest: 80, host: 8080
  
    # Create a forwarded port mapping which allows access to a specific port
    # within the machine from a port on the host machine and only allow access
    # via 127.0.0.1 to disable public access
    if OS.mac? || OS.linux?
      # if mac or linux host machines, we use private network + nfs sharing
      config.vm.network "private_network", ip: "192.168.50.2"
      config.vm.synced_folder ".", "/home/vagrant/codebase", type: "nfs", create: true
    else
      # if windows host machine, we have to use port forwarding and the default vp file sharing
      config.vm.network "forwarded_port", guest: 5432, host: 5432
      config.vm.network "forwarded_port", guest: 27017, host: 27017
      config.vm.synced_folder ".", "/home/vagrant/codebase", create: true
    end
  
    config.ssh.forward_agent = true
    # Provider-specific configuration so you can fine-tune various
    # backing providers for Vagrant. These expose provider-specific options.
    # Example for VirtualBox:
    #
    # config.vm.provider "virtualbox" do |vb|
    #   # Display the VirtualBox GUI when booting the machine
    #   vb.gui = true
    #
    #   # Customize the amount of memory on the VM:
    #   vb.memory = "1024"
    # end
    #
    # View the documentation for the provider you are using for more
    # information on available options.
  
    # Enable provisioning with a shell script. Additional provisioners such as
    # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
    # documentation for more information about their specific syntax and use.
    config.vm.provision "file", source: "./provision", destination: "~/provision"
    config.vm.provision "shell",
      inline: "sh /home/vagrant/provision/provision.sh"
  end
  