#!/bin/bash
# this script installs and runs the ansible script on the guest
# machine. Mostly used on windows where ansible cannot operate natively
# move ssh key for bitbucket
echo "-----copying ssh keys for bitbucket";
sudo cp -R -f /home/vagrant/provision/.ssh/* /root/.ssh;
echo "-----changing ssh private key ownership";
sudo chmod 400 /root/.ssh/id_rsa;

if sudo dpkg -s ansible; then 
    echo "-----ansible installed";
    # run playbook
    echo "-----running ansible playbook";
    sudo cp -f /home/vagrant/provision/hosts /etc/ansible/hosts &&
    sudo ansible-playbook /home/vagrant/provision/playbook/playbook.yml;
    exit 0;
else 
    echo "-----ansible not installed, installing...";
    sudo apt-get install -y software-properties-common &&
    sudo apt-add-repository ppa:ansible/ansible &&
    sudo apt-get update &&
    sudo apt-get install -y ansible;
    echo "-----ansible install finished";
    # run playbook
    echo "-----running ansible playbook";
    sudo cp -f /home/vagrant/provision/hosts /etc/ansible/hosts &&
    sudo ansible-playbook /home/vagrant/provision/playbook/playbook.yml; 
    exit 0;
fi
