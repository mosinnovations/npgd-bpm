all: startBackend startFrontend startGBMFrontend;

startBackend:
	(cd packages/ngpd-bpmn-server; npm start);

startFrontend:
	(cd packages/ngpd-bpmn-client; npm start);

startGBMFrontend:
	(cd packages/gbm-client; npm start);

startGBMBackend:
	(cd packages/gbm-server; npm start);

startDB:
	mkdir -p db;
	mongod --dbpath=db;
