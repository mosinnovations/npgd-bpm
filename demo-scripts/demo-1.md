## Demo 1

* Add a new project called 'Harmonise'
* Define a new model for 'Harmonise' called 'Client Model' using the following JSON

```json
{
  "companyId": {
    "dataType": "string"
  },
  "companyName": {
    "dataType": "string"
  },
  "address": {
    "line1":  {
      "dataType": "string"
    },
    "line2": {
      "dataType": "string"
    },
    "zip": {
      "dataType": "string"
    }
  },
  "contacts": [
    {
      "firstName": {
        "dataType": "string"
      },
      "lastName": {
        "dataType": "string"
      },
      "email": {
        "dataType": "string"
      },
      "isPrimary": {
        "dataType": "boolean"
      }
    }
  ]
}
```

* Add a rule for 'Client Model' using the following data
```gherkin
When companyId == 'Oracle' and isPrimary == true
```

### Stretch Goal

* Create a test model
* Execute the rule against test model
* See results in UI