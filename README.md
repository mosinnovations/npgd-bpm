# NGPD Project Starter

This is a monorepo project starter that includes setups for:

* Angular client project using 'ngpd-merceros-ui'
* Express typescript server project
* Shared merceros module

Stack:

* Angular Merceros UI pack
* Express js
* Gulp
* Lerna (Monorepo build tool)
* Mongodb with Mongoose
* Typescript
* Webpack

Quick start:

1. bootstrap project - execute the following command at the root project level
	```
	bash
	npm run bootstrap
	```
2. pull latest, start bpmn-client fe & be projects
	```
	make -j
	```